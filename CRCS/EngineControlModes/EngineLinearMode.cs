﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace KerbCom.EngineControlModes
{
    [ControlMode.Details(guiName = "Engine linear control", guiHelp =
@"Engine linear control:

This mode behaves just like ""RCS linear control"", except that it works using engines instead."    
    )]
    class EngineLinearMode : ControlMode
    {
        private Vessel _vessel;
        private IIterativeWorker worker;


        ThrottleController[] throttles = new ThrottleController[0];

        private struct EngineConstants
        {
            public float minThrust, maxThrust;
            public int thrustCount;
        }

        private struct EngineVariables
        {
            public float maxFlow;
            public Vector3[] direction, position;
            public bool operable;
        }

        private class SharedState
        {
            //input
            public int engineCount = 0;
            public EngineConstants[] constants;
            public EngineVariables[] variables;
            public Vector3 COM;
            public Vector3 controlInput = Vector3.zero;
            public int frameCount = 0; //used to average accumulated input
            public bool layoutChanged = true;
            //output
            public double[] results;
            public enum Status
            {
                InputIsZero,
                Infeasible,
                ThrustSolverFailure,
                FuelSolverFailure,
                Solved
            }
            public Status status;
        }

        SharedState stateLocal = new SharedState(), stateWorker = new SharedState();

        //local private state for the GUI
        private SharedState.Status status;
        private Vector3 strength = Vector3.one;

        //worker private state:
        SimplexBoundedTwoPhase.State fuelSolverState;
        SimplexBounded.State thrustSolverState;
        int lastEngineCount = 0;
        private bool layoutChanged = true;

        public EngineLinearMode()
        {
            worker = new SequentialIterativeWorker(
                () =>
                {
                    //copy input state
                    if (stateLocal.layoutChanged)
                    {

                        if (stateLocal.constants == null)
                            stateWorker.constants = null;
                        else
                        {
                            if (stateWorker.constants == null || stateLocal.constants.Count() != stateWorker.constants.Count())
                                stateWorker.constants = new EngineConstants[stateLocal.constants.Count()];
                            stateLocal.constants.CopyTo(stateWorker.constants, 0);
                        }
                    }
                    if (stateLocal.variables == null)
                        stateWorker.variables = null;
                    else
                    {
                        if (stateWorker.variables == null || stateLocal.variables.Count() != stateWorker.variables.Count())
                            stateWorker.variables = new EngineVariables[stateLocal.variables.Count()];
                        stateLocal.variables.CopyTo(stateWorker.variables, 0);
                    }
                    stateWorker.COM = stateLocal.COM;
                    stateWorker.controlInput = stateLocal.controlInput;
                    stateWorker.frameCount = stateLocal.frameCount;
                    stateWorker.layoutChanged = stateLocal.layoutChanged;
                    stateWorker.engineCount = stateLocal.engineCount;
                    stateLocal.controlInput = Vector3.zero;
                    stateLocal.frameCount = 0;
                    stateLocal.layoutChanged = false;
                },
                () =>
                {
                    //Debug.Log("Updating:");
                    //try
                    //{
                    /// solve to find maximum allowed thrust, then solve to minimise fuel cost for a fraction of that thrust

                    // if the vessel port count has changed, recreate the solver states and constants
                    if (stateWorker.engineCount != lastEngineCount || fuelSolverState == null || thrustSolverState == null)
                    {
                        stateWorker.results = new double[stateWorker.engineCount];

                        fuelSolverState = SimplexBoundedTwoPhase.createState(stateWorker.engineCount + 6, 6, 2, 1.0);
                        for (int i = stateWorker.engineCount; i < fuelSolverState.n; ++i)
                            fuelSolverState.upper_bounds[i] = double.PositiveInfinity; // Remove artificial variable upper bounds
                        thrustSolverState = SimplexBounded.createState(stateWorker.engineCount, 5, null, null, fuelSolverState.upper_bounds, null);

                        lastEngineCount = stateWorker.engineCount;
                    }
                    Vector3 controlInput = stateWorker.controlInput / stateWorker.frameCount;
                    //don't bother if input is 0
                    if (controlInput.sqrMagnitude > float.Epsilon * 10)
                    {
                        // set the constraints
                        Vector3 controlDirection = controlInput.normalized;
                        float controlMagnitude = controlInput.magnitude;
                        if (controlMagnitude > 1.0f) controlMagnitude = 1.0f;
                        // find best axis to use as measure - largest coefficient
                        Vector3 magDir = new Vector3(Mathf.Abs(controlDirection.x), Mathf.Abs(controlDirection.y), Mathf.Abs(controlDirection.z));
                        int i_axis = magDir.x > magDir.y ? (magDir.x > magDir.z ? 0 : 2) : (magDir.y > magDir.z ? 1 : 2);
                        // other axes
                        int d_axis1 = (i_axis + 1) % 3;
                        int d_axis2 = (i_axis + 2) % 3;
                        float d_coeff1 = controlDirection[d_axis1] / controlDirection[i_axis];
                        float d_coeff2 = controlDirection[d_axis2] / controlDirection[i_axis];
                        // set constraints
                        for (int i = 0; i < stateWorker.engineCount; ++i)
                        {
                            //fuel cost
                            fuelSolverState.objectives[0, i] = stateWorker.variables[i].maxFlow;

                            fuelSolverState.constraints[0, i] = 0.0f;
                            fuelSolverState.constraints[1, i] = 0.0f;
                            fuelSolverState.constraints[2, i] = 0.0f;
                            fuelSolverState.constraints[3, i] = 0.0f;
                            fuelSolverState.constraints[4, i] = 0.0f;
                            fuelSolverState.constraints[5, i] = 0.0f;

                            if (stateWorker.variables[i].operable)
                            {
                                fuelSolverState.objectives[0, i] = stateWorker.variables[i].maxFlow;
                                int thrustCount = stateWorker.constants[i].thrustCount;
                                float thrust = stateWorker.constants[i].maxThrust / thrustCount;
                                for (int j = 0; j < thrustCount; ++j)
                                {
                                    Vector3 force = thrust * stateWorker.variables[i].direction[j];
                                    Vector3 offset = stateWorker.variables[i].position[j] - stateWorker.COM;
                                    Vector3 moment = Vector3.Cross(offset, force);
                                    fuelSolverState.constraints[0, i] += force.x;
                                    fuelSolverState.constraints[1, i] += force.y;
                                    fuelSolverState.constraints[2, i] += force.z;
                                    fuelSolverState.constraints[3, i] += moment.x;
                                    fuelSolverState.constraints[4, i] += moment.y;
                                    fuelSolverState.constraints[5, i] += moment.z;
                                }
                            }
                            else
                            {
                                fuelSolverState.constraints[0, i] = fuelSolverState.constraints[1, i] = fuelSolverState.constraints[2, i] = 0.0;
                                fuelSolverState.constraints[3, i] = fuelSolverState.constraints[4, i] = fuelSolverState.constraints[5, i] = 0.0;
                            }
                            //thrust in direction
                            thrustSolverState.equations[0, i] = -Vector3.Dot(new Vector3(
                                    (float)fuelSolverState.constraints[0, i],
                                    (float)fuelSolverState.constraints[1, i],
                                    (float)fuelSolverState.constraints[2, i]), controlDirection);

                            thrustSolverState.equations[1, i] = fuelSolverState.constraints[d_axis1, i] - d_coeff1 * fuelSolverState.constraints[i_axis, i];
                            thrustSolverState.equations[2, i] = fuelSolverState.constraints[d_axis2, i] - d_coeff2 * fuelSolverState.constraints[i_axis, i];
                            thrustSolverState.equations[3, i] = fuelSolverState.constraints[3, i];
                            thrustSolverState.equations[4, i] = fuelSolverState.constraints[4, i];
                            thrustSolverState.equations[5, i] = fuelSolverState.constraints[5, i];

                        }
                        thrustSolverState.equations[0, stateWorker.engineCount] = 0.0; //initial feasible total thrust is zero

                        thrustSolverState.equations[1, stateWorker.engineCount] = 0.0;
                        thrustSolverState.equations[2, stateWorker.engineCount] = 0.0;
                        thrustSolverState.equations[3, stateWorker.engineCount] = 0.0;
                        thrustSolverState.equations[4, stateWorker.engineCount] = 0.0;
                        thrustSolverState.equations[5, stateWorker.engineCount] = 0.0;
                        thrustSolverState.at_ub.populate(false);
                        thrustSolverState.optimal = false;
                        thrustSolverState.unconstrained = false;
                        thrustSolverState.iteration_count = 0;
#if DEBUG
                            Debug.Log("thrustSolverState: " + thrustSolverState.ToString());
#endif

                        //although feasible, the tableau is not in canonical form since basic variables do not form an identity matrix.
                        //correct this:
                        {
                            int basic_i = 0;
                            for (int j = 0; j < 5; ++j)
                            {
                                while (basic_i < stateWorker.engineCount && !SimplexBounded.canonicise(thrustSolverState, j, basic_i))
                                    ++basic_i;
                                if (basic_i == stateWorker.engineCount) // degenerate constraint
                                {
                                    thrustSolverState.basic_vars[j] = -1;
                                }
                            }
                        }
#if DEBUG
                            Debug.Log("pre-solve thrustSolverState: " + thrustSolverState.ToString());
#endif

                        SimplexBounded.solve(thrustSolverState);
                        if (thrustSolverState.optimal)
                        {
                            double maxThrust = SimplexBounded.getValue(thrustSolverState);

                            double targetThrust = maxThrust * controlMagnitude * 0.98;

                            if (targetThrust > double.Epsilon * 10)
                            {
                                //determine initial feasible state for the fuel solver
                                //finalise constraints
                                fuelSolverState.objective_RHS[0] = 0.0;
                                fuelSolverState.constraint_RHS[0] = targetThrust * controlDirection.x;
                                fuelSolverState.constraint_RHS[1] = targetThrust * controlDirection.y;
                                fuelSolverState.constraint_RHS[2] = targetThrust * controlDirection.z;
                                fuelSolverState.constraint_RHS[3] = 0.0;
                                fuelSolverState.constraint_RHS[4] = 0.0;
                                fuelSolverState.constraint_RHS[5] = 0.0;

                                fuelSolverState.at_ub.populate(false);
                                fuelSolverState.n = stateWorker.engineCount + 6;
                                //set up artificial objective and variables
                                for (int j = 0; j < 6; ++j)
                                {
                                    for (int i = stateWorker.engineCount; i < fuelSolverState.n; ++i)
                                        fuelSolverState.constraints[j, i] = i - stateWorker.engineCount == j ? (fuelSolverState.constraint_RHS[j] >= 0 ? 1.0 : -1.0) : 0.0;
                                    fuelSolverState.objectives[1, stateWorker.engineCount + j] = 1.0;
                                    fuelSolverState.objectives[0, stateWorker.engineCount + j] = 0.0;
                                }
                                for (int i = 0; i < stateWorker.engineCount; ++i)
                                    fuelSolverState.objectives[1, i] = 0.0;
                                fuelSolverState.objective_RHS[1] = 0.0;

                                //put in proper form
                                for (int j = 0; j < 6; ++j)
                                {
                                    SimplexBoundedTwoPhase.canonicise(fuelSolverState, j, stateWorker.engineCount + j);
                                }
                                //solve
                                fuelSolverState.optimal = false;
                                fuelSolverState.unconstrained = false;
                                fuelSolverState.iteration_count = 0;
                                fuelSolverState.active_obj = 1;

                                SimplexBoundedTwoPhase.solve(fuelSolverState); // Phase 1
                                if (fuelSolverState.optimal && Math.Abs(SimplexBoundedTwoPhase.getValue(fuelSolverState, 1)) < 1E-5)
                                {
                                    fuelSolverState.optimal = false;
                                    fuelSolverState.n = stateWorker.engineCount;
                                    fuelSolverState.active_obj = 0;

                                    SimplexBoundedTwoPhase.solve(fuelSolverState); // Phase 2
                                    if (fuelSolverState.optimal) // success
                                    {
                                        double fuel_used = SimplexBoundedTwoPhase.getValue(fuelSolverState, 0);

                                        SimplexBoundedTwoPhase.getVariableValues(fuelSolverState, ref stateWorker.results);
                                        stateWorker.status = SharedState.Status.Solved;
                                    }
                                    else // failure
                                    {
                                        stateWorker.status = SharedState.Status.FuelSolverFailure;
                                    }
                                }
                                else // failure
                                {
                                    stateWorker.status = SharedState.Status.FuelSolverFailure;
                                }
                            }
                            else
                            {
                                stateWorker.status = SharedState.Status.Infeasible;
                            }
                        }
                        else // failure
                        {
                            stateWorker.status = SharedState.Status.ThrustSolverFailure;
                        }
                    }
                    else // no input
                    {
                        stateWorker.status = SharedState.Status.InputIsZero;
                    }
                },
                () =>
                {
                    if (stateWorker.results == null)
                        stateLocal.results = null;
                    else if (stateLocal.results == null || stateWorker.results.Count() != stateLocal.results.Count())
                        stateLocal.results = new double[stateWorker.results.Count()];
                    if (stateWorker.status == SharedState.Status.Solved)
                        stateWorker.results.CopyTo(stateLocal.results, 0);

                    stateLocal.status = stateWorker.status;
                });

        }
        public override Vessel vessel
        {
            get
            {
                return _vessel;
            }
            set
            {
                _vessel = value;
            }
        }

        public override void onStructuralChange()
        {
            layoutChanged = true;
        }

        public override void onFrame()
        {
            string stage = "update";
            try
            {
                worker.update(resultsAvailable =>
                {
                    stage = "set control input";
                    if (stateLocal == null)
                        stage += ", stateLocal == null";
                    if (_vessel == null)
                        stage += ", vessel == null";
                    if (stateLocal.controlInput == null)
                        stage += ", stateLocal.controlInput == null";
                    //update input state
                    Vector3 modinput = new Vector3(_vessel.ctrlState.X, _vessel.ctrlState.Z, _vessel.ctrlState.Y);//.normalized;
                    modinput.Scale(strength);
                    stateLocal.controlInput += -_vessel.transform.InverseTransformDirection(_vessel.ReferenceTransform.rotation * modinput);
                    ++stateLocal.frameCount;

                    stateLocal.layoutChanged = layoutChanged;
                    if (layoutChanged)
                    {
                        stage = "layoutChanged";
                        throttles = ModuleKCAvionics.getAllModules<ThrottleController>(_vessel).ToArray();
                        stateLocal.engineCount = throttles.Count();
                        if (stateLocal.constants == null || stateLocal.constants.Count() != stateLocal.engineCount)
                        {
                            stateLocal.constants = new EngineConstants[stateLocal.engineCount];
                            stateLocal.variables = new EngineVariables[stateLocal.engineCount];
                        }
                        //update constants
                        for (int i = 0; i < stateLocal.engineCount; ++i)
                        {
                            stateLocal.constants[i].maxThrust = throttles[i].engine.maxThrust;
                            stateLocal.constants[i].minThrust = throttles[i].engine.minThrust;
                            int thrustCount = throttles[i].engine.thrustTransforms.Count;
                            stateLocal.constants[i].thrustCount = thrustCount;
                            stateLocal.variables[i].direction = new Vector3[thrustCount];
                            stateLocal.variables[i].position = new Vector3[thrustCount];
                        }
                        layoutChanged = false;
                    }
                    stage = "variable update";
                    //update variables
                    for (int i = 0; i < stateLocal.engineCount; ++i)
                    {
                        ThrottleController m = throttles[i];
                        int thrustCount = stateLocal.constants[i].thrustCount;
                        for (int j = 0; j < thrustCount; ++j)
                        {
                            Transform t = m.engine.thrustTransforms[j];
                            stateLocal.variables[i].direction[j] = vessel.transform.InverseTransformDirection(-t.forward);
                            stateLocal.variables[i].position[j] = vessel.transform.InverseTransformPoint(t.position);
                        }
                        stateLocal.variables[i].maxFlow = m.engine.realIsp > 0 ? m.engine.maxThrust / (m.engine.realIsp * m.engine.G) : 0.0f;
                        stateLocal.variables[i].operable = m.operational;
                    }
                    stateLocal.COM = vessel.transform.InverseTransformPoint(vessel.CoM + vessel.rb_velocity * Time.deltaTime);

                    //update output from output state
                    if (resultsAvailable)
                    {
                        stage = "update thrusts";
                        status = stateLocal.status;
                        if (stateLocal.status == SharedState.Status.Solved)
                            for (int i = 0; i < stateLocal.engineCount; ++i)
                            {
                                ThrottleController module = throttles[i];
                                if (module.operational)
                                    module.overrideThrottle = (float)stateLocal.results[i];
                                else
                                    module.overrideThrottle = 0.0f;
                            }
                        else
                            for (int i = 0; i < stateLocal.engineCount; ++i)
                            {
                                ThrottleController module = throttles[i];
                                module.overrideThrottle = 0.0f;
                            }
                    }
                    return true;

                });
            }
            catch (Exception)
            {
                Debug.Log("LinearMode2 update: Error in stage: " + stage);
                throw;
            }
        }

        public override void onGUI()
        {
            string statusText = "Unknown";
            switch (status)
            {
                case SharedState.Status.Solved:
                    statusText = "Nominal";
                    break;
                case SharedState.Status.Infeasible:
                    statusText = "Infeasible state";
                    break;
                case SharedState.Status.InputIsZero:
                    statusText = "No input.";
                    break;
                case SharedState.Status.FuelSolverFailure:
                    statusText = "Internal Error 01";
                    break;
                case SharedState.Status.ThrustSolverFailure:
                    statusText = "Internal Error 02";
                    break;
            }
            if (worker != null)
                GUILayout.Label(worker.running ? "Active" : "Inactive");
            if (throttles != null)
                GUILayout.Label("Engines controlled: " + throttles.Count());
            GUILayout.Label("Status: " + statusText);

            GUILayout.Label("Strength: ");
            GUILayout.BeginHorizontal();
            GUILayout.Label("X:");
            strength.x = GUILayout.HorizontalSlider(strength.x, 0.0f, 1.0f);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Y:");
            strength.y = GUILayout.HorizontalSlider(strength.y, 0.0f, 1.0f);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Z:");
            strength.z = GUILayout.HorizontalSlider(strength.z, 0.0f, 1.0f);
            GUILayout.EndHorizontal();
        }

        public override void start()
        {
            worker.start();
        }

        public override void stop()
        {
            worker.stop();
            worker.reset();
            layoutChanged = true;
        }

        public override bool running
        {
            get { return worker.running; }
        }

        public override void Load(ConfigNode node)
        {
            ConfigNode strength_node = node.GetNode("strength");
            if (strength_node != null)
            {
                strength.x = float.Parse(strength_node.GetValue("x"));
                strength.y = float.Parse(strength_node.GetValue("y"));
                strength.z = float.Parse(strength_node.GetValue("z"));
            }
        }

        public override void Save(ConfigNode node)
        {
            ConfigNode strength_node = node.AddNode("strength");
            strength_node.AddValue("x", strength.x);
            strength_node.AddValue("y", strength.y);
            strength_node.AddValue("z", strength.z);
        }

        public override IEnumerable<PartEffectorModule> getDesiredEffectors()
        {
            return ModuleKCAvionics.getAllModules<ThrottleController>(_vessel).Cast<PartEffectorModule>();
        }
    }
}

﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace KerbCom.EngineControlModes
{
    [ControlMode.Details(guiName = "Gimballed Engine Auto-Trim", guiHelp =
@"Engine Auto-Trim:

This mode is the current solution for asymmetric lifters, VTOL craft and other rocket-powered contraptions. It balances thrust and provides differential thrust steering using the main engines. It also ensures that the most fuel efficient combination of individual throttles is used to match the master throttle.

This mode does not currently support Solid Rocket Boosters, or other engines with a minimum active thrust setting. This will be rectified in an upcoming update.

It also does not yet make use of engine gimbals to provide balance. This means that several common designs, such as dual engine, single engine or Space Shuttle-style designs are not yet supported by this plugin. This will be rectified in another version.

This mode works by maximising thrust in the weighted average direction of all the engines on the vessel. It will therefore work as expected most of the time, regardless of command pod orientation."
        )]
    class EngineGimbalMode : ControlMode
    {
        private Vessel _vessel;
        private IIterativeWorker worker;

        GimballedEngineController[] gimballedEngines = new GimballedEngineController[0];
        BaseThrottleController[] staticEngines = new BaseThrottleController[0];

        #region Shared State
        
        private struct StaticEngineConstants
        {
            public float minThrust, maxThrust;
            public int thrustCount;
        }

        private struct StaticEngineVariables
        {
            public float maxFlow;
            public Vector3[] direction, position;
            public bool operable;
        }

        private struct GimballedEngineConstants
        {
            public float minThrust, maxThrust;
            public int thrustCount;
            public float gimbalRange;
        }

        private struct GimballedEngineVariables
        {
            public float maxFlow;
            public Vector3[] position;
            public Quaternion[] orientation;
            public bool operable;
        }

        private class SharedState
        {
            //input
            public int staticEngineCount = 0;
            public StaticEngineConstants[] staticConstants;
            public StaticEngineVariables[] staticVariables;

            public int gimballedEngineCount = 0;
            public GimballedEngineConstants[] gimbalConstants;
            public GimballedEngineVariables[] gimbalVariables;

            public Vector3 COM;
            public float throttleInput = 0.0f;
            public Vector3 torqueInput = Vector3.zero;
            public int frameCount = 0;
            public bool layoutChanged = true;

            //output
            public double[] staticThrusts;
            public double[] gimballedThrusts;
            public Vector2[] gimbalSettings;
            public enum Status
            {
                InputIsZero,
                Infeasible,
                ThrustSolverFailure,
                TorqueSolverFailure,
                FuelSolverFailure,
                Solved
            }
            public Status status;
        }

        SharedState stateLocal = new SharedState(), stateWorker = new SharedState();
        #endregion

        //local private state for the GUI
        private SharedState.Status status;
        private float strength = 1.0f;
        private Vector3 torqueStrength = Vector3.one;

        #region Worker state
        
        CLP.Constraint thrustMomentX, thrustMomentY, thrustMomentZ;
        CLP.Constraint torqueThrustMag;
        CLP.Constraint torqueMomentA1, torqueMomentA2;
        CLP.Constraint fuelThrustMag;
        CLP.Constraint fuelMomentX, fuelMomentY, fuelMomentZ;
        CLP.Constraint[] fuelMoments;
        CLP.Problem fuelProblem, thrustProblem, torqueProblem;
        CLP.Solvers.Solver fuelSolver, thrustSolver, torqueSolver;
        CLP.BoundedVariable[] staticEngineVariables;
        private struct GimbalVariables
        {
            public CLP.BoundedVariable[] NZ, PX, NX, PY, NY;
            public CLP.BoundedVariable[][] slack;
            public CLP.BoundedVariable[] bpx, bnx, bpy, bny,
                tpx, tnx, tpy, tny;
            public GimbalVariables(int size)
            {
                slack = new CLP.BoundedVariable[8][] {
                    bpx = new CLP.BoundedVariable[size],
                    bpy = new CLP.BoundedVariable[size],
                    bnx = new CLP.BoundedVariable[size],
                    bny = new CLP.BoundedVariable[size],
                    tpx = new CLP.BoundedVariable[size],
                    tpy = new CLP.BoundedVariable[size],
                    tnx = new CLP.BoundedVariable[size],
                    tny = new CLP.BoundedVariable[size]
                };
                for (int i = 0; i < 8; ++i)
                {
                    for (int j = 0; j < size; ++j)
                        slack[i][j] = new CLP.BoundedVariable();
                }

                NZ = new CLP.BoundedVariable[size];
                PX = new CLP.BoundedVariable[size];
                NX = new CLP.BoundedVariable[size];
                PY = new CLP.BoundedVariable[size];
                NY = new CLP.BoundedVariable[size];
                for (int i = 0; i < size; ++i)
                {
                    NZ[i] = new CLP.BoundedVariable(0.0f, 1.0f);
                    PX[i] = new CLP.BoundedVariable();
                    NX[i] = new CLP.BoundedVariable();
                    PY[i] = new CLP.BoundedVariable();
                    NY[i] = new CLP.BoundedVariable();
                }
            }
            public IEnumerable<CLP.BoundedVariable> all()
            {
                return new[] { NZ, PX, NX, PY, NY, slack[0], slack[1], slack[2], slack[3], slack[4], slack[5], slack[6], slack[7] }.SelectMany(id => id);
            }
        }
        GimbalVariables gimbalVariables;
        IEnumerable<CLP.BoundedVariable> allVariables;
        class GimbalConstraintSet
        {
            public CLP.Constraint[] c;
            public CLP.Constraint bpx, bpy, bnx, bny,
                tpx, tpy, tnx, tny;
            public GimbalConstraintSet()
            {
                c = new CLP.Constraint[] {
                    bpx = new CLP.Constraint(),
                    bpy = new CLP.Constraint(),
                    bnx = new CLP.Constraint(),
                    bny = new CLP.Constraint(),
                    tpx = new CLP.Constraint(),
                    tpy = new CLP.Constraint(),
                    tnx = new CLP.Constraint(),
                    tny = new CLP.Constraint()
                };
            }
        }
        GimbalConstraintSet[] gimbalConstraints;

        int lastStaticEngineCount = 0;
        int lastGimballedEngineCount = 0;
        #endregion

        
        private bool layoutChanged = true;

        public EngineGimbalMode()
        {
            worker = new SequentialIterativeWorker(
                #region Copy input to worker
                () =>
                {
                    //copy input state
                    if (stateLocal.layoutChanged)
                    {

                        if (stateLocal.staticConstants == null)
                            stateWorker.staticConstants = null;
                        else
                        {
                            if (stateWorker.staticConstants == null || stateLocal.staticConstants.Count() != stateWorker.staticConstants.Count())
                                stateWorker.staticConstants = new StaticEngineConstants[stateLocal.staticConstants.Count()];
                            stateLocal.staticConstants.CopyTo(stateWorker.staticConstants, 0);
                        }
                        if (stateLocal.gimbalConstants == null)
                            stateWorker.gimbalConstants = null;
                        else
                        {
                            if (stateWorker.gimbalConstants == null || stateLocal.gimbalConstants.Count() != stateWorker.gimbalConstants.Count())
                                stateWorker.gimbalConstants = new GimballedEngineConstants[stateLocal.gimbalConstants.Count()];
                            stateLocal.gimbalConstants.CopyTo(stateWorker.gimbalConstants, 0);
                        }
                    }
                    if (stateLocal.staticVariables == null)
                        stateWorker.staticVariables = null;
                    else
                    {
                        if (stateWorker.staticVariables == null || stateLocal.staticVariables.Count() != stateWorker.staticVariables.Count())
                            stateWorker.staticVariables = new StaticEngineVariables[stateLocal.staticVariables.Count()];
                        stateLocal.staticVariables.CopyTo(stateWorker.staticVariables, 0);
                    }
                    if (stateLocal.gimbalVariables == null)
                        stateWorker.gimbalVariables = null;
                    else
                    {
                        if (stateWorker.gimbalVariables == null || stateLocal.gimbalVariables.Count() != stateWorker.gimbalVariables.Count())
                            stateWorker.gimbalVariables = new GimballedEngineVariables[stateLocal.gimbalVariables.Count()];
                        stateLocal.gimbalVariables.CopyTo(stateWorker.gimbalVariables, 0);
                    }
                    stateWorker.COM = stateLocal.COM;
                    stateWorker.throttleInput = stateLocal.throttleInput;
                    stateWorker.torqueInput = stateLocal.torqueInput;
                    stateWorker.frameCount = stateLocal.frameCount;
                    stateWorker.layoutChanged = stateLocal.layoutChanged;
                    stateWorker.staticEngineCount = stateLocal.staticEngineCount;
                    stateWorker.gimballedEngineCount = stateLocal.gimballedEngineCount;
                    stateLocal.throttleInput = 0.0f;
                    stateLocal.torqueInput = Vector3.zero;
                    stateLocal.frameCount = 0;
                    stateLocal.layoutChanged = false;
                },
                #endregion
                #region Solve system
                () =>
                {
                    if (stateWorker.staticEngineCount != lastStaticEngineCount || stateWorker.gimballedEngineCount != lastGimballedEngineCount || fuelSolver == null)
                    #region Reconstruct system and solvers
                    
                    {
                        //Debug.Log("Constructing solver state, engine count: " + stateWorker.staticEngineCount + ", gimballed engine count: " + stateWorker.gimballedEngineCount);
                        stateWorker.staticThrusts = new double[stateWorker.staticEngineCount];

                        staticEngineVariables = new CLP.BoundedVariable[stateWorker.staticEngineCount];
                        for (int i = 0; i < stateWorker.staticEngineCount; ++i)
                        {
                            staticEngineVariables[i] = new CLP.BoundedVariable(0, 1);
                        }

                        stateWorker.gimballedThrusts = new double[stateWorker.gimballedEngineCount];
                        stateWorker.gimbalSettings = new Vector2[stateWorker.gimballedEngineCount];
                        gimbalVariables = new GimbalVariables(stateWorker.gimballedEngineCount);
                        allVariables = staticEngineVariables.Concat(gimbalVariables.all());

                        fuelProblem = new CLP.Problem();
                        fuelProblem.constraints.AddRange(new CLP.Constraint[]{
                            fuelThrustMag = new CLP.Constraint(),
                            fuelMomentX = new CLP.Constraint(),
                            fuelMomentY = new CLP.Constraint(),
                            fuelMomentZ = new CLP.Constraint()
                        });
                        fuelMoments = new CLP.Constraint[] { fuelMomentX, fuelMomentY, fuelMomentZ };

                        thrustProblem = new CLP.Problem();
                        thrustProblem.constraints.AddRange(new CLP.Constraint[]{
                            thrustMomentX = fuelMomentX.shallow_clone(),
                            thrustMomentY = fuelMomentY.shallow_clone(),
                            thrustMomentZ = fuelMomentZ.shallow_clone()
                        });
                        thrustMomentX.RHS = 0.0;
                        thrustMomentY.RHS = 0.0;
                        thrustMomentZ.RHS = 0.0;

                        torqueProblem = new CLP.Problem();
                        torqueProblem.constraints.AddRange(new CLP.Constraint[]{
                            torqueThrustMag = fuelThrustMag.shallow_clone(),
                            torqueMomentA1 = new CLP.Constraint(),
                            torqueMomentA2 = new CLP.Constraint()
                        });
                        torqueMomentA1.RHS = 0.0;
                        torqueMomentA2.RHS = 0.0;

                        //Gimbal constraints - these do not change on each itertion, so set them here
                        gimbalConstraints = new GimbalConstraintSet[stateWorker.gimballedEngineCount];
                        for (int i = 0; i < stateWorker.gimballedEngineCount; ++i)
                        {
                            CLP.BoundedVariable PX, NX, PY, NY, NZ;
                            CLP.BoundedVariable[] vars = new CLP.BoundedVariable[]{
                                PX = gimbalVariables.PX[i],
                                PY = gimbalVariables.PY[i],
                                NZ = gimbalVariables.NZ[i],
                                NX = gimbalVariables.NX[i],
                                NY = gimbalVariables.NY[i]
                            };
                            gimbalConstraints[i] = new GimbalConstraintSet();
                            for (int j = 0; j < 8; ++j)
                            {
                                fuelProblem.constraints.Add(gimbalConstraints[i].c[j]);
                                torqueProblem.constraints.Add(gimbalConstraints[i].c[j]);
                                thrustProblem.constraints.Add(gimbalConstraints[i].c[j]);
                            }
                            //Construct points:
                            Vector3d origin = Vector3d.zero;
                            Vector3d apo = Vector3d.back;
                            double range = stateWorker.gimbalConstants[i].gimbalRange * (Math.PI / 180.0);
                            double s_t = Math.Sin(range);
                            double c_t = Math.Cos(range);

                            Vector3d[] lowerNormals = new Vector3d[] {
                                new Vector3d(c_t, 0.0, s_t),//px
                                new Vector3d(0.0, c_t, s_t),//py
                                new Vector3d(-c_t, 0.0, s_t),//nx
                                new Vector3d(0.0, -c_t, s_t),//ny
                                new Vector3d(c_t, 0.0, s_t),//px
                            };
                            //angle limits:
                            for (int j = 0; j < 4; ++j)
                            {
                                Vector3d normal = lowerNormals[j];
                                normal.Normalize();
                                //Equation is dot(p, normal) <= 0
                                // :. dot(p, normal) + s = 0, s >= 0
                                CLP.Constraint c = gimbalConstraints[i].c[j];
                                c.f[NX] = -(c.f[PX] = normal.x); //x dir pair
                                c.f[NY] = -(c.f[PY] = normal.y); //y dir pair
                                c.f[NZ] = -normal.z; // thrust is neg z
                                c.f[gimbalVariables.slack[j][i]] = 1.0; //slack
                                c.RHS = 0.0; // plane intersects origin
                            }

                            //calc upper plane normal - px:
                            Vector3d upxn;
                            {
                                Vector3d pyc = Vector3d.Cross(lowerNormals[1], lowerNormals[0]);
                                pyc.z += 1.0;
                                Vector3d nyc = Vector3d.Cross(lowerNormals[0], lowerNormals[3]);
                                nyc.z += 1.0;
                                upxn = Vector3d.Cross(pyc, nyc);
                                upxn.Normalize();
                            }
                            Vector3d[] upperNormals = new Vector3d[] {
                                new Vector3d(upxn.x, 0.0, upxn.z),
                                new Vector3d(0.0, upxn.x, upxn.z),
                                new Vector3d(-upxn.x, 0.0, upxn.z),
                                new Vector3d(0.0, -upxn.x, upxn.z),
                            };
                            double uRHS = -upxn.z;
                            for (int j = 0; j < 4; ++j)
                            {
                                Vector3d normal = upperNormals[j];
                                normal.Normalize();
                                //Equation is dot(p, normal) <= dot((0,0,-1),normal)
                                // :. dot(p, normal) + s = dot((0,0,-1),normal), s >= 0
                                CLP.Constraint c = gimbalConstraints[i].c[j + 4];
                                c.f[NX] = -(c.f[PX] = normal.x); //x dir pair
                                c.f[NY] = -(c.f[PY] = normal.y); //y dir pair
                                c.f[NZ] = -normal.z; // thrust is neg z
                                c.f[gimbalVariables.slack[j + 4][i]] = 1.0; //slack
                                c.RHS = uRHS;// Vector3d.Dot(normal, new Vector3d(0.0, 0.0, -1.0));
                            }
#if NOTDEFINED

                            //Errors in the upper planes
                            //tip - calculate one plane, then reflect and rotate it to make the others
                            Vector3d[] relcorners = new Vector3d[5];
                            for (int j = 0; j < 4; ++j)
                            {
                                relcorners[j + 1] = Vector3d.Cross(lowerNormals[j + 1], lowerNormals[j]);
                                relcorners[j + 1].Normalize();
                                relcorners[j + 1].z += 1.0;
                            }
                            relcorners[0] = relcorners[4];
                            //conservative approximation of magnitude limits:
                            for (int j = 0; j < 4; ++j)
                            {
                                Vector3d normal = Vector3d.Cross(relcorners[j], relcorners[j + 1]);
                                normal.Normalize();
                                //Equation is dot(p, normal) <= dot((0,0,-1),normal)
                                // :. dot(p, normal) + s = dot((0,0,-1),normal), s >= 0
                                CLP.Constraint c = gimbalConstraints[i].c[j + 4];
                                c.f[NX] = -(c.f[PX] = normal.x); //x dir pair
                                c.f[NY] = -(c.f[PY] = normal.y); //y dir pair
                                c.f[NZ] = -normal.z; // thrust is neg z
                                c.f[gimbalVariables.slack[j + 4][i]] = 1.0; //slack
                                c.RHS = Vector3d.Dot(normal, new Vector3d(0.0, 0.0, -1.0));
                            }
#endif
                        }

                        //thrustSolver = new CLP.Solvers.MaxPrimalSimplex(thrustProblem, allVariables);
                        //torqueSolver = new CLP.Solvers.MaxPrimalSimplex(torqueProblem, allVariables);
                        //fuelSolver = new CLP.Solvers.MaxPrimalSimplex(fuelProblem, allVariables);
                        thrustSolver = new CLP.Solvers.MaxLPSolve(thrustProblem, allVariables);
                        torqueSolver = new CLP.Solvers.MaxLPSolve(torqueProblem, allVariables);
                        fuelSolver = new CLP.Solvers.MaxLPSolve(fuelProblem, allVariables);

                        lastStaticEngineCount = stateWorker.staticEngineCount;
                        lastGimballedEngineCount = stateWorker.gimballedEngineCount;
                        //Debug.Log("Done constructing");
                    }
                    #endregion

                    float targetThrottle = Mathf.Clamp(stateWorker.throttleInput / stateWorker.frameCount, 0.0f, 1.0f);
                    Vector3 thrustDirection = Vector3.zero;
                    #region Calculate target thrust direction
                    for (int i = 0; i < stateWorker.staticEngineCount; ++i)
                    {
                        if (stateWorker.staticVariables[i].operable)
                        {
                            int thrustCount = stateWorker.staticConstants[i].thrustCount;
                            float thrust = stateWorker.staticConstants[i].maxThrust / thrustCount;
                            for (int j = 0; j < thrustCount; ++j)
                            {
                                thrustDirection += thrust * stateWorker.staticVariables[i].direction[j];
                            }
                        }
                    }
                    for (int i = 0; i < stateWorker.gimballedEngineCount; ++i)
                    {
                        if (stateWorker.gimbalVariables[i].operable)
                        {
                            int thrustCount = stateWorker.gimbalConstants[i].thrustCount;
                            float thrust = stateWorker.gimbalConstants[i].maxThrust / thrustCount;
                            for (int j = 0; j < thrustCount; ++j)
                            {
                                thrustDirection += thrust * (stateWorker.gimbalVariables[i].orientation[j] * Vector3.back);
                            }
                        }
                    }
                    thrustDirection.Normalize();
                    #endregion
                    Vector3 thrustInput = thrustDirection * targetThrottle;

                    // torque solver dependencies
                    bool torqueEnabled = false;
                    Vector3 torqueInput = stateWorker.torqueInput / stateWorker.frameCount;

                    float torqueInputMagnitude = torqueInput.magnitude;

                    Vector3d torqueDirection = Vector3d.zero;
                    int i_axis = 0, d_axis1 = 0, d_axis2 = 0;
                    double d_coeff1 = 0, d_coeff2 = 0;
                    if (torqueInputMagnitude > 1E-4)
                    {
                        torqueEnabled = true;
                        torqueDirection = torqueInput / torqueInputMagnitude;
                        if (torqueInputMagnitude > 1.0f) torqueInputMagnitude = 1.0f;

                        // find best axis to use as measure - largest coefficient
                        Vector3d magDir = new Vector3d(Math.Abs(torqueDirection.x), Math.Abs(torqueDirection.y), Math.Abs(torqueDirection.z));
                        i_axis = magDir.x > magDir.y ? (magDir.x > magDir.z ? 0 : 2) : (magDir.y > magDir.z ? 1 : 2);
                        // other axes
                        d_axis1 = (i_axis + 1) % 3;
                        d_axis2 = (i_axis + 2) % 3;
                        d_coeff1 = torqueDirection[d_axis1] / torqueDirection[i_axis];
                        d_coeff2 = torqueDirection[d_axis2] / torqueDirection[i_axis];
                    }
                    //don't bother if input is 0
                    if (targetThrottle < 1E-6)
                    {
                        stateWorker.status = SharedState.Status.InputIsZero;
                        return;
                    }

                    //Debug.Log("Setting up problems");

                    #region Set up coefficients for static engines
                    for (int i = 0; i < stateWorker.staticEngineCount; ++i)
                    {
                        CLP.BoundedVariable var = staticEngineVariables[i];

                        #region Set fuel problem objective and constraints
                        
                        //fuel cost
                        fuelProblem.objective[var] = -stateWorker.staticVariables[i].maxFlow;

                        if (stateWorker.staticVariables[i].operable)
                        {
                            double mag = 0.0;
                            double mX = 0.0, mY = 0.0, mZ = 0.0;

                            int thrustCount = stateWorker.staticConstants[i].thrustCount;
                            float thrust = stateWorker.staticConstants[i].maxThrust / thrustCount;
                            for (int j = 0; j < thrustCount; ++j)
                            {
                                Vector3 force = thrust * stateWorker.staticVariables[i].direction[j];
                                Vector3 offset = stateWorker.staticVariables[i].position[j] - stateWorker.COM;
                                Vector3 moment = Vector3.Cross(offset, force);
                                mag += Vector3.Dot(force, thrustDirection);
                                mX += moment.x;
                                mY += moment.y;
                                mZ += moment.z;
                            }
                            fuelThrustMag.f[var] = mag;
                            fuelMomentX.f[var] = mX;
                            fuelMomentY.f[var] = mY;
                            fuelMomentZ.f[var] = mZ;
                        }
                        else
                        {
                            fuelThrustMag.f[var] = 0.0;
                            fuelMomentX.f[var] = 0.0;
                            fuelMomentY.f[var] = 0.0;
                            fuelMomentZ.f[var] = 0.0;
                        }
                        #endregion
                        
                        //Set thrust problem objective
                        thrustProblem.objective[var] = fuelThrustMag.f[var];

                        if (torqueEnabled)
                        {
                            #region Set torque problem objective and constraints
                            torqueProblem.objective[var] = Vector3d.Dot(
                                new Vector3d(
                                    fuelMomentX.f[var],
                                    fuelMomentY.f[var],
                                    fuelMomentZ.f[var]),
                                torqueDirection);
                            torqueMomentA1.f[var] = fuelMoments[d_axis1].f[var] - d_coeff1 * fuelMoments[i_axis].f[var];
                            torqueMomentA2.f[var] = fuelMoments[d_axis2].f[var] - d_coeff2 * fuelMoments[i_axis].f[var];
                            #endregion
                        }
                    }
                    #endregion

                    #region Set up coefficients for gimballed engines
                    {
                        //Debug.Log("Setting up gimbal constraints/objectives");
                        CLP.BoundedVariable[] vars;
                        double[] mag = new double[3];
                        double[] mX = new double[3], mY = new double[3], mZ = new double[3];
                        Vector3[] dir = new Vector3[3];
                        int i, j, k;
                        for (i = 0; i < stateWorker.gimballedEngineCount; ++i)
                        {
                            // fuel minimisation:
                            // cost is a linear approximation of pythagoras.
                            fuelProblem.objective[gimbalVariables.NZ[i]] = -stateWorker.gimbalVariables[i].maxFlow;
                            // make sure that corner points match the original problem
                            double r = Math.Sin(stateWorker.gimbalConstants[i].gimbalRange);
                            double hscale = (Math.Sqrt(1.0 - 2.0 * r * r) * 0.5 - 0.5) / r; //should really precompute this
                            double hflow = -stateWorker.gimbalVariables[i].maxFlow * hscale;
                            fuelProblem.objective[gimbalVariables.PX[i]] = hflow;
                            fuelProblem.objective[gimbalVariables.NX[i]] = hflow;
                            fuelProblem.objective[gimbalVariables.PY[i]] = hflow;
                            fuelProblem.objective[gimbalVariables.NY[i]] = hflow;
                            CLP.BoundedVariable PX, NX, PY, NY, NZ;
                            vars = new CLP.BoundedVariable[]{
                                PX = gimbalVariables.PX[i],
                                PY = gimbalVariables.PY[i],
                                NZ = gimbalVariables.NZ[i],
                                NX = gimbalVariables.NX[i],
                                NY = gimbalVariables.NY[i]
                            };
                            if (stateWorker.gimbalVariables[i].operable)
                            {
                                mag.populate(0.0);
                                mX.populate(0.0);
                                mY.populate(0.0);
                                mZ.populate(0.0);
                                int thrustCount = stateWorker.gimbalConstants[i].thrustCount;
                                float thrust = stateWorker.gimbalConstants[i].maxThrust / thrustCount;
                                //Vector3[] ldir = new Vector3[] { new Vector3(1.0f, 0.0f, 0.0f), new Vector3(0.0f, 1.0f, 0.0f), Vector3.back };

                                for (j = 0; j < thrustCount; ++j)
                                {
                                    dir[0] = stateWorker.gimbalVariables[i].orientation[j] * Vector3.left;
                                    dir[1] = stateWorker.gimbalVariables[i].orientation[j] * Vector3.down;
                                    dir[2] = stateWorker.gimbalVariables[i].orientation[j] * Vector3.back;
                                    for (k = 0; k < 3; ++k)
                                    {
                                        Vector3 force = thrust * dir[k];
                                        Vector3 offset = stateWorker.gimbalVariables[i].position[j] - stateWorker.COM;
                                        Vector3 moment = Vector3.Cross(offset, force);
                                        mag[k] += Vector3.Dot(force, thrustDirection);
                                        mX[k] += moment.x;
                                        mY[k] += moment.y;
                                        mZ[k] += moment.z;
                                    }
                                }
                                fuelThrustMag.f[PX] = mag[0];
                                fuelThrustMag.f[NX] = -mag[0];
                                fuelThrustMag.f[PY] = mag[1];
                                fuelThrustMag.f[NY] = -mag[1];
                                fuelThrustMag.f[NZ] = mag[2];

                                fuelMomentX.f[PX] = mX[0];
                                fuelMomentX.f[NX] = -mX[0];
                                fuelMomentX.f[PY] = mX[1];
                                fuelMomentX.f[NY] = -mX[1];
                                fuelMomentX.f[NZ] = mX[2];

                                fuelMomentY.f[PX] = mY[0];
                                fuelMomentY.f[NX] = -mY[0];
                                fuelMomentY.f[PY] = mY[1];
                                fuelMomentY.f[NY] = -mY[1];
                                fuelMomentY.f[NZ] = mY[2];

                                fuelMomentZ.f[PX] = mZ[0];
                                fuelMomentZ.f[NX] = -mZ[0];
                                fuelMomentZ.f[PY] = mZ[1];
                                fuelMomentZ.f[NY] = -mZ[1];
                                fuelMomentZ.f[NZ] = mZ[2];
                            }
                            else
                            {
                                fuelThrustMag.f[PX] = 0.0;
                                fuelThrustMag.f[NX] = 0.0;
                                fuelThrustMag.f[PY] = 0.0;
                                fuelThrustMag.f[NY] = 0.0;
                                fuelThrustMag.f[NZ] = 0.0;

                                fuelMomentX.f[PX] = 0.0;
                                fuelMomentX.f[NX] = 0.0;
                                fuelMomentX.f[PY] = 0.0;
                                fuelMomentX.f[NY] = 0.0;
                                fuelMomentX.f[NZ] = 0.0;

                                fuelMomentY.f[PX] = 0.0;
                                fuelMomentY.f[NX] = 0.0;
                                fuelMomentY.f[PY] = 0.0;
                                fuelMomentY.f[NY] = 0.0;
                                fuelMomentY.f[NZ] = 0.0;

                                fuelMomentZ.f[PX] = 0.0;
                                fuelMomentZ.f[NX] = 0.0;
                                fuelMomentZ.f[PY] = 0.0;
                                fuelMomentZ.f[NY] = 0.0;
                                fuelMomentZ.f[NZ] = 0.0;
                            }
                            //thrust in direction
                            thrustProblem.objective[PX] = fuelThrustMag.f[PX];
                            thrustProblem.objective[NX] = fuelThrustMag.f[NX];
                            thrustProblem.objective[PY] = fuelThrustMag.f[PY];
                            thrustProblem.objective[NY] = fuelThrustMag.f[NY];
                            thrustProblem.objective[NZ] = fuelThrustMag.f[NZ];

                            if (torqueEnabled)
                            {
                                for (int _i = 0; _i < 3; ++_i)
                                {
                                    CLP.BoundedVariable var = vars[_i];
                                    torqueProblem.objective[var] = Vector3d.Dot(
                                        new Vector3d(
                                            fuelMomentX.f[var],
                                            fuelMomentY.f[var],
                                            fuelMomentZ.f[var]),
                                        torqueDirection);
                                    torqueMomentA1.f[var] = fuelMoments[d_axis1].f[var] - d_coeff1 * fuelMoments[i_axis].f[var];
                                    torqueMomentA2.f[var] = fuelMoments[d_axis2].f[var] - d_coeff2 * fuelMoments[i_axis].f[var];
                                };
                                for (int _i = 0; _i < 2; ++_i)
                                {
                                    CLP.BoundedVariable nvar = vars[_i + 3];
                                    CLP.BoundedVariable var = vars[_i];
                                    torqueProblem.objective[nvar] = -torqueProblem.objective[var];
                                    torqueMomentA1.f[nvar] = -torqueMomentA1.f[var];
                                    torqueMomentA2.f[nvar] = -torqueMomentA2.f[var];
                                };
                            }
                        }
                    }
                    #endregion

                    //Debug.Log("Done set up, now solving");
                    Debug.Log("Thrust solver:");
                    #region Solve to find maximum thrust
                    thrustSolver.solve();

                    if (thrustSolver.status != CLP.Solvers.Solver.Status.Optimal)
                    {
                        stateWorker.status = SharedState.Status.ThrustSolverFailure;
                        return;
                    }
                    double maxThrust = thrustSolver.objective_value;
                    Debug.Log("Max thrust: " + maxThrust);
                    double targetThrust = maxThrust * targetThrottle * 0.98;
                    Debug.Log("Target thrust: " + maxThrust);
                    if (targetThrust < double.Epsilon * 10)
                    {
                        stateWorker.status = SharedState.Status.Infeasible;
                        return;
                    }
                    #endregion

                    Vector3d targetTorque = Vector3d.zero;
                    double maxTorque = 0.0;
                    //find the range of rotation possible:
                    if (torqueEnabled)
                    {
                        #region Solve to find maximum torque at constant thrust
                        torqueThrustMag.RHS = targetThrust;
                        Debug.Log("Torque solver:");
                        torqueSolver.solve();
                        if (torqueSolver.status != CLP.Solvers.Solver.Status.Optimal)
                        {
                            stateWorker.status = SharedState.Status.TorqueSolverFailure;
                            return;
                        }
                        maxTorque = torqueSolver.objective_value;
                        Debug.Log("Max torque: " + maxTorque);
                        double targetTorqueMagnitude = 0.98 * torqueInputMagnitude * maxTorque;
                        Debug.Log("Target torque: " + maxTorque);
                        targetTorque = targetTorqueMagnitude * torqueDirection / torqueDirection.magnitude;
                        #endregion
                    }

                    //finalise constraints
                    fuelThrustMag.RHS = targetThrust;
                    fuelMomentX.RHS = targetTorque.x;
                    fuelMomentY.RHS = targetTorque.y;
                    fuelMomentZ.RHS = targetTorque.z;
                    Debug.Log("Fuel solver:");
                    fuelSolver.solve();

                    if (fuelSolver.status != CLP.Solvers.Solver.Status.Optimal)
                    {
                        //Debug.Log("Fuel solver failure.");
                        //Debug.Log(string.Format("Direction: ({0}, {1}, {2})", torqueDirection.x, torqueDirection.y, torqueDirection.z));
                        //Debug.Log("magnitude: " + torqueInputMagnitude);
                        stateWorker.status = SharedState.Status.FuelSolverFailure;
                        return;
                    }

                    for (int i = 0; i < stateWorker.staticEngineCount; ++i)
                        stateWorker.staticThrusts[i] = fuelSolver.values[staticEngineVariables[i]];
                    for (int i = 0; i < stateWorker.gimballedEngineCount; ++i)
                    {
                        Vector3d thrust = new Vector3d(
                            fuelSolver.values[gimbalVariables.PX[i]] - fuelSolver.values[gimbalVariables.NX[i]],
                            fuelSolver.values[gimbalVariables.PY[i]] - fuelSolver.values[gimbalVariables.NY[i]],
                            -fuelSolver.values[gimbalVariables.NZ[i]]);
                        double mag = thrust.magnitude;
                        stateWorker.gimballedThrusts[i] = mag;
                        //calculate angles:
                        double rx = Math.Atan(thrust.y / thrust.z);
                        //double ry = Math.Atan(thrust.x / Vector3d.Dot(thrust, new Vector3d(0, thrust.y, thrust.z).normalized));
                        double ry = Math.Atan(thrust.x / (mag * Math.Cos(rx)));
                        stateWorker.gimbalSettings[i].x = (float)(rx * (180.0 / Math.PI));
                        stateWorker.gimbalSettings[i].y = (float)(ry * (180.0 / Math.PI));
                    }
                    stateWorker.status = SharedState.Status.Solved;

                },
                #endregion
                #region Copy output from worker
                () =>
                {
                    if (stateWorker.staticThrusts == null)
                    {
                        stateLocal.staticThrusts = null;
                    }
                    else if (stateLocal.staticThrusts == null || stateWorker.staticThrusts.Count() != stateLocal.staticThrusts.Count() || stateWorker.gimballedThrusts.Count() != stateLocal.gimballedThrusts.Count())
                    {
                        stateLocal.staticThrusts = new double[stateWorker.staticThrusts.Count()];
                        stateLocal.gimballedThrusts = new double[stateWorker.gimballedThrusts.Count()];
                        stateLocal.gimbalSettings = new Vector2[stateWorker.gimbalSettings.Count()];
                    }

                    if (stateWorker.status == SharedState.Status.Solved)
                    {
                        stateWorker.staticThrusts.CopyTo(stateLocal.staticThrusts, 0);
                        stateWorker.gimballedThrusts.CopyTo(stateLocal.gimballedThrusts, 0);
                        stateWorker.gimbalSettings.CopyTo(stateLocal.gimbalSettings, 0);
                    }

                    stateLocal.status = stateWorker.status;
                }
                #endregion
                );

        }
        public override Vessel vessel
        {
            get
            {
                return _vessel;
            }
            set
            {
                _vessel = value;
            }
        }

        public override void onStructuralChange()
        {
            layoutChanged = true;
        }

        public override void onFrame()
        {
            string stage = "update";
            try
            {
                worker.update(resultsAvailable =>
                {
                    stage = "set control input";
                    if (stateLocal == null)
                        stage += ", stateLocal == null";
                    if (_vessel == null)
                        stage += ", vessel == null";
                    //update input state;
                    stateLocal.throttleInput += strength * _vessel.ctrlState.mainThrottle;//-_vessel.transform.InverseTransformDirection(_vessel.ReferenceTransform.rotation * modinput);
                    stateLocal.torqueInput += Vector3.Scale(-_vessel.transform.InverseTransformDirection(_vessel.ReferenceTransform.rotation * new Vector3(vessel.ctrlState.pitch, vessel.ctrlState.roll, vessel.ctrlState.yaw)), torqueStrength);
                    ++stateLocal.frameCount;

                    stateLocal.layoutChanged = layoutChanged;
                    if (layoutChanged)
                    {
                        stage = "layoutChanged";
                        staticEngines = ModuleKCAvionics.getAllModules<ThrottleController>(_vessel).ToArray();
                        stateLocal.staticEngineCount = staticEngines.Count();
                        gimballedEngines = ModuleKCAvionics.getAllModules<GimballedEngineController>(_vessel).ToArray();
                        stateLocal.gimballedEngineCount = gimballedEngines.Count();
                        if (stateLocal.staticConstants == null || stateLocal.staticConstants.Count() != stateLocal.staticEngineCount | stateLocal.gimbalConstants.Count() != stateLocal.gimballedEngineCount)
                        {
                            stateLocal.staticConstants = new StaticEngineConstants[stateLocal.staticEngineCount];
                            stateLocal.staticVariables = new StaticEngineVariables[stateLocal.staticEngineCount];
                            stateLocal.gimbalConstants = new GimballedEngineConstants[stateLocal.gimballedEngineCount];
                            stateLocal.gimbalVariables = new GimballedEngineVariables[stateLocal.gimballedEngineCount];
                        }
                        //update constants
                        for (int i = 0; i < stateLocal.staticEngineCount; ++i)
                        {
                            ModuleEngines engine = staticEngines[i].getEngine();
                            stateLocal.staticConstants[i].maxThrust = engine.maxThrust;
                            stateLocal.staticConstants[i].minThrust = engine.minThrust;
                            int thrustCount = engine.thrustTransforms.Count;
                            stateLocal.staticConstants[i].thrustCount = thrustCount;
                            stateLocal.staticVariables[i].direction = new Vector3[thrustCount];
                            stateLocal.staticVariables[i].position = new Vector3[thrustCount];
                        }
                        for (int i = 0; i < stateLocal.gimballedEngineCount; ++i)
                        {
                            stateLocal.gimbalConstants[i].maxThrust = gimballedEngines[i].engine.maxThrust;
                            stateLocal.gimbalConstants[i].minThrust = gimballedEngines[i].engine.minThrust;
                            stateLocal.gimbalConstants[i].gimbalRange = gimballedEngines[i].gimbal.gimbalRange;
                            int thrustCount = gimballedEngines[i].thrustOriginTransforms.Count();
                            stateLocal.gimbalConstants[i].thrustCount = thrustCount;
                            stateLocal.gimbalVariables[i].orientation = new Quaternion[thrustCount];
                            stateLocal.gimbalVariables[i].position = new Vector3[thrustCount];
                        }
                        layoutChanged = false;
                    }
                    stage = "variable update";
                    //update variables
                    for (int i = 0; i < stateLocal.staticEngineCount; ++i)
                    {
                        BaseThrottleController m = staticEngines[i];
                        int thrustCount = stateLocal.staticConstants[i].thrustCount;
                        for (int j = 0; j < thrustCount; ++j)
                        {
                            //Transform t = m.engine.thrustTransforms[j];
                            //stateLocal.staticVariables[i].direction[j] = vessel.transform.InverseTransformDirection(-t.forward);
                            //stateLocal.staticVariables[i].position[j] = vessel.transform.InverseTransformPoint(t.position);
                            stateLocal.staticVariables[i].direction[j] = vessel.transform.InverseTransformDirection(m.getDirection(j));
                            stateLocal.staticVariables[i].position[j] = vessel.transform.InverseTransformPoint(m.getPosition(j));
                        }
                        ModuleEngines engine = m.getEngine();
                        stateLocal.staticVariables[i].maxFlow = engine.realIsp > 0 ? engine.maxThrust / (engine.realIsp * engine.G) : 0.0f;
                        stateLocal.staticVariables[i].operable = m.operational;
                    }
                    for (int i = 0; i < stateLocal.gimballedEngineCount; ++i)
                    {
                        GimballedEngineController m = gimballedEngines[i];
                        int thrustCount = stateLocal.gimbalConstants[i].thrustCount;
                        for (int j = 0; j < thrustCount; ++j)
                        {
                            Transform t = m.thrustOriginTransforms[j];
                            stateLocal.gimbalVariables[i].orientation[j] = Quaternion.Inverse(vessel.transform.rotation) *  t.rotation;
                            stateLocal.gimbalVariables[i].position[j] = vessel.transform.InverseTransformPoint(t.position);
                        }
                        stateLocal.gimbalVariables[i].maxFlow = m.engine.realIsp > 0 ? m.engine.maxThrust / (m.engine.realIsp * m.engine.G) : 0.0f;
                        stateLocal.gimbalVariables[i].operable = m.operational;
                    }
                    stateLocal.COM = vessel.transform.InverseTransformPoint(vessel.CoM + vessel.rb_velocity * Time.deltaTime);

                    //update output from output state
                    if (resultsAvailable)
                    {
                        stage = "update thrusts";
                        status = stateLocal.status;
                        if (stateLocal.status == SharedState.Status.Solved)
                        {
                            for (int i = 0; i < stateLocal.staticEngineCount; ++i)
                            {
                                BaseThrottleController module = staticEngines[i];
                                if (module.operational)
                                    module.overrideThrottle = (float)stateLocal.staticThrusts[i];
                                else
                                    module.overrideThrottle = 0.0f;
                            }
                            for (int i = 0; i < stateLocal.gimballedEngineCount; ++i)
                            {
                                GimballedEngineController module = gimballedEngines[i];
                                if (module.operational)
                                {
                                    module.overrideThrottle = (float)stateLocal.gimballedThrusts[i];
                                    if (module.overrideThrottle > 1E-5)
                                    {
                                        module.overrideAngleX = (float)stateLocal.gimbalSettings[i].x / stateLocal.gimbalConstants[i].gimbalRange;
                                        module.overrideAngleY = (float)stateLocal.gimbalSettings[i].y / stateLocal.gimbalConstants[i].gimbalRange;
                                    }
                                    else
                                    {
                                        module.overrideAngleX = 0.0f;
                                        module.overrideAngleY = 0.0f;
                                    }
                                }
                                else
                                {
                                    module.overrideThrottle = 0.0f;
                                    module.overrideAngleX = 0.0f;
                                    module.overrideAngleY = 0.0f;
                                }
                            }
                        }
                        else
                        {
                            for (int i = 0; i < stateLocal.staticEngineCount; ++i)
                            {
                                BaseThrottleController module = staticEngines[i];
                                module.overrideThrottle = 0.0f;
                            }
                            for (int i = 0; i < stateLocal.gimballedEngineCount; ++i)
                            {
                                GimballedEngineController module = gimballedEngines[i];
                                module.overrideThrottle = 0.0f;
                                module.overrideAngleX = 0.0f;
                                module.overrideAngleY = 0.0f;
                            }
                        }
                    }
                    return true;

                });
            }
            catch (Exception)
            {
                Debug.Log("EngineMode2 update: Error in stage: " + stage);
                throw;
            }
        }

        public override void onGUI()
        {
            string statusText = "Unknown";
            switch (status)
            {
                case SharedState.Status.Solved:
                    statusText = "Nominal";
                    break;
                case SharedState.Status.Infeasible:
                    statusText = "Infeasible state";
                    break;
                case SharedState.Status.InputIsZero:
                    statusText = "No input.";
                    break;
                case SharedState.Status.FuelSolverFailure:
                    statusText = "Internal Error 01";
                    break;
                case SharedState.Status.ThrustSolverFailure:
                    statusText = "Internal Error 02";
                    break;
                case SharedState.Status.TorqueSolverFailure:
                    statusText = "Internal Error 03";
                    break;
            }
            if (worker != null)
                GUILayout.Label(worker.running ? "Active" : "Inactive");
            if (staticEngines != null)
                GUILayout.Label("Engines controlled: " + (staticEngines.Count() + gimballedEngines.Count()));
            GUILayout.Label("Status: " + statusText);

            GUILayout.Label("Throttle Strength:");
            strength = GUILayout.HorizontalSlider(strength, 0.0f, 1.0f);
            GUILayout.Label("Torque Sensitivity:");
            GUILayout.BeginHorizontal();
            GUILayout.Label("Pitch:");
            torqueStrength.x = GUILayout.HorizontalSlider(torqueStrength.x, 0.0f, 1.0f);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Roll:");
            torqueStrength.y = GUILayout.HorizontalSlider(torqueStrength.y, 0.0f, 1.0f);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Yaw:");
            torqueStrength.z = GUILayout.HorizontalSlider(torqueStrength.z, 0.0f, 1.0f);
            GUILayout.EndHorizontal();
        }

        public override void start()
        {
            worker.start();
        }

        public override void stop()
        {
            worker.stop();
            worker.reset();
            layoutChanged = true;
        }

        public override bool running
        {
            get { return worker.running; }
        }

        public override void Load(ConfigNode node)
        {
            strength = float.Parse(node.GetValue("strength"));
            ConfigNode torqueStrength_node = node.GetNode("torqueStrength");
            if (torqueStrength_node != null)
            {
                torqueStrength.x = float.Parse(torqueStrength_node.GetValue("x"));
                torqueStrength.y = float.Parse(torqueStrength_node.GetValue("y"));
                torqueStrength.z = float.Parse(torqueStrength_node.GetValue("z"));
            }
        }

        public override void Save(ConfigNode node)
        {
            node.AddValue("strength", strength);
            ConfigNode torqueStrength_node = node.AddNode("torqueStrength");
            torqueStrength_node.AddValue("x", torqueStrength.x);
            torqueStrength_node.AddValue("y", torqueStrength.y);
            torqueStrength_node.AddValue("z", torqueStrength.z);
        }

        public override IEnumerable<PartEffectorModule> getDesiredEffectors()
        {

            return _vessel.parts.SelectMany(p => p.Modules.Cast<PartModule>().Where(m => m is ThrottleController || m is GimballedEngineController).Cast<PartEffectorModule>());
        }
    }
}

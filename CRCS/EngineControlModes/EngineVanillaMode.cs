﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
namespace KerbCom.EngineControlModes
{
    [ControlMode.Details(guiName = "Stock engine control", guiHelp =
@"Stock engine control:

This mode behaves identically to normal engine control, with each engine following the master throttle. The exception is that this mode also provides a strength multiplier. This is useful for avoiding overheating, for example."
        )]
    class EngineVanillaMode : ControlMode
    {
        bool active = false;
        private Vessel _vessel;
        float multiplier = 1.0f;

        BaseThrottleController[] throttles = new BaseThrottleController[0];

        public override Vessel vessel
        {
            get
            {
                return _vessel;
            }
            set
            {
                _vessel = value;
            }
        }

        public override void Load(ConfigNode node)
        {
            multiplier = float.Parse(node.GetValue("multiplier"));
        }

        public override void Save(ConfigNode node)
        {
            node.AddValue("multiplier", multiplier.ToString());
        }

        public override void onStructuralChange()
        {
            throttles = ModuleKCAvionics.getAllModules<BaseThrottleController>(_vessel).ToArray();
        }

        public override void onFrame()
        {
            if (active)
            {
                foreach (BaseThrottleController engine in throttles)
                {
                    engine.overrideThrottle = _vessel.ctrlState.mainThrottle * multiplier;
                }
            }
        }

        public override void onGUI()
        {
            GUILayout.Label(active ? "Active" : "Inactive");
            GUILayout.Label("Engines controlled: " + throttles.Count());
            GUILayout.Label("Strength:");
            multiplier = GUILayout.HorizontalSlider(multiplier, 0.0f, 1.0f);
            GUILayout.Label(String.Format("{0}%", (int)(multiplier * 100)));
        }

        public override void start()
        {
            active = true;
        }

        public override bool running
        {
            get { return active; }
        }

        public override void stop()
        {
            active = false;
        }

        public override IEnumerable<PartEffectorModule> getDesiredEffectors()
        {
            return ModuleKCAvionics.getAllModules<BaseThrottleController>(_vessel).Cast<PartEffectorModule>();
        }
    }
}

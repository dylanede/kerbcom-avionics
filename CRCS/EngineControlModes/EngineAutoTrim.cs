﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace KerbCom.EngineControlModes
{
    [ControlMode.Details(guiName = "Engine Auto-Trim", guiHelp = 
@"Engine Auto-Trim:

This mode is the current solution for asymmetric lifters, VTOL craft and other rocket-powered contraptions. It balances thrust and provides differential thrust steering using the main engines. It also ensures that the most fuel efficient combination of individual throttles is used to match the master throttle.

This mode does not currently support Solid Rocket Boosters, or other engines with a minimum active thrust setting. This will be rectified in an upcoming update.

It also does not yet make use of engine gimbals to provide balance. This means that several common designs, such as dual engine, single engine or Space Shuttle-style designs are not yet supported by this plugin. This will be rectified in another version.

This mode works by maximising thrust in the weighted average direction of all the engines on the vessel. It will therefore work as expected most of the time, regardless of command pod orientation."
        )]
    class EngineAutoTrim : ControlMode
    {
        private Vessel _vessel;
        private IIterativeWorker worker;

        ThrottleController[] throttles = new ThrottleController[0];

        private struct EngineConstants
        {
            public float minThrust, maxThrust;
            public int thrustCount;
        }

        private struct EngineVariables
        {
            public float maxFlow;
            public Vector3[] direction, position;
            public bool operable;
        }

        private class SharedState
        {
            //input
            public int engineCount = 0;
            public EngineConstants[] constants;
            public EngineVariables[] variables;
            public Vector3 COM;
            public float throttleInput = 0.0f;
            public Vector3 torqueInput = Vector3.zero;
            public int frameCount = 0;
            public bool layoutChanged = true;
            //output
            public double[] results;
            public enum Status
            {
                InputIsZero,
                Infeasible,
                ThrustSolverFailure,
                TorqueSolverFailure,
                FuelSolverFailure,
                Solved
            }
            public Status status;
        }

        SharedState stateLocal = new SharedState(), stateWorker = new SharedState();

        //local private state for the GUI
        private SharedState.Status status;
        private float strength = 1.0f;
        private Vector3 torqueStrength = Vector3.one;

        //worker private state:
        CLP.Constraint thrustMomentX, thrustMomentY, thrustMomentZ;
        CLP.Constraint torqueThrustMag;
        CLP.Constraint torqueMomentA1, torqueMomentA2;
        CLP.Constraint fuelThrustMag;
        CLP.Constraint fuelMomentX, fuelMomentY, fuelMomentZ;
        CLP.Constraint[] fuelMoments;
        CLP.Problem fuelProblem, thrustProblem, torqueProblem;
        CLP.Solvers.Solver fuelSolver, thrustSolver, torqueSolver;
        CLP.BoundedVariable[] engineVariables;

        int lastEngineCount = 0;
        private bool layoutChanged = true;

        public EngineAutoTrim()
        {
            worker = new SequentialIterativeWorker(
                () =>
                {
                    //copy input state
                    if (stateLocal.layoutChanged)
                    {

                        if (stateLocal.constants == null)
                            stateWorker.constants = null;
                        else
                        {
                            if (stateWorker.constants == null || stateLocal.constants.Count() != stateWorker.constants.Count())
                                stateWorker.constants = new EngineConstants[stateLocal.constants.Count()];
                            stateLocal.constants.CopyTo(stateWorker.constants, 0);
                        }
                    }
                    if (stateLocal.variables == null)
                        stateWorker.variables = null;
                    else
                    {
                        if (stateWorker.variables == null || stateLocal.variables.Count() != stateWorker.variables.Count())
                            stateWorker.variables = new EngineVariables[stateLocal.variables.Count()];
                        stateLocal.variables.CopyTo(stateWorker.variables, 0);
                    }
                    stateWorker.COM = stateLocal.COM;
                    stateWorker.throttleInput = stateLocal.throttleInput;
                    stateWorker.torqueInput = stateLocal.torqueInput;
                    stateWorker.frameCount = stateLocal.frameCount;
                    stateWorker.layoutChanged = stateLocal.layoutChanged;
                    stateWorker.engineCount = stateLocal.engineCount;
                    stateLocal.throttleInput = 0.0f;
                    stateLocal.torqueInput = Vector3.zero;
                    stateLocal.frameCount = 0;
                    stateLocal.layoutChanged = false;
                },
                () =>
                {
                    if (stateWorker.engineCount != lastEngineCount || fuelSolver == null)
                    {
                        stateWorker.results = new double[stateWorker.engineCount];

                        engineVariables = new CLP.BoundedVariable[stateWorker.engineCount];
                        for (int i = 0; i < stateWorker.engineCount; ++i)
                        {
                            engineVariables[i] = new CLP.BoundedVariable(0, 1);
                        }

                        fuelProblem = new CLP.Problem();
                        fuelProblem.constraints.AddRange(new CLP.Constraint[]{
                            fuelThrustMag = new CLP.Constraint(),
                            fuelMomentX = new CLP.Constraint(),
                            fuelMomentY = new CLP.Constraint(),
                            fuelMomentZ = new CLP.Constraint()
                        });
                        fuelMoments = new CLP.Constraint[] { fuelMomentX, fuelMomentY, fuelMomentZ };

                        thrustProblem = new CLP.Problem();
                        thrustProblem.constraints.AddRange(new CLP.Constraint[]{
                            thrustMomentX = fuelMomentX.shallow_clone(),
                            thrustMomentY = fuelMomentY.shallow_clone(),
                            thrustMomentZ = fuelMomentZ.shallow_clone()
                        });
                        thrustMomentX.RHS = 0.0;
                        thrustMomentY.RHS = 0.0;
                        thrustMomentZ.RHS = 0.0;

                        torqueProblem = new CLP.Problem();
                        torqueProblem.constraints.AddRange(new CLP.Constraint[]{
                            torqueThrustMag = fuelThrustMag.shallow_clone(),
                            torqueMomentA1 = new CLP.Constraint(),
                            torqueMomentA2 = new CLP.Constraint()
                        });
                        torqueMomentA1.RHS = 0.0;
                        torqueMomentA2.RHS = 0.0;

                        thrustSolver = new CLP.Solvers.MaxPrimalSimplex(thrustProblem, engineVariables);
                        torqueSolver = new CLP.Solvers.MaxPrimalSimplex(torqueProblem, engineVariables);
                        fuelSolver = new CLP.Solvers.MaxPrimalSimplex(fuelProblem, engineVariables);

                        lastEngineCount = stateWorker.engineCount;
                    }
                    float targetThrottle = Mathf.Clamp(stateWorker.throttleInput / stateWorker.frameCount, 0.0f, 1.0f);
                    Vector3 thrustDirection = Vector3.zero;
                    for (int i = 0; i < stateWorker.engineCount; ++i)
                    {
                        if (stateWorker.variables[i].operable)
                        {
                            int thrustCount = stateWorker.constants[i].thrustCount;
                            float thrust = stateWorker.constants[i].maxThrust / thrustCount;
                            for (int j = 0; j < thrustCount; ++j)
                            {
                                thrustDirection += thrust * stateWorker.variables[i].direction[j];
                            }
                        }
                    }
                    thrustDirection.Normalize();
                    Vector3 thrustInput = thrustDirection * targetThrottle;

                    // torque solver dependencies
                    bool torqueEnabled = false;
                    Vector3 torqueInput = stateWorker.torqueInput / stateWorker.frameCount;

                    float torqueInputMagnitude = torqueInput.magnitude;

                    Vector3d torqueDirection = Vector3d.zero;
                    int i_axis = 0, d_axis1 = 0, d_axis2 = 0;
                    double d_coeff1 = 0, d_coeff2 = 0;
                    if (torqueInputMagnitude > 1E-4)
                    {
                        torqueEnabled = true;
                        torqueDirection = torqueInput / torqueInputMagnitude;
                        if (torqueInputMagnitude > 1.0f) torqueInputMagnitude = 1.0f;

                        // find best axis to use as measure - largest coefficient
                        Vector3d magDir = new Vector3d(Math.Abs(torqueDirection.x), Math.Abs(torqueDirection.y), Math.Abs(torqueDirection.z));
                        i_axis = magDir.x > magDir.y ? (magDir.x > magDir.z ? 0 : 2) : (magDir.y > magDir.z ? 1 : 2);
                        // other axes
                        d_axis1 = (i_axis + 1) % 3;
                        d_axis2 = (i_axis + 2) % 3;
                        d_coeff1 = torqueDirection[d_axis1] / torqueDirection[i_axis];
                        d_coeff2 = torqueDirection[d_axis2] / torqueDirection[i_axis];
                    }
#if DEBUG

                    bool check = torqueDirection.x == 0.0f || torqueDirection.y == 0.0f || torqueDirection.z == 0.0f;
                    if (check)
                    {
                        Debug.Log(string.Format("Direction: ({0}, {1}, {2})", torqueDirection.x, torqueDirection.y, torqueDirection.z));
                        Debug.Log("magnitude: " + torqueInputMagnitude);
                        Debug.Log("i_axis: " + i_axis);
                        Debug.Log("d_axis1: " + d_axis1);
                        Debug.Log("d_axis2: " + d_axis2);
                    }
                    Debug.Log("thrustDirection: " + thrustDirection.ToString());
                    Debug.Log("stateWorker.thrustInput: " + stateWorker.thrustInput.ToString());
                    Debug.Log("stateWorker.frameCount: " + stateWorker.frameCount);
                    Debug.Log("thrustInput: " + thrustInput.ToString());
#endif
                    //don't bother if input is 0
                    if (targetThrottle < 1E-6)
                    {
                        stateWorker.status = SharedState.Status.InputIsZero;
                        return;
                    }

                    for (int i = 0; i < stateWorker.engineCount; ++i)
                    {
                        CLP.BoundedVariable var = engineVariables[i];

                        //fuel cost
                        fuelProblem.objective[var] = -stateWorker.variables[i].maxFlow;

                        fuelProblem.objective[var] = -stateWorker.variables[i].maxFlow;
                        if (stateWorker.variables[i].operable)
                        {
                            double mag = 0.0;
                            double mX = 0.0, mY = 0.0, mZ = 0.0;
                            
                            int thrustCount = stateWorker.constants[i].thrustCount;
                            float thrust = stateWorker.constants[i].maxThrust / thrustCount;
                            for (int j = 0; j < thrustCount; ++j)
                            {
                                Vector3 force = thrust * stateWorker.variables[i].direction[j];
                                Vector3 offset = stateWorker.variables[i].position[j] - stateWorker.COM;
                                Vector3 moment = Vector3.Cross(offset, force);
                                mag += Vector3.Dot(force, thrustDirection);
                                mX += moment.x;
                                mY += moment.y;
                                mZ += moment.z;
                            }
                            fuelThrustMag.f[var] = mag;
                            fuelMomentX.f[var] = mX;
                            fuelMomentY.f[var] = mY;
                            fuelMomentZ.f[var] = mZ;
                        }
                        else
                        {
                            fuelThrustMag.f[var] = 0.0;
                            fuelMomentX.f[var] = 0.0;
                            fuelMomentY.f[var] = 0.0;
                            fuelMomentZ.f[var] = 0.0;
                        }
                        //thrust in direction
                        thrustProblem.objective[var] = fuelThrustMag.f[var];

                        if (torqueEnabled)
                        {
                            torqueProblem.objective[var] = Vector3d.Dot(
                                new Vector3d(
                                    fuelMomentX.f[var],
                                    fuelMomentY.f[var],
                                    fuelMomentZ.f[var]),
                                torqueDirection);
                            torqueMomentA1.f[var] = fuelMoments[d_axis1].f[var] - d_coeff1 * fuelMoments[i_axis].f[var];
                            torqueMomentA2.f[var] = fuelMoments[d_axis2].f[var] - d_coeff2 * fuelMoments[i_axis].f[var];
                        }

                    }

                    thrustSolver.solve();

                    if (thrustSolver.status != CLP.Solvers.Solver.Status.Optimal)
                    {
                        stateWorker.status = SharedState.Status.ThrustSolverFailure;
                        return;
                    }
                    double maxThrust = thrustSolver.objective_value;
                    double targetThrust = maxThrust * targetThrottle * 0.98;

                    if (targetThrust < double.Epsilon * 10)
                    {
                        stateWorker.status = SharedState.Status.Infeasible;
                        return;
                    }

                    Vector3d targetTorque = Vector3d.zero;
                    double maxTorque = 0.0;
                    //find the range of rotation possible:
                    if (torqueEnabled)
                    {
                        torqueThrustMag.RHS = targetThrust;

                        torqueSolver.solve();
                        if (torqueSolver.status != CLP.Solvers.Solver.Status.Optimal)
                        {
                            stateWorker.status = SharedState.Status.TorqueSolverFailure;
                            return;
                        }
                        maxTorque = torqueSolver.objective_value;
                        //Debug.Log("Max torque: " + maxTorque);
                        double targetTorqueMagnitude = 0.5 * torqueInputMagnitude * maxTorque;
                        targetTorque = targetTorqueMagnitude * torqueDirection / torqueDirection.magnitude;
                    }

                    //finalise constraints
                    fuelThrustMag.RHS = targetThrust;
                    fuelMomentX.RHS = targetTorque.x;
                    fuelMomentY.RHS = targetTorque.y;
                    fuelMomentZ.RHS = targetTorque.z;

                    fuelSolver.solve();
                    
                    if (fuelSolver.status != CLP.Solvers.Solver.Status.Optimal)
                    {
                        //Debug.Log("Fuel solver failure.");
                        //Debug.Log(string.Format("Direction: ({0}, {1}, {2})", torqueDirection.x, torqueDirection.y, torqueDirection.z));
                        //Debug.Log("magnitude: " + torqueInputMagnitude);
                        stateWorker.status = SharedState.Status.FuelSolverFailure;
                        return;
                    }

                    for (int i = 0; i < stateWorker.engineCount; ++i)
                        stateWorker.results[i] = fuelSolver.values[engineVariables[i]];
                    stateWorker.status = SharedState.Status.Solved;

                },
                () =>
                {
                    if (stateWorker.results == null)
                        stateLocal.results = null;
                    else if (stateLocal.results == null || stateWorker.results.Count() != stateLocal.results.Count())
                        stateLocal.results = new double[stateWorker.results.Count()];
                    if (stateWorker.status == SharedState.Status.Solved)
                        stateWorker.results.CopyTo(stateLocal.results, 0);

                    stateLocal.status = stateWorker.status;
                });

        }
        public override Vessel vessel
        {
            get
            {
                return _vessel;
            }
            set
            {
                _vessel = value;
            }
        }

        public override void onStructuralChange()
        {
            layoutChanged = true;
        }

        public override void onFrame()
        {
            string stage = "update";
            try
            {
                worker.update(resultsAvailable =>
                {
                    stage = "set control input";
                    if (stateLocal == null)
                        stage += ", stateLocal == null";
                    if (_vessel == null)
                        stage += ", vessel == null";
                    //update input state;
                    stateLocal.throttleInput += strength * _vessel.ctrlState.mainThrottle;//-_vessel.transform.InverseTransformDirection(_vessel.ReferenceTransform.rotation * modinput);
                    stateLocal.torqueInput += Vector3.Scale(-_vessel.transform.InverseTransformDirection(_vessel.ReferenceTransform.rotation * new Vector3(vessel.ctrlState.pitch, vessel.ctrlState.roll, vessel.ctrlState.yaw)), torqueStrength);
                    ++stateLocal.frameCount;

                    stateLocal.layoutChanged = layoutChanged;
                    if (layoutChanged)
                    {
                        stage = "layoutChanged";
                        throttles = ModuleKCAvionics.getAllModules<ThrottleController>(_vessel).ToArray();
                        stateLocal.engineCount = throttles.Count();
                        if (stateLocal.constants == null || stateLocal.constants.Count() != stateLocal.engineCount)
                        {
                            stateLocal.constants = new EngineConstants[stateLocal.engineCount];
                            stateLocal.variables = new EngineVariables[stateLocal.engineCount];
                        }
                        //update constants
                        for (int i = 0; i < stateLocal.engineCount; ++i)
                        {
                            stateLocal.constants[i].maxThrust = throttles[i].engine.maxThrust;
                            stateLocal.constants[i].minThrust = throttles[i].engine.minThrust;
                            int thrustCount = throttles[i].engine.thrustTransforms.Count;
                            stateLocal.constants[i].thrustCount = thrustCount;
                            stateLocal.variables[i].direction = new Vector3[thrustCount];
                            stateLocal.variables[i].position = new Vector3[thrustCount];
                        }
                        layoutChanged = false;
                    }
                    stage = "variable update";
                    //update variables
                    for (int i = 0; i < stateLocal.engineCount; ++i)
                    {
                        ThrottleController m = throttles[i];
                        int thrustCount = stateLocal.constants[i].thrustCount;
                        for (int j = 0; j < thrustCount; ++j)
                        {
                            Transform t = m.engine.thrustTransforms[j];
                            stateLocal.variables[i].direction[j] = vessel.transform.InverseTransformDirection(-t.forward);
                            stateLocal.variables[i].position[j] = vessel.transform.InverseTransformPoint(t.position);
                        }
                        stateLocal.variables[i].maxFlow = m.engine.realIsp > 0 ? m.engine.maxThrust / (m.engine.realIsp * m.engine.G) : 0.0f;
                        stateLocal.variables[i].operable = m.operational;
                    }
                    stateLocal.COM = vessel.transform.InverseTransformPoint(vessel.CoM + vessel.rb_velocity * Time.deltaTime);

                    //update output from output state
                    if (resultsAvailable)
                    {
                        stage = "update thrusts";
                        status = stateLocal.status;
                        if (stateLocal.status == SharedState.Status.Solved)
                            for (int i = 0; i < stateLocal.engineCount; ++i)
                            {
                                ThrottleController module = throttles[i];
                                if (module.operational)
                                    module.overrideThrottle = (float)stateLocal.results[i];
                                else
                                    module.overrideThrottle = 0.0f;
                            }
                        else
                            for (int i = 0; i < stateLocal.engineCount; ++i)
                            {
                                ThrottleController module = throttles[i];
                                module.overrideThrottle = 0.0f;
                            }
                    }
                    return true;

                });
            }
            catch (Exception)
            {
                Debug.Log("EngineMode2 update: Error in stage: " + stage);
                throw;
            }
        }

        public override void onGUI()
        {
            string statusText = "Unknown";
            switch (status)
            {
                case SharedState.Status.Solved:
                    statusText = "Nominal";
                    break;
                case SharedState.Status.Infeasible:
                    statusText = "Infeasible state";
                    break;
                case SharedState.Status.InputIsZero:
                    statusText = "No input.";
                    break;
                case SharedState.Status.FuelSolverFailure:
                    statusText = "Internal Error 01";
                    break;
                case SharedState.Status.ThrustSolverFailure:
                    statusText = "Internal Error 02";
                    break;
                case SharedState.Status.TorqueSolverFailure:
                    statusText = "Internal Error 03";
                    break;
            }
            if (worker != null)
                GUILayout.Label(worker.running ? "Active" : "Inactive");
            if (throttles != null)
                GUILayout.Label("Engines controlled: " + throttles.Count());
            GUILayout.Label("Status: " + statusText);

            GUILayout.Label("Throttle Strength:");
            strength = GUILayout.HorizontalSlider(strength, 0.0f, 1.0f);
            GUILayout.Label("Torque Sensitivity:");
            GUILayout.BeginHorizontal();
            GUILayout.Label("Pitch:");
            torqueStrength.x = GUILayout.HorizontalSlider(torqueStrength.x, 0.0f, 1.0f);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Roll:");
            torqueStrength.y = GUILayout.HorizontalSlider(torqueStrength.y, 0.0f, 1.0f);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Yaw:");
            torqueStrength.z = GUILayout.HorizontalSlider(torqueStrength.z, 0.0f, 1.0f);
            GUILayout.EndHorizontal();
        }

        public override void start()
        {
            worker.start();
        }

        public override void stop()
        {
            worker.stop();
            worker.reset();
            layoutChanged = true;
        }

        public override bool running
        {
            get { return worker.running; }
        }

        public override void Load(ConfigNode node)
        {
            strength = float.Parse(node.GetValue("strength"));
            ConfigNode torqueStrength_node = node.GetNode("torqueStrength");
            if (torqueStrength_node != null)
            {
                torqueStrength.x = float.Parse(torqueStrength_node.GetValue("x"));
                torqueStrength.y = float.Parse(torqueStrength_node.GetValue("y"));
                torqueStrength.z = float.Parse(torqueStrength_node.GetValue("z"));
            }
        }

        public override void Save(ConfigNode node)
        {
            node.AddValue("strength", strength);
            ConfigNode torqueStrength_node = node.AddNode("torqueStrength");
            torqueStrength_node.AddValue("x", torqueStrength.x);
            torqueStrength_node.AddValue("y", torqueStrength.y);
            torqueStrength_node.AddValue("z", torqueStrength.z);
        }

        public override IEnumerable<PartEffectorModule> getDesiredEffectors()
        {
            return ModuleKCAvionics.getAllModules<ThrottleController>(_vessel).Cast<PartEffectorModule>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;
namespace KerbCom
{
    /// <summary>
    /// Provides a common framework for performing iterative work asynchronously, with both pre- and post- work synchronisation callbacks.
    /// The main thread signals the worker that new work is available using signal().
    /// It then can check for new results and process them or wait for results using update().
    /// </summary>
    class ParallelIterativeWorker : IIterativeWorker
    {
        private Thread thread;
        private bool shouldStop = false;
        private bool resultsAvailable = false;
        private bool dataAvailable = false;
        private Action prework;
        private Action work;
        private Action postwork;

        private void loop()
        {
            Debug.Log("Thread starting");
            string stage = "loop";
            try
            {
                while (!shouldStop)
                {
                    stage = "loop";
                    lock (this)
                    {
                        if (!dataAvailable)
                            Monitor.Wait(this);
                        if (shouldStop)
                            break;
                        stage = "prework";
                        prework();
                    }
                    stage = "work";
                    work();
                    lock (this)
                    {
                        stage = "postwork";
                        postwork();
                        resultsAvailable = true;
                        Monitor.Pulse(this);
                    }
                }
            }
            catch(Exception e)
            {
                Debug.Log("Thread Exception at stage " + stage  + ":\n" + e.ToString());
            }
        }

        /// <summary>
        /// Synchronously checks if the worker thread has iterated since the last processed result.
        /// It then calls updater with this value before letting the worker thread continue.
        /// If the result of updater is true, signal() is called to start a new iteration.
        /// </summary>
        /// <param name="updater">The synchronous action to perform. It is passed true if the worker has iterated since the last processed update.
        /// It should return true if it requires the worker to perform an iteration.</param>
        /// <param name="wait">If true, this thread waits until the worker has completed an iteration.</param>
        /// <returns>Whether an update was available.</returns>
        public bool update(Func<bool,bool> updater, bool wait = false)
        {
            lock (this)
            {
                if (wait && !resultsAvailable)
                    Monitor.Wait(this);
                if (updater(resultsAvailable))
                    signal();
                bool result = resultsAvailable;
                resultsAvailable = false;
                return result;
            }
        }
        /// <summary>
        /// Constructs a new unstarted IterativeWorker.
        /// </summary>
        /// <param name="prework">In an iteration of the worker thread, the first action to be called. It is thread-safe.</param>
        /// <param name="work">In an iteration of the worker thread, the second action to be called. It is asynchronous and thread-unsafe.</param>
        /// <param name="postwork">In an iteration of the worker thread, the last action to be called. It is thread-safe.</param>
        public ParallelIterativeWorker(Action prework, Action work, Action postwork)
        {
            this.prework = prework;
            this.work = work;
            this.postwork = postwork;
            thread = new Thread(new ThreadStart(loop));
        }
        /// <summary>
        /// Notify thread that new data to work on is available so that it can iterate again.
        /// </summary>
        public void signal()
        {
            lock (this)
            {
                dataAvailable = true;
                Monitor.Pulse(this);
            }
        }
        /// <summary>
        /// Start the worker thread. It will not actually perform work until signal() is called.
        /// </summary>
        public void start()
        {
            shouldStop = false;
            if (!thread.IsAlive)
            {
                if (thread.ThreadState != ThreadState.Unstarted)
                {
                    Debug.Log("Creating new thread.");
                    thread = new Thread(new ThreadStart(loop));
                }
                Debug.Log("Starting thread.");
                thread.Start();
            }
        }
        /// <summary>
        /// Resets the status of the worker before a call to start(). This means that past signal() and update() calls will not pass on to
        /// later iterations/updates.
        /// </summary>
        public void reset()
        {
            if (!thread.IsAlive)
            {
                dataAvailable = false;
                resultsAvailable = false;
            }
            else
                throw new Exception(this.GetType().Name + ".reset should not be called when the worker is running.");
        }

        public bool running
        {
            get
            {
                return thread.IsAlive;
            }
        }

        /// <summary>
        /// Stops the worker thread as soon as possible. This is either after the current iteration is complete,
        /// or if the worker is currently waiting for work, immediately. Either way, this function returns when
        /// the worker has stopped.
        /// </summary>
        public void stop()
        {
            lock (this)
            {
                shouldStop = true;
                Monitor.Pulse(this); // in case the worker is waiting
            }
            thread.Join();
        }
    }
}

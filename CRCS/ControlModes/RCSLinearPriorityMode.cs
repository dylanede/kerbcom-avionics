﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace KerbCom.RCSControlModes
{
    [ControlMode.Details(guiName = "RCS Linear priority control", guiHelp =
@"RCS Linear priority control:

This modes behaves very similarly to ""RCS Linear control"" (see the help for that mode for more details), except that it also responds to angular input (Q, E, W, A, S, D keys) to provide accurate torque control that does not induce translation as a side effect.

This particular version ""Linear priority"", will, if it has to make a choice, bias towards translation rather than rotation when balancing the inputs. This means that attempting to rotate while translating will not decrease effective translation acceleration, but attempting to translate while rotating may decrease the effective angular acceleration if necessary."
    )]
    class RCSLinearPriorityMode : ControlMode
    {
        private Vessel _vessel;
        private IIterativeWorker worker;

        private struct PortRef
        {
            public int index;
            public MyRCSModule module;
        }

        PortRef[] portRefs;
        ReactionWheelController[] wheels;

        private struct PortConstants
        {
            public float maxThrust;
        }
        private struct PortVariables
        {
            public float maxFlow;
            public Vector3 direction, position;
            public bool operable;
        }

        private class SharedState
        {
            //input
            public int portCount = 0;
            public PortConstants[] constants;
            public PortVariables[] variables;
            public Vector3 wheelStrength;
            public Vector3 COM;
            public Vector3 linearInput = Vector3.zero;
            public Vector3 angularInput = Vector3.zero;
            public int frameCount = 0; //used to average accumulated input
            public bool layoutChanged = true;
            //output
            public Vector3 wheelResult;
            public double[] results;
            public enum Status
            {
                InputIsZero,
                Infeasible,
                ThrustSolverFailure,
                FuelSolverFailure,
                Solved,
                TorqueSolverFailure
            }
            public Status status;
        }

        SharedState stateLocal = new SharedState(), stateWorker = new SharedState();

        //local private state for the GUI
        private SharedState.Status status;
        private Vector3 linearStrength = Vector3.one;
        private Vector3 angularStrength = Vector3.one;

        //worker private state:
        CLP.Constraint thrustForceA1, thrustForceA2;
        CLP.Constraint thrustMomentX, thrustMomentY, thrustMomentZ;
        CLP.Constraint torqueForceX, torqueForceY, torqueForceZ;
        CLP.Constraint torqueMomentA1, torqueMomentA2;
        CLP.Constraint fuelForceX, fuelForceY, fuelForceZ;
        CLP.Constraint fuelMomentX, fuelMomentY, fuelMomentZ;
        CLP.Constraint[] fuelForces, fuelMoments;
        CLP.Problem fuelProblem, thrustProblem, torqueProblem;
        CLP.Solvers.Solver fuelSolver, thrustSolver, torqueSolver;
        CLP.BoundedVariable[] portVariables;
        CLP.BoundedVariable wheelVarX, wheelVarY, wheelVarZ;
        IEnumerable<CLP.BoundedVariable> allVariables;
        int lastPortCount = 0;
        private bool layoutChanged = true;

        public RCSLinearPriorityMode()
        {
            worker = new SequentialIterativeWorker(
                () =>
                {
                    //copy input state
                    if (stateLocal.layoutChanged)
                    {

                        if (stateLocal.constants == null)
                            stateWorker.constants = null;
                        else
                        {
                            if (stateWorker.constants == null || stateLocal.constants.Count() != stateWorker.constants.Count())
                                stateWorker.constants = new PortConstants[stateLocal.constants.Count()];
                            stateLocal.constants.CopyTo(stateWorker.constants, 0);
                            stateWorker.wheelStrength = stateLocal.wheelStrength;
                        }
                    }
                    if (stateLocal.variables == null)
                        stateWorker.variables = null;
                    else
                    {
                        if (stateWorker.variables == null || stateLocal.variables.Count() != stateWorker.variables.Count())
                            stateWorker.variables = new PortVariables[stateLocal.variables.Count()];
                        stateLocal.variables.CopyTo(stateWorker.variables, 0);
                    }
                    stateWorker.COM = stateLocal.COM;
                    stateWorker.linearInput = stateLocal.linearInput;
                    stateWorker.angularInput = stateLocal.angularInput;
                    stateWorker.frameCount = stateLocal.frameCount;
                    stateWorker.layoutChanged = stateLocal.layoutChanged;
                    stateWorker.portCount = stateLocal.portCount;
                    stateLocal.linearInput = Vector3.zero;
                    stateLocal.angularInput = Vector3.zero;
                    stateLocal.frameCount = 0;
                    stateLocal.layoutChanged = false;
                },
                () =>
                {
                    if (stateWorker.portCount != lastPortCount || fuelSolver == null || stateWorker.wheelStrength != stateLocal.wheelStrength)
                    {
                        stateWorker.results = new double[stateWorker.portCount];

                        portVariables = new CLP.BoundedVariable[stateWorker.portCount];
                        for (int i = 0; i < stateWorker.portCount; ++i)
                        {
                            portVariables[i] = new CLP.BoundedVariable(0, 1);
                        }
                        wheelVarX = new CLP.BoundedVariable(-stateWorker.wheelStrength.x, stateWorker.wheelStrength.x);
                        wheelVarY = new CLP.BoundedVariable(-stateWorker.wheelStrength.y, stateWorker.wheelStrength.y);
                        wheelVarZ = new CLP.BoundedVariable(-stateWorker.wheelStrength.z, stateWorker.wheelStrength.z);
                        allVariables = portVariables.Concat(new CLP.BoundedVariable[] { wheelVarX, wheelVarY, wheelVarZ });
                        fuelProblem = new CLP.Problem();
                        fuelProblem.constraints.AddRange(new CLP.Constraint[]{
                            fuelMomentX = new CLP.Constraint(),
                            fuelMomentY = new CLP.Constraint(),
                            fuelMomentZ = new CLP.Constraint(),
                            fuelForceX = new CLP.Constraint(),
                            fuelForceY = new CLP.Constraint(),
                            fuelForceZ = new CLP.Constraint()
                        });
                        fuelForces = new CLP.Constraint[] { fuelForceX, fuelForceY, fuelForceZ };
                        fuelMoments = new CLP.Constraint[] { fuelMomentX, fuelMomentY, fuelMomentZ };

                        thrustProblem = new CLP.Problem();
                        thrustProblem.constraints.AddRange(new CLP.Constraint[]{
                            thrustForceA1 = new CLP.Constraint(),
                            thrustForceA2 = new CLP.Constraint(),
                            thrustMomentX = fuelMomentX.shallow_clone(),
                            thrustMomentY = fuelMomentY.shallow_clone(),
                            thrustMomentZ = fuelMomentZ.shallow_clone()
                        });
                        thrustForceA1.RHS = 0.0;
                        thrustForceA2.RHS = 0.0;
                        thrustMomentX.RHS = 0.0;
                        thrustMomentY.RHS = 0.0;
                        thrustMomentZ.RHS = 0.0;

                        torqueProblem = new CLP.Problem();
                        torqueProblem.constraints.AddRange(new CLP.Constraint[] {
                            torqueForceX = fuelForceX.shallow_clone(),
                            torqueForceY = fuelForceY.shallow_clone(),
                            torqueForceZ = fuelForceZ.shallow_clone(),
                            torqueMomentA1 = new CLP.Constraint(),
                            torqueMomentA2 = new CLP.Constraint()
                        });
                        torqueMomentA1.RHS = 0.0;
                        torqueMomentA2.RHS = 0.0;
                        //Debug.Log("Creating solvers");
                        thrustSolver = new CLP.Solvers.MaxLPSolve(thrustProblem, allVariables);
                        torqueSolver = new CLP.Solvers.MaxLPSolve(torqueProblem, allVariables);
                        fuelSolver = new CLP.Solvers.MaxLPSolve(fuelProblem, allVariables);

                        lastPortCount = stateWorker.portCount;
                    }
                    for (int i = 0; i < stateWorker.portCount; ++i)
                    {
                        CLP.BoundedVariable var = portVariables[i];

                        //fuel cost
                        fuelProblem.objective[var] = -stateWorker.variables[i].maxFlow;

                        if (stateWorker.variables[i].operable)
                        {
                            Vector3 force = stateWorker.constants[i].maxThrust * stateWorker.variables[i].direction;

                            fuelForceX.f[var] = force.x;
                            fuelForceY.f[var] = force.y;
                            fuelForceZ.f[var] = force.z;
                            Vector3 offset = stateWorker.variables[i].position - stateWorker.COM;
                            Vector3 moment = Vector3.Cross(offset, force);
                            fuelMomentX.f[var] = moment.x;
                            fuelMomentY.f[var] = moment.y;
                            fuelMomentZ.f[var] = moment.z;
                        }
                        else
                        {
                            fuelForceX.f[var] = 0.0;
                            fuelForceY.f[var] = 0.0;
                            fuelForceZ.f[var] = 0.0;
                            fuelMomentX.f[var] = 0.0;
                            fuelMomentY.f[var] = 0.0;
                            fuelMomentZ.f[var] = 0.0;
                        }
                    }
                    if (stateWorker.wheelStrength.sqrMagnitude > 1E-5)
                    {
                        fuelMomentX.f[wheelVarX] = -1.0;
                        fuelMomentY.f[wheelVarY] = -1.0;
                        fuelMomentZ.f[wheelVarZ] = -1.0;
                    }
                    else
                    {
                        fuelMomentX.f[wheelVarX] = 0.0;
                        fuelMomentY.f[wheelVarY] = 0.0;
                        fuelMomentZ.f[wheelVarZ] = 0.0;
                    }
                    Vector3 linearInput = stateWorker.linearInput / stateWorker.frameCount;
                    bool zeroInput = true;
                    Vector3 linearDirection = Vector3.zero;
                    float linearMagnitude = 0.0f;
                    double maxThrust = 0.0f;
                    Vector3d targetThrust = Vector3.zero;
                    Vector3 angularInput = stateWorker.angularInput / stateWorker.frameCount;
                    Vector3 angularDirection = Vector3.zero;
                    float angularMagnitude = 0.0f;
                    double maxTorque = 0.0f;
                    Vector3d targetTorque = Vector3.zero;
                    if (linearInput.sqrMagnitude > float.Epsilon * 10)
                    {
                        zeroInput = false;
                        //Debug.Log("input non-zero");
                        // set the constraints
                        linearMagnitude = linearInput.magnitude;
                        linearDirection = linearInput / linearMagnitude;
                        if (linearMagnitude > 1.0f) linearMagnitude = 1.0f;
                        // find best axis to use as measure - largest coefficient
                        Vector3 magDir = new Vector3(Mathf.Abs(linearDirection.x), Mathf.Abs(linearDirection.y), Mathf.Abs(linearDirection.z));
                        int i_axis = magDir.x > magDir.y ? (magDir.x > magDir.z ? 0 : 2) : (magDir.y > magDir.z ? 1 : 2);
                        // other axes
                        int d_axis1 = (i_axis + 1) % 3;
                        int d_axis2 = (i_axis + 2) % 3;
                        float d_coeff1 = linearDirection[d_axis1] / linearDirection[i_axis];
                        float d_coeff2 = linearDirection[d_axis2] / linearDirection[i_axis];
                        for (int i = 0; i < stateWorker.portCount; ++i)
                        {
                            CLP.BoundedVariable var = portVariables[i];
                            
                            //thrust in direction
                            thrustProblem.objective[var] = Vector3.Dot(stateWorker.constants[i].maxThrust * stateWorker.variables[i].direction, linearDirection);

                            thrustForceA1.f[var] = fuelForces[d_axis1].f[var] - d_coeff1 * fuelForces[i_axis].f[var];
                            thrustForceA2.f[var] = fuelForces[d_axis2].f[var] - d_coeff2 * fuelForces[i_axis].f[var];
                        }

                        thrustSolver.solve();

                        if (thrustSolver.status != CLP.Solvers.Solver.Status.Optimal)
                        {
                            stateWorker.status = SharedState.Status.ThrustSolverFailure;
                            return;
                        }

                        maxThrust = thrustSolver.objective_value;
                        targetThrust = (maxThrust * linearMagnitude * 0.98) * (Vector3d)linearDirection;
                    }
                    if (angularInput.sqrMagnitude > float.Epsilon * 10)
                    {
                        zeroInput = false;
                        // set the constraints
                        angularMagnitude = angularInput.magnitude;
                        angularDirection = angularInput / angularMagnitude;
                        if (angularMagnitude > 1.0f) angularMagnitude = 1.0f;
                        // find best axis to use as measure - largest coefficient
                        Vector3 magDir = new Vector3(Mathf.Abs(angularDirection.x), Mathf.Abs(angularDirection.y), Mathf.Abs(angularDirection.z));
                        int i_axis = magDir.x > magDir.y ? (magDir.x > magDir.z ? 0 : 2) : (magDir.y > magDir.z ? 1 : 2);
                        // other axes
                        int d_axis1 = (i_axis + 1) % 3;
                        int d_axis2 = (i_axis + 2) % 3;
                        float d_coeff1 = angularDirection[d_axis1] / angularDirection[i_axis];
                        float d_coeff2 = angularDirection[d_axis2] / angularDirection[i_axis];
                        foreach(CLP.BoundedVariable var in allVariables )
                        {
                            torqueProblem.objective[var] = Vector3d.Dot(
                                new Vector3d(
                                    fuelMomentX.f[var],
                                    fuelMomentY.f[var],
                                    fuelMomentZ.f[var]),
                                angularDirection);

                            torqueMomentA1.f[var] = fuelMoments[d_axis1].f[var] - d_coeff1 * fuelMoments[i_axis].f[var];
                            torqueMomentA2.f[var] = fuelMoments[d_axis2].f[var] - d_coeff2 * fuelMoments[i_axis].f[var];
                        }
                        torqueForceX.RHS = targetThrust.x;
                        torqueForceY.RHS = targetThrust.y;
                        torqueForceZ.RHS = targetThrust.z;

                        torqueSolver.solve();

                        if (torqueSolver.status != CLP.Solvers.Solver.Status.Optimal)
                        {
                            stateWorker.status = SharedState.Status.TorqueSolverFailure;
                            return;
                        }

                        maxTorque = torqueSolver.objective_value;
                        targetTorque = (maxTorque * angularMagnitude * 0.98) * (Vector3d)angularDirection;
                    }

                    if (zeroInput)
                    {
                        stateWorker.status = SharedState.Status.InputIsZero;
                        return;
                    }

                    //finalise constraints
                    fuelForceX.RHS = targetThrust.x;
                    fuelForceY.RHS = targetThrust.y;
                    fuelForceZ.RHS = targetThrust.z;
                    fuelMomentX.RHS = targetTorque.x;
                    fuelMomentY.RHS = targetTorque.y;
                    fuelMomentZ.RHS = targetTorque.z;

                    fuelSolver.solve();
                    if (fuelSolver.status != CLP.Solvers.Solver.Status.Optimal)
                    {
                        stateWorker.status = SharedState.Status.FuelSolverFailure;
                        return;
                    }

                    for (int i = 0; i < stateWorker.portCount; ++i)
                        stateWorker.results[i] = fuelSolver.values[portVariables[i]];
                    stateWorker.wheelResult.x = (float)fuelSolver.values[wheelVarX];
                    stateWorker.wheelResult.y = (float)fuelSolver.values[wheelVarY];
                    stateWorker.wheelResult.z = (float)fuelSolver.values[wheelVarZ];
                    stateWorker.status = SharedState.Status.Solved;
                },
                () =>
                {
                    if (stateWorker.results == null)
                        stateLocal.results = null;
                    else if (stateLocal.results == null || stateWorker.results.Count() != stateLocal.results.Count())
                        stateLocal.results = new double[stateWorker.results.Count()];
                    if (stateWorker.status == SharedState.Status.Solved)
                        stateWorker.results.CopyTo(stateLocal.results, 0);
                    stateLocal.wheelResult = stateWorker.wheelResult;
                    stateLocal.status = stateWorker.status;
                });

        }

        public override Vessel vessel
        {
            get
            {
                return _vessel;
            }
            set
            {
                _vessel = value;
            }
        }

        public override void onStructuralChange()
        {
            layoutChanged = true;
        }

        public override void onFrame()
        {
            string stage = "update";
            try
            {
                worker.update(resultsAvailable =>
                {
                    stage = "set control input";
                    if (stateLocal == null)
                        stage += ", stateLocal == null";
                    if (_vessel == null)
                        stage += ", vessel == null";
                    if (stateLocal.linearInput == null)
                        stage += ", stateLocal.controlInput == null";
                    //update input state
                    Vector3 linearInput = new Vector3(_vessel.ctrlState.X, _vessel.ctrlState.Z, _vessel.ctrlState.Y);
                    linearInput.Scale(linearStrength);
                    stateLocal.linearInput += -_vessel.transform.InverseTransformDirection(_vessel.ReferenceTransform.rotation * linearInput);
                    Vector3 angularInput = new Vector3(vessel.ctrlState.pitch, vessel.ctrlState.roll, vessel.ctrlState.yaw);
                    angularInput.Scale(angularStrength);
                    stateLocal.angularInput += -_vessel.transform.InverseTransformDirection(_vessel.ReferenceTransform.rotation * angularInput);
                    ++stateLocal.frameCount;

                    stateLocal.layoutChanged = layoutChanged;
                    if (layoutChanged)
                    {
                        stage = "layoutChanged";
                        portRefs = _vessel.parts.SelectMany(p => p.Modules.Cast<PartModule>().Where(m => m is MyRCSModule).SelectMany(_m =>
                        {
                            MyRCSModule m = (MyRCSModule)_m;
                            int i = 0;
                            return m.thrusterTransforms.Select(t => new PortRef { index = i++, module = m });
                        })).ToArray();
                        wheels = FlightTimeEnhancer.getAllModules<ReactionWheelController>(_vessel).ToArray();
                        stateLocal.portCount = portRefs.Count();
                        if (stateLocal.constants == null || stateLocal.constants.Count() != stateLocal.portCount)
                        {
                            stateLocal.constants = new PortConstants[stateLocal.portCount];
                            stateLocal.variables = new PortVariables[stateLocal.portCount];
                        }
                        //update constants
                        for (int i = 0; i < stateLocal.portCount; ++i)
                        {
                            stateLocal.constants[i].maxThrust = portRefs[i].module.thrusterPower;
                        }
                        {
                            Vector3 total = Vector3.zero;
                            for (int i = 0; i < wheels.Count(); ++i)
                                if(wheels[i].operational)
                                    total += wheels[i].rotPower;
                            if (stateLocal.wheelStrength != total)
                            {
                                stateLocal.layoutChanged = true;
                            }
                            stateLocal.wheelStrength = total;
                        }
                        layoutChanged = false;
                    }
                    stage = "variable update";
                    //update variables
                    for (int i = 0; i < stateLocal.portCount; ++i)
                    {
                        MyRCSModule m = portRefs[i].module;
                        Transform t = m.thrusterTransforms[portRefs[i].index];
                        stateLocal.variables[i].direction = vessel.transform.InverseTransformDirection(-t.up);
                        stateLocal.variables[i].position = vessel.transform.InverseTransformPoint(t.position);
                        stateLocal.variables[i].maxFlow = m.realISP > 0 ? m.thrusterPower / (m.realISP * m.G) : 0.0f;
                        stateLocal.variables[i].operable = _vessel.ActionGroups[KSPActionGroup.RCS] && m.operational && m.isEnabled;
                    }
                    stateLocal.COM = vessel.transform.InverseTransformPoint(vessel.CoM + vessel.rb_velocity * Time.deltaTime);

                    //update output from output state
                    if (resultsAvailable)
                    {
                        stage = "update thrusts";
                        status = stateLocal.status;
                        if (stateLocal.status == SharedState.Status.Solved)
                        {
                            for (int i = 0; i < stateLocal.portCount; ++i)
                            {
                                float thrust = (float)stateLocal.results[i];
                                MyRCSModule module = portRefs[i].module;
                                int index = portRefs[i].index;
                                module.thrustForces[index] = thrust;
                            }
                            Vector3 torqueLeft = stateLocal.wheelResult;
                            for (int i = 0; i < wheels.Count(); ++i)
                            {
                                ReactionWheelController wheel = wheels[i];
                                if (Mathf.Abs(torqueLeft.x) < wheel.rotPower.x)
                                {
                                    wheel.overrideInput.x = torqueLeft.x / wheel.rotPower.x;
                                    torqueLeft.x = 0.0f;
                                }
                                else
                                {
                                    wheel.overrideInput.x = Mathf.Sign(torqueLeft.x);
                                    torqueLeft.x -= wheel.rotPower.x;
                                }
                                if (Mathf.Abs(torqueLeft.y) < wheel.rotPower.y)
                                {
                                    wheel.overrideInput.y = torqueLeft.y / wheel.rotPower.y;
                                    torqueLeft.y = 0.0f;
                                }
                                else
                                {
                                    wheel.overrideInput.y = Mathf.Sign(torqueLeft.y);
                                    torqueLeft.y -= wheel.rotPower.y;
                                }
                                if (Mathf.Abs(torqueLeft.z) < wheel.rotPower.z)
                                {
                                    wheel.overrideInput.z = torqueLeft.z / wheel.rotPower.z;
                                    torqueLeft.z = 0.0f;
                                }
                                else
                                {
                                    wheel.overrideInput.z = Mathf.Sign(torqueLeft.z);
                                    torqueLeft.z -= wheel.rotPower.z;
                                }
                                if (torqueLeft.x == 0.0f && torqueLeft.y == 0.0f && torqueLeft.z == 0.0f)
                                    break;
                            }
                        }
                        else
                        {
                            for (int i = 0; i < stateLocal.portCount; ++i)
                            {
                                MyRCSModule module = portRefs[i].module;
                                int index = portRefs[i].index;
                                module.thrustForces[index] = 0.0f;
                            }
                            for (int i = 0; i < wheels.Count(); ++i)
                            {
                                wheels[i].overrideInput = Vector3.zero;
                            }
                        }
                    }
                    return true;

                });
            }
            catch (Exception)
            {
                Debug.Log("LinearMode2 update: Error in stage: " + stage);
                throw;
            }
        }

        public override void onGUI()
        {
            string statusText = "Unknown";
            switch (status)
            {
                case SharedState.Status.Solved:
                    statusText = "Nominal";
                    break;
                case SharedState.Status.Infeasible:
                    statusText = "Port layout infeasible.";
                    break;
                case SharedState.Status.InputIsZero:
                    statusText = "No input.";
                    break;
                case SharedState.Status.FuelSolverFailure:
                    statusText = "Internal Error 01";
                    break;
                case SharedState.Status.ThrustSolverFailure:
                    statusText = "Internal Error 02";
                    break;
                case SharedState.Status.TorqueSolverFailure:
                    statusText = "Internal Error 03";
                    break;
            }
            if (worker != null)
                GUILayout.Label(worker.running ? "Active" : "Inactive");
            //if (portRefs != null)
            //    GUILayout.Label("Ports controlled: " + portRefs.Count());
            GUILayout.Label("Status: " + statusText);

            GUILayout.Label("Linear Strength: ");
            GUILayout.BeginHorizontal();
            GUILayout.Label("X:");
            linearStrength.x = GUILayout.HorizontalSlider(linearStrength.x, 0.0f, 1.0f);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Y:");
            linearStrength.y = GUILayout.HorizontalSlider(linearStrength.y, 0.0f, 1.0f);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Z:");
            linearStrength.z = GUILayout.HorizontalSlider(linearStrength.z, 0.0f, 1.0f);
            GUILayout.EndHorizontal();
            GUILayout.Label("Angular Strength: ");
            GUILayout.BeginHorizontal();
            GUILayout.Label("Pitch:");
            angularStrength.x = GUILayout.HorizontalSlider(angularStrength.x, 0.0f, 1.0f);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Roll:");
            angularStrength.y = GUILayout.HorizontalSlider(angularStrength.y, 0.0f, 1.0f);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Yaw:");
            angularStrength.z = GUILayout.HorizontalSlider(angularStrength.z, 0.0f, 1.0f);
            GUILayout.EndHorizontal();
        }

        public override void start()
        {
            //Debug.Log("Starting!");
            worker.start();
        }

        public override void stop()
        {
            worker.stop();
            worker.reset();
            layoutChanged = true;
        }

        public override bool running
        {
            get { return worker.running; }
        }

        public override void Load(ConfigNode node)
        {
            ConfigNode linear_strength_node = node.GetNode("linearStrength");
            if (linear_strength_node != null)
            {
                linearStrength.x = float.Parse(linear_strength_node.GetValue("x"));
                linearStrength.y = float.Parse(linear_strength_node.GetValue("y"));
                linearStrength.z = float.Parse(linear_strength_node.GetValue("z"));
            }
            ConfigNode angular_strength_node = node.GetNode("angularStrength");
            if (angular_strength_node != null)
            {
                angularStrength.x = float.Parse(angular_strength_node.GetValue("pitch"));
                angularStrength.y = float.Parse(angular_strength_node.GetValue("roll"));
                angularStrength.z = float.Parse(angular_strength_node.GetValue("yaw"));
            }
        }

        public override void Save(ConfigNode node)
        {
            ConfigNode linear_strength_node = node.AddNode("linearStrength");
            linear_strength_node.AddValue("x", linearStrength.x);
            linear_strength_node.AddValue("y", linearStrength.y);
            linear_strength_node.AddValue("z", linearStrength.z);
            ConfigNode angular_strength_node = node.AddNode("angularStrength");
            angular_strength_node.AddValue("pitch", angularStrength.x);
            angular_strength_node.AddValue("roll", angularStrength.y);
            angular_strength_node.AddValue("yaw", angularStrength.z);
        }

        public override IEnumerable<PartEffectorModule> getDesiredEffectors()
        {
            return ModuleKCAvionics.getAllModules<MyRCSModule>(_vessel).Cast<PartEffectorModule>();
        }
    }
}

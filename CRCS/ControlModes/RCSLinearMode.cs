﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace KerbCom.RCSControlModes
{
    [ControlMode.Details(guiName = "RCS Linear control", guiHelp =
@"RCS Linear Control:

This mode controls the RCS thrusters on the vessel. It responds to the translation controls (typically H, N, I, J, K and L) to provide accurate horizontal motion while not inducing any rotation.

In order to work, the vessel must have an adequate distribution of active RCS ports, arranged in an adequate distribution of directions. It does not take into account or make use of the reaction wheel in command pods, so most vessels will need at least 6 quad-RCS blocks - 3 above the centre of mass and 3 below, distributed around the vessel. In a future version, with reaction wheel support, this mode may only require as few as 3 blocks for accurate motion.

If the mode cannot produce accurate translation with the currently activated RCS ports it will not produce any motion at all, and its status will be ""infeasible""."
    )]
    class RCSLinearMode : ControlMode
    {
        private Vessel _vessel;
        private IIterativeWorker worker;

        private struct PortRef
        {
            public int index;
            public MyRCSModule module;
        }

        PortRef[] portRefs;

        private struct PortConstants
        {
            public float maxThrust;
        }
        private struct PortVariables
        {
            public float maxFlow;
            public Vector3 direction, position;
            public bool operable;
        }

        private class SharedState
        {
            //input
            public int portCount = 0;
            public PortConstants[] constants;
            public PortVariables[] variables;
            public Vector3 COM;
            public Vector3 controlInput = Vector3.zero;
            public int frameCount = 0;
            public bool layoutChanged = true;
            //output
            public double[] results;
            public enum Status
            {
                InputIsZero,
                Infeasible,
                ThrustSolverFailure,
                FuelSolverFailure,
                Solved
            }
            public Status status;
        }

        SharedState stateLocal = new SharedState(), stateWorker = new SharedState();

        //local private state for the GUI
        private SharedState.Status status;
        private Vector3 strength = Vector3.one;

        //worker private state:
        CLP.Constraint fuelMomentX, fuelMomentY, fuelMomentZ;
        CLP.Constraint fuelForceX, fuelForceY, fuelForceZ;
        CLP.Constraint[] fuelForces;
        CLP.Constraint thrustMomentX, thrustMomentY, thrustMomentZ;
        CLP.Constraint thrustForceA1, thrustForceA2;
        CLP.Problem fuelProblem;
        CLP.Problem thrustProblem;
        CLP.Solvers.Solver thrustSolver;
        CLP.Solvers.Solver fuelSolver;
        private CLP.BoundedVariable[] portVariables;

        int lastPortCount = 0;
        private bool layoutChanged = true;

        public RCSLinearMode()
        {
            worker = new SequentialIterativeWorker(
                () =>
                {
                    //copy input state
                    if (stateLocal.layoutChanged)
                    {

                        if (stateLocal.constants == null)
                            stateWorker.constants = null;
                        else
                        {
                            if (stateWorker.constants == null || stateLocal.constants.Count() != stateWorker.constants.Count())
                                stateWorker.constants = new PortConstants[stateLocal.constants.Count()];
                            stateLocal.constants.CopyTo(stateWorker.constants, 0);
                        }
                    }
                    if (stateLocal.variables == null)
                        stateWorker.variables = null;
                    else
                    {
                        if (stateWorker.variables == null || stateLocal.variables.Count() != stateWorker.variables.Count())
                            stateWorker.variables = new PortVariables[stateLocal.variables.Count()];
                        stateLocal.variables.CopyTo(stateWorker.variables, 0);
                    }
                    stateWorker.COM = stateLocal.COM;
                    stateWorker.controlInput = stateLocal.controlInput;
                    stateWorker.frameCount = stateLocal.frameCount;
                    stateWorker.layoutChanged = stateLocal.layoutChanged;
                    stateWorker.portCount = stateLocal.portCount;
                    stateLocal.controlInput = Vector3.zero;
                    stateLocal.frameCount = 0;
                    stateLocal.layoutChanged = false;
                },
                () =>
                {
                    if (stateWorker.portCount != lastPortCount || fuelSolver == null)
                    {
                        stateWorker.results = new double[stateWorker.portCount];
                        portVariables = new CLP.BoundedVariable[stateWorker.portCount];
                        for (int i = 0; i < stateWorker.portCount; ++i)
                        {
                            portVariables[i] = new CLP.BoundedVariable(0, 1);
                        }
                        fuelProblem = new CLP.Problem();
                        fuelProblem.constraints.AddRange(new CLP.Constraint[]{
                            fuelMomentX = new CLP.Constraint(),
                            fuelMomentY = new CLP.Constraint(),
                            fuelMomentZ = new CLP.Constraint(),
                            fuelForceX = new CLP.Constraint(),
                            fuelForceY = new CLP.Constraint(),
                            fuelForceZ = new CLP.Constraint()
                        });
                        fuelMomentX.RHS = 0.0;
                        fuelMomentY.RHS = 0.0;
                        fuelMomentZ.RHS = 0.0;
                        fuelForces = new CLP.Constraint[] { fuelForceX, fuelForceY, fuelForceZ };
                        thrustProblem = new CLP.Problem();
                        thrustProblem.constraints.AddRange(new CLP.Constraint[]{
                            thrustForceA1 = new CLP.Constraint(),
                            thrustForceA2 = new CLP.Constraint(),
                            thrustMomentX = fuelMomentX.shallow_clone(),
                            thrustMomentY = fuelMomentY.shallow_clone(),
                            thrustMomentZ = fuelMomentZ.shallow_clone()
                        });
                        thrustForceA1.RHS = 0.0;
                        thrustForceA2.RHS = 0.0;
                        thrustMomentX.RHS = 0.0;
                        thrustMomentY.RHS = 0.0;
                        thrustMomentZ.RHS = 0.0;
                        fuelSolver = new CLP.Solvers.MaxLPSolve(fuelProblem, portVariables);
                        thrustSolver = new CLP.Solvers.MaxLPSolve(thrustProblem, portVariables);

                        lastPortCount = stateWorker.portCount;
                    }

                    Vector3 controlInput = stateWorker.controlInput / stateWorker.frameCount;
                    //don't bother if input is 0
                    if (controlInput.sqrMagnitude < 1E-5)
                    {
                        stateWorker.status = SharedState.Status.InputIsZero;
                        return;
                    }
                    // set the constraints
                    Vector3 controlDirection = controlInput.normalized;
                    float controlMagnitude = controlInput.magnitude;
                    if (controlMagnitude > 1.0f) controlMagnitude = 1.0f;
                    // find best axis to use as measure - largest coefficient
                    Vector3 magDir = new Vector3(Mathf.Abs(controlDirection.x), Mathf.Abs(controlDirection.y), Mathf.Abs(controlDirection.z));
                    int i_axis = magDir.x > magDir.y ? (magDir.x > magDir.z ? 0 : 2) : (magDir.y > magDir.z ? 1 : 2);
                    // other axes
                    int d_axis1 = (i_axis + 1) % 3;
                    int d_axis2 = (i_axis + 2) % 3;
                    float d_coeff1 = controlDirection[d_axis1] / controlDirection[i_axis];
                    float d_coeff2 = controlDirection[d_axis2] / controlDirection[i_axis];
                    // set constraints
                    for (int i = 0; i < stateWorker.portCount; ++i)
                    {
                        CLP.BoundedVariable var = portVariables[i];
                        //fuel cost
                        fuelProblem.objective[var] = -stateWorker.variables[i].maxFlow;

                        if (stateWorker.variables[i].operable)
                        {
                            Vector3 force = stateWorker.constants[i].maxThrust * stateWorker.variables[i].direction;

                            fuelForceX.f[var] = force.x;
                            fuelForceY.f[var] = force.y;
                            fuelForceZ.f[var] = force.z;
                            Vector3 offset = stateWorker.variables[i].position - stateWorker.COM;
                            Vector3 moment = Vector3.Cross(offset, force);
                            fuelMomentX.f[var] = moment.x;
                            fuelMomentY.f[var] = moment.y;
                            fuelMomentZ.f[var] = moment.z;
                        }
                        else
                        {
                            fuelForceX.f[var] = 0.0;
                            fuelForceY.f[var] = 0.0;
                            fuelForceZ.f[var] = 0.0;
                            fuelMomentX.f[var] = 0.0;
                            fuelMomentY.f[var] = 0.0;
                            fuelMomentZ.f[var] = 0.0;
                        }
                        //thrust in direction
                        thrustProblem.objective[var] = Vector3.Dot(stateWorker.constants[i].maxThrust * stateWorker.variables[i].direction, controlDirection);

                        thrustForceA1.f[var] = fuelForces[d_axis1].f[var] - d_coeff1 * fuelForces[i_axis].f[var];
                        thrustForceA2.f[var] = fuelForces[d_axis2].f[var] - d_coeff2 * fuelForces[i_axis].f[var];
                    }

                    thrustSolver.solve();

                    if (thrustSolver.status != CLP.Solvers.Solver.Status.Optimal)
                    {
                        Debug.Log("RCSLinearMode: Thrust solver failure - status: " + thrustSolver.status.ToString());
                        Debug.Log("State: " + thrustSolver.ToString());
                        stateWorker.status = SharedState.Status.ThrustSolverFailure;
                        return;
                    }
                    double maxThrust = thrustSolver.objective_value;

                    double targetThrust = maxThrust * controlMagnitude * 0.98;

                    if (targetThrust < double.Epsilon * 10)
                    {
                        stateWorker.status = SharedState.Status.Infeasible;
                        return;
                    }

                    //Debug.Log("target thrust: " + targetThrust);
                    //finalise constraints
                    fuelForceX.RHS = targetThrust * controlDirection.x;
                    fuelForceY.RHS = targetThrust * controlDirection.y;
                    fuelForceZ.RHS = targetThrust * controlDirection.z;

                    fuelSolver.solve();

                    if (fuelSolver.status != CLP.Solvers.Solver.Status.Optimal)
                    {
                        Debug.Log("RCSLinearMode: Fuel solver failure - status: " + fuelSolver.status.ToString());
                        Debug.Log("State: " + fuelSolver.ToString());
                        stateWorker.status = SharedState.Status.FuelSolverFailure;
                        return;
                    }
                    double fuel_used = fuelSolver.objective_value;

                    for (int i = 0; i < stateWorker.portCount; ++i)
                        stateWorker.results[i] = fuelSolver.values[portVariables[i]];
                    stateWorker.status = SharedState.Status.Solved;
                },
                () =>
                {
                    if (stateWorker.results == null)
                        stateLocal.results = null;
                    else if (stateLocal.results == null || stateWorker.results.Count() != stateLocal.results.Count())
                        stateLocal.results = new double[stateWorker.results.Count()];
                    if (stateWorker.status == SharedState.Status.Solved)
                        stateWorker.results.CopyTo(stateLocal.results, 0);

                    stateLocal.status = stateWorker.status;
                });

        }

        private void enumeratePorts()
        {
            portRefs = _vessel.parts.SelectMany(p => p.Modules.Cast<PartModule>().Where(m => m is MyRCSModule).SelectMany(_m =>
                {
                    MyRCSModule m = (MyRCSModule)_m;
                    int i = 0;
                    return m.thrusterTransforms.Select(t => new PortRef { index = i++, module = m });
                })).ToArray();
        }

        public override Vessel vessel
        {
            get
            {
                return _vessel;
            }
            set
            {
                _vessel = value;
            }
        }

        public override void onStructuralChange()
        {
            layoutChanged = true;
        }

        public override void onFrame()
        {
            string stage = "update";
            try
            {
                worker.update(resultsAvailable =>
                {
                    stage = "set control input";
                    if (stateLocal == null)
                        stage += ", stateLocal == null";
                    if (_vessel == null)
                        stage += ", vessel == null";
                    if (stateLocal.controlInput == null)
                        stage += ", stateLocal.controlInput == null";
                    //update input state
                    Vector3 modinput = new Vector3(_vessel.ctrlState.X, _vessel.ctrlState.Z, _vessel.ctrlState.Y);//.normalized;
                    modinput.Scale(strength);
                    stateLocal.controlInput += -_vessel.transform.InverseTransformDirection(_vessel.ReferenceTransform.rotation * modinput);
                    ++stateLocal.frameCount;

                    stateLocal.layoutChanged = layoutChanged;
                    if (layoutChanged)
                    {
                        stage = "layoutChanged";
                        enumeratePorts();
                        stateLocal.portCount = portRefs.Count();
                        if (stateLocal.constants == null || stateLocal.constants.Count() != stateLocal.portCount)
                        {
                            stateLocal.constants = new PortConstants[stateLocal.portCount];
                            stateLocal.variables = new PortVariables[stateLocal.portCount];
                        }
                        //update constants
                        for (int i = 0; i < stateLocal.portCount; ++i)
                        {
                            stateLocal.constants[i].maxThrust = portRefs[i].module.thrusterPower;
                        }
                        layoutChanged = false;
                    }
                    stage = "variable update";
                    //update variables
                    for (int i = 0; i < stateLocal.portCount; ++i)
                    {
                        MyRCSModule m = portRefs[i].module;
                        Transform t = m.thrusterTransforms[portRefs[i].index];
                        stateLocal.variables[i].direction = vessel.transform.InverseTransformDirection(-t.up);
                        stateLocal.variables[i].position = vessel.transform.InverseTransformPoint(t.position);
                        stateLocal.variables[i].maxFlow = m.realISP > 0 ? m.thrusterPower / (m.realISP * m.G) : 0.0f;
                        stateLocal.variables[i].operable = m.operational && m.isEnabled;
                    }
                    stateLocal.COM = vessel.transform.InverseTransformPoint(vessel.CoM + vessel.rb_velocity * Time.deltaTime);

                    //update output from output state
                    if (resultsAvailable)
                    {
                        stage = "update thrusts";
                        status = stateLocal.status;
                        if (stateLocal.status == SharedState.Status.Solved)
                            for (int i = 0; i < stateLocal.portCount; ++i)
                            {
                                float thrust = (float)stateLocal.results[i];
                                MyRCSModule module = portRefs[i].module;
                                int index = portRefs[i].index;
                                module.thrustForces[index] = thrust;
                            }
                        else if(stateLocal.status == SharedState.Status.InputIsZero)
                            for (int i = 0; i < stateLocal.portCount; ++i)
                            {
                                MyRCSModule module = portRefs[i].module;
                                int index = portRefs[i].index;
                                module.thrustForces[index] = 0.0f;
                            }
                    }
                    return true;

                });
            }
            catch (Exception)
            {
                Debug.Log("LinearMode2 update: Error in stage: " + stage);
                throw;
            }
        }

        public override void onGUI()
        {
            string statusText = "Unknown";
            switch (status)
            {
                case SharedState.Status.Solved:
                    statusText = "Nominal";
                    break;
                case SharedState.Status.Infeasible:
                    statusText = "Port layout infeasible.";
                    break;
                case SharedState.Status.InputIsZero:
                    statusText = "No input.";
                    break;
                case SharedState.Status.FuelSolverFailure:
                    statusText = "Internal Error 01";
                    break;
                case SharedState.Status.ThrustSolverFailure:
                    statusText = "Internal Error 02";
                    break;
            }
            if (worker != null)
                GUILayout.Label(worker.running ? "Active" : "Inactive");
            if (portRefs != null)
                GUILayout.Label("Ports controlled: " + portRefs.Count());
            GUILayout.Label("Status: " + statusText);

            GUILayout.Label("Strength: ");
            GUILayout.BeginHorizontal();
            GUILayout.Label("X:");
            strength.x = GUILayout.HorizontalSlider(strength.x, 0.0f, 1.0f);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Y:");
            strength.y = GUILayout.HorizontalSlider(strength.y, 0.0f, 1.0f);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Z:");
            strength.z = GUILayout.HorizontalSlider(strength.z, 0.0f, 1.0f);
            GUILayout.EndHorizontal();
        }

        public override void start()
        {
            worker.start();
        }

        public override void stop()
        {
            worker.stop();
            worker.reset();
            layoutChanged = true;
        }

        public override bool running
        {
            get { return worker.running; }
        }

        public override void Load(ConfigNode node)
        {
            ConfigNode strength_node = node.GetNode("strength");
            if (strength_node != null)
            {
                strength.x = float.Parse(strength_node.GetValue("x"));
                strength.y = float.Parse(strength_node.GetValue("y"));
                strength.z = float.Parse(strength_node.GetValue("z"));
            }
        }

        public override void Save(ConfigNode node)
        {
            ConfigNode strength_node = node.AddNode("strength");
            strength_node.AddValue("x", strength.x);
            strength_node.AddValue("y", strength.y);
            strength_node.AddValue("z", strength.z);
        }

        public override IEnumerable<PartEffectorModule> getDesiredEffectors()
        {
            return ModuleKCAvionics.getAllModules<MyRCSModule>(_vessel).Cast<PartEffectorModule>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace KerbCom.ControlModes
{
    [ControlMode.Details(guiName = "Combined Control", guiHelp =
@"Combined Control:

This mode controls the vessel using engines, gimbals, RCS thrusters and reaction wheels simultaneously.

If the vessel is not throttled up, the vessel is in ""docking"" mode, where RCS and reaction wheels are used to precisely translate and rotate the vessel.

When throttled up the vessel is in ""guidance"" mode, where main engines, gimbals, RCS and reaction wheels are used only to maintain and control the orientation of the vessel while under thrust. This mode does not make any guarantees about the net magnitude or direction of thrust.

This behaviour can be overridden so that the vessel is always in ""guidance"" mode.")]
    class CombinedMode : ControlMode
    {
        private Vessel _vessel;
        private IIterativeWorker worker;
        private struct PortRef
        {
            public int index;
            public MyRCSModule module;
        }
        ReactionWheelController[] wheels = new ReactionWheelController[0];
        PortRef[] portRefs = new PortRef[0];
        GimballedEngineController[] gimballedEngines = new GimballedEngineController[0];
        BaseThrottleController[] staticEngines = new BaseThrottleController[0];

        #region Shared state
        private class SharedState
        {
            public struct Constants
            {
                public struct Port
                {
                    public float maxThrust;
                }
                public struct StaticEngine
                {
                    public float minThrust, maxThrust;
                    public int thrustCount;
                }
                public struct GimballedEngine
                {
                    public float minThrust, maxThrust;
                    public int thrustCount;
                    public float gimbalRange;
                }
                public Port[] port;
                public StaticEngine[] static_;
                public GimballedEngine[] gimbal;
            }
            public struct Variables
            {
                public struct Port
                {
                    public float maxFlow;
                    public Vector3 direction, position;
                    public bool operable;
                }

                public struct StaticEngine
                {
                    public float maxFlow;
                    public Vector3[] direction, position;
                    public bool operable;
                }

                public struct GimballedEngine
                {
                    public float maxFlow;
                    public Vector3[] position;
                    public Quaternion[] orientation;
                    public bool operable;
                    public bool locked;
                }
                public Port[] port;
                public StaticEngine[] static_;
                public GimballedEngine[] gimbal;
            }
            public enum Status
            {
                ThrustSolverFailure,
                TorqueSolverFailure,
                MonoSolverFailure,
                FuelSolverFailure,
                Solved,
                InputIsZero,
                BadEngineConfig
            }
            public bool init = false;
            #region Input
            public bool alwaysGuidance;
            public Constants constants;
            public Variables variables;
            public int portCount, staticCount, gimbalCount;

            public Vector3 wheelStrength;
            public Vector3 COM;

            public Vector3 linearInput = Vector3.zero;
            public Vector3 torqueInput = Vector3.zero;
            public float throttleInput = 0.0f;

            public bool layoutChanged;
            public int frameCount = 0;
            #endregion

            #region Output
            public Vector3 wheelTorque;
            public double[] portThrusts, staticThrusts, gimbalThrusts;
            public Vector2[] gimbalSettings;
            public Status status;
            #endregion
        }
        SharedState stateLocal = new SharedState(), stateWorker = new SharedState();
        #endregion

        #region GUI state
        private SharedState.Status status;
        private float throttleStrength = 1.0f;
        private Vector3 guidanceTorqueStrength = Vector3.one;
        private Vector3 dockingTorqueStrength = Vector3.one;
        private Vector3 rcsThrustStrength = Vector3.one;
        private bool layoutChanged = true;
        private bool alwaysGuidance = false;
        #endregion

        #region Worker state
        private class TriConstraint
        {
            public CLP.Constraint x, y, z;
            private CLP.Constraint[] c;
            public TriConstraint()
            {
                c = new CLP.Constraint[] { x = new CLP.Constraint(), y = new CLP.Constraint(), z = new CLP.Constraint() };
            }
            public TriConstraint(CLP.Constraint x, CLP.Constraint y, CLP.Constraint z)
            {
                c = new CLP.Constraint[] { this.x = x, this.y = y, this.z = z };
            }
            public CLP.Constraint this[int i]
            {
                get
                {
                    return c[i];
                }
            }
        }
        private class BiConstraint
        {
            public CLP.Constraint x, y;
            private CLP.Constraint[] c;
            public BiConstraint()
            {
                c = new CLP.Constraint[] { x = new CLP.Constraint(), y = new CLP.Constraint() };
            }
            public BiConstraint(CLP.Constraint x, CLP.Constraint y)
            {
                c = new CLP.Constraint[] { this.x = x, this.y = y };
            }
            public CLP.Constraint this[int i]
            {
                get
                {
                    return c[i];
                }
            }
        }
        public struct WheelVariables
        {
            public CLP.BoundedVariable x, y, z;
        }
        private struct GuidanceProblem
        {
            public struct ThrustProblem
            {
                public TriConstraint moment;
                public CLP.Problem problem;
                public CLP.Solvers.Solver solver;
            }
            public ThrustProblem thrust;
            public struct TorqueProblem
            {
                public CLP.Constraint thrust;
                public BiConstraint moment;
                public CLP.Problem problem;
                public CLP.Solvers.Solver solver;
            }
            public TorqueProblem torque;
            public struct MonoProblem
            {
                public CLP.Constraint thrust;
                public TriConstraint moment;
                public CLP.Problem problem;
                public CLP.Solvers.Solver solver;
            }
            public MonoProblem mono;
            public struct FuelProblem
            {
                public CLP.Constraint mono;
                public CLP.Constraint thrust;
                public TriConstraint moment;
                public CLP.Problem problem;
                public CLP.Solvers.Solver solver;
            }
            public FuelProblem fuel;
            public struct Gimbal
            {
                public struct Variables
                {
                    public CLP.BoundedVariable[] NZ, PX, NX, PY, NY;
                    public CLP.BoundedVariable[][] slack;
                    public CLP.BoundedVariable[] bpx, bnx, bpy, bny,
                        tpx, tnx, tpy, tny;
                    public Variables(int size)
                    {
                        slack = new CLP.BoundedVariable[8][] {
                        bpx = new CLP.BoundedVariable[size],
                        bpy = new CLP.BoundedVariable[size],
                        bnx = new CLP.BoundedVariable[size],
                        bny = new CLP.BoundedVariable[size],
                        tpx = new CLP.BoundedVariable[size],
                        tpy = new CLP.BoundedVariable[size],
                        tnx = new CLP.BoundedVariable[size],
                        tny = new CLP.BoundedVariable[size]
                    };
                        for (int i = 0; i < 8; ++i)
                        {
                            for (int j = 0; j < size; ++j)
                                slack[i][j] = new CLP.BoundedVariable();
                        }

                        NZ = new CLP.BoundedVariable[size];
                        PX = new CLP.BoundedVariable[size];
                        NX = new CLP.BoundedVariable[size];
                        PY = new CLP.BoundedVariable[size];
                        NY = new CLP.BoundedVariable[size];
                        for (int i = 0; i < size; ++i)
                        {
                            NZ[i] = new CLP.BoundedVariable(0.0f, 1.0f);
                            PX[i] = new CLP.BoundedVariable();
                            NX[i] = new CLP.BoundedVariable();
                            PY[i] = new CLP.BoundedVariable();
                            NY[i] = new CLP.BoundedVariable();
                        }
                    }
                    public IEnumerable<CLP.BoundedVariable> all()
                    {
                        return new[] { NZ, PX, NX, PY, NY, slack[0], slack[1], slack[2], slack[3], slack[4], slack[5], slack[6], slack[7] }.SelectMany(id => id);
                    }
                }
                public Variables variables;
                public class ConstraintSet
                {
                    public CLP.Constraint[] c;
                    public CLP.Constraint bpx, bpy, bnx, bny,
                        tpx, tpy, tnx, tny;
                    public ConstraintSet()
                    {
                        c = new CLP.Constraint[] {
                            bpx = new CLP.Constraint(),
                            bpy = new CLP.Constraint(),
                            bnx = new CLP.Constraint(),
                            bny = new CLP.Constraint(),
                            tpx = new CLP.Constraint(),
                            tpy = new CLP.Constraint(),
                            tnx = new CLP.Constraint(),
                            tny = new CLP.Constraint()
                        };
                    }
                }
                public ConstraintSet[] constraints;
            }

            public Gimbal gimbal;
            public WheelVariables wheelVariables;
            public CLP.BoundedVariable[] portVariables, staticVariables;
            public IEnumerable<CLP.BoundedVariable> allVariables;

            public Dictionary<CLP.Constraint, string> cnames;
            public Dictionary<CLP.BoundedVariable, string> vnames;
        }
        GuidanceProblem guidance;
        private struct DockingProblem
        {
            public struct ThrustProblem
            {
                public TriConstraint moment;
                public BiConstraint thrust;
                public CLP.Problem problem;
                public CLP.Solvers.Solver solver;
            }
            public ThrustProblem thrust;
            public struct TorqueProblem
            {
                public BiConstraint moment;
                public TriConstraint thrust;
                public CLP.Problem problem;
                public CLP.Solvers.Solver solver;
            }
            public TorqueProblem torque;
            public struct MonoProblem
            {
                public TriConstraint moment, thrust;
                public CLP.Problem problem;
                public CLP.Solvers.Solver solver;
            }
            public MonoProblem mono;
            public CLP.BoundedVariable[] portVariables;

            public WheelVariables wheelVariables;
            public IEnumerable<CLP.BoundedVariable> allVariables;

            public Dictionary<CLP.Constraint, string> cnames;
            public Dictionary<CLP.BoundedVariable, string> vnames;
        }
        DockingProblem docking;

        //int lastPortCount = 0, lastStaticCount = 0, lastGimbalCount = 0;
        #endregion

        private void copyInput()
        {
            if (stateLocal.layoutChanged || !stateWorker.init)
            {
                if (!stateWorker.init)
                {
                    stateWorker.portCount = stateLocal.portCount;
                    stateWorker.staticCount = stateLocal.staticCount;
                    stateWorker.gimbalCount = stateLocal.gimbalCount;
                    stateWorker.constants.port = new SharedState.Constants.Port[stateWorker.portCount];
                    stateWorker.constants.static_ = new SharedState.Constants.StaticEngine[stateWorker.staticCount];
                    stateWorker.constants.gimbal = new SharedState.Constants.GimballedEngine[stateWorker.gimbalCount];
                    stateWorker.variables.port = new SharedState.Variables.Port[stateWorker.portCount];
                    stateWorker.variables.static_ = new SharedState.Variables.StaticEngine[stateWorker.staticCount];
                    stateWorker.variables.gimbal = new SharedState.Variables.GimballedEngine[stateWorker.gimbalCount];
                    stateWorker.portThrusts = new double[stateWorker.portCount];
                    stateWorker.staticThrusts = new double[stateWorker.staticCount];
                    stateWorker.gimbalThrusts = new double[stateWorker.gimbalCount];
                    stateWorker.gimbalSettings = new Vector2[stateWorker.gimbalCount];
                    stateWorker.init = true;
                }
                else
                {
                    if (stateWorker.portCount != stateLocal.portCount)
                    {
                        stateWorker.portCount = stateLocal.portCount;
                        stateWorker.constants.port = new SharedState.Constants.Port[stateWorker.portCount];
                        stateWorker.variables.port = new SharedState.Variables.Port[stateWorker.portCount];
                    }
                    if (stateWorker.staticCount != stateLocal.staticCount)
                    {
                        stateWorker.staticCount = stateLocal.staticCount;
                        stateWorker.constants.static_ = new SharedState.Constants.StaticEngine[stateWorker.staticCount];
                        stateWorker.variables.static_ = new SharedState.Variables.StaticEngine[stateWorker.staticCount];
                    }
                    if (stateWorker.gimbalCount != stateLocal.gimbalCount)
                    {
                        stateWorker.gimbalCount = stateLocal.gimbalCount;
                        stateWorker.constants.gimbal = new SharedState.Constants.GimballedEngine[stateWorker.gimbalCount];
                        stateWorker.variables.gimbal = new SharedState.Variables.GimballedEngine[stateWorker.gimbalCount];
                    }
                }
                stateLocal.constants.port.CopyTo(stateWorker.constants.port, 0);
                stateLocal.constants.static_.CopyTo(stateWorker.constants.static_, 0);
                stateLocal.constants.gimbal.CopyTo(stateWorker.constants.gimbal, 0);    
            }
            stateWorker.alwaysGuidance = stateLocal.alwaysGuidance;
            stateWorker.wheelStrength = stateLocal.wheelStrength;

            stateLocal.variables.port.CopyTo(stateWorker.variables.port, 0);
            stateLocal.variables.static_.CopyTo(stateWorker.variables.static_, 0);
            stateLocal.variables.gimbal.CopyTo(stateWorker.variables.gimbal, 0);

            stateWorker.COM = stateLocal.COM;

            stateWorker.throttleInput = stateLocal.throttleInput;
            stateWorker.torqueInput = stateLocal.torqueInput;
            stateWorker.linearInput = stateLocal.linearInput;

            stateWorker.frameCount = stateLocal.frameCount;
            stateWorker.layoutChanged = stateLocal.layoutChanged;

            stateLocal.throttleInput = 0.0f;
            stateLocal.torqueInput = Vector3.zero;
            stateLocal.linearInput = Vector3.zero;
            stateLocal.frameCount = 0;
            stateLocal.layoutChanged = false;
        }

        private string stage = "";
        private void work()
        {
            if (stateWorker.layoutChanged)
            {
                stage = "layout change";
                #region Reallocate the output
                stateWorker.portThrusts = new double[stateWorker.portCount];
                stateWorker.staticThrusts = new double[stateWorker.staticCount];
                stateWorker.gimbalThrusts = new double[stateWorker.gimbalCount];
                stateWorker.gimbalSettings = new Vector2[stateWorker.gimbalCount];
                #endregion
                #region (Re)construct the system and solvers
                #region Guidance
                #region Variables
                guidance.cnames = new Dictionary<CLP.Constraint, string>();
                guidance.vnames = new Dictionary<CLP.BoundedVariable, string>();
                guidance.portVariables = new CLP.BoundedVariable[stateWorker.portCount];
                guidance.portVariables.populate(_ => new CLP.BoundedVariable(0.0, 1.0));
                for (int i = 0; i < stateWorker.portCount; ++i)
                    guidance.vnames[guidance.portVariables[i]] = "p_" + i;
                guidance.staticVariables = new CLP.BoundedVariable[stateWorker.staticCount];
                guidance.staticVariables.populate(_ => new CLP.BoundedVariable(0.0, 1.0));
                for (int i = 0; i < stateWorker.staticCount; ++i)
                    guidance.vnames[guidance.staticVariables[i]] = "s_" + i;
                guidance.gimbal.variables = new GuidanceProblem.Gimbal.Variables(stateWorker.gimbalCount);
                for (int i = 0; i < stateWorker.gimbalCount; ++i)
                {
                    GuidanceProblem.Gimbal.Variables vars = guidance.gimbal.variables;
                    guidance.vnames[vars.PX[i]] = "px_" + i;
                    guidance.vnames[vars.NX[i]] = "nx_" + i;
                    guidance.vnames[vars.PY[i]] = "py_" + i;
                    guidance.vnames[vars.NY[i]] = "ny_" + i;
                    guidance.vnames[vars.NZ[i]] = "nz_" + i;
                    guidance.vnames[vars.bnx[i]] = "bnx_" + i;
                    guidance.vnames[vars.bny[i]] = "bny_" + i;
                    guidance.vnames[vars.bpx[i]] = "bpx_" + i;
                    guidance.vnames[vars.bpy[i]] = "bpy_" + i;
                    guidance.vnames[vars.tnx[i]] = "tnx_" + i;
                    guidance.vnames[vars.tny[i]] = "tny_" + i;
                    guidance.vnames[vars.tpx[i]] = "tpx_" + i;
                    guidance.vnames[vars.tpy[i]] = "tpy_" + i;
                }
                guidance.wheelVariables.x = new CLP.BoundedVariable(-stateWorker.wheelStrength.x, stateWorker.wheelStrength.x);
                guidance.wheelVariables.y = new CLP.BoundedVariable(-stateWorker.wheelStrength.y, stateWorker.wheelStrength.y);
                guidance.wheelVariables.z = new CLP.BoundedVariable(-stateWorker.wheelStrength.z, stateWorker.wheelStrength.z);
                guidance.vnames[guidance.wheelVariables.x] = "wheel.x";
                guidance.vnames[guidance.wheelVariables.y] = "wheel.x";
                guidance.vnames[guidance.wheelVariables.z] = "wheel.z";
                guidance.allVariables = new[] { new[] { guidance.wheelVariables.x, guidance.wheelVariables.y, guidance.wheelVariables.z }, guidance.portVariables, guidance.staticVariables, guidance.gimbal.variables.all() }.SelectMany(vs => vs);
                #endregion
                #region Thrust
                guidance.thrust.problem = new CLP.Problem();
                guidance.thrust.moment = new TriConstraint();
                guidance.cnames[guidance.thrust.moment.x] = "moment.x";
                guidance.cnames[guidance.thrust.moment.y] = "moment.y";
                guidance.cnames[guidance.thrust.moment.z] = "moment.z";
                guidance.thrust.problem.constraints.AddRange(new CLP.Constraint[]{
                    guidance.thrust.moment.x,
                    guidance.thrust.moment.y,
                    guidance.thrust.moment.z
                });

                #endregion
                #region Torque
                guidance.torque.problem = new CLP.Problem();
                guidance.torque.moment = new BiConstraint();
                guidance.cnames[guidance.torque.moment.x] = "moment.x";
                guidance.cnames[guidance.torque.moment.y] = "moment.y";
                guidance.torque.problem.constraints.AddRange(new CLP.Constraint[]{
                    guidance.torque.thrust = new CLP.Constraint(guidance.thrust.problem.objective, 0.0),
                    guidance.torque.moment.x,
                    guidance.torque.moment.y,
                });
                guidance.cnames[guidance.torque.thrust] = "thrust";

                #endregion
                #region Mono
                guidance.mono.problem = new CLP.Problem();
                guidance.mono.moment = new TriConstraint(
                    guidance.thrust.moment.x.shallow_clone(),
                    guidance.thrust.moment.y.shallow_clone(),
                    guidance.thrust.moment.z.shallow_clone());
                guidance.cnames[guidance.mono.moment.x] = "moment.x";
                guidance.cnames[guidance.mono.moment.y] = "moment.y";
                guidance.cnames[guidance.mono.moment.z] = "moment.z";
                guidance.mono.problem.constraints.AddRange(new CLP.Constraint[]{
                    guidance.mono.thrust = guidance.torque.thrust,
                    guidance.mono.moment.x,
                    guidance.mono.moment.y,
                    guidance.mono.moment.z
                });

                #endregion
                #region Fuel
                guidance.fuel.problem = new CLP.Problem();
                guidance.fuel.moment = guidance.mono.moment;
                guidance.fuel.problem.constraints.AddRange(new CLP.Constraint[]{
                    guidance.fuel.mono = new CLP.Constraint(guidance.mono.problem.objective, 0.0),
                    guidance.fuel.thrust = guidance.torque.thrust,
                    guidance.fuel.moment.x,
                    guidance.fuel.moment.y,
                    guidance.fuel.moment.z
                });
                guidance.cnames[guidance.fuel.mono] = "mono";

                #endregion
                #region Gimbal
                guidance.gimbal.constraints = new GuidanceProblem.Gimbal.ConstraintSet[stateWorker.gimbalCount];
                for (int i = 0; i < stateWorker.gimbalCount; ++i)
                {
                    CLP.BoundedVariable PX, NX, PY, NY, NZ;
                    CLP.BoundedVariable[] vars = new CLP.BoundedVariable[]{
                                PX = guidance.gimbal.variables.PX[i],
                                PY = guidance.gimbal.variables.PY[i],
                                NZ = guidance.gimbal.variables.NZ[i],
                                NX = guidance.gimbal.variables.NX[i],
                                NY = guidance.gimbal.variables.NY[i]
                            };
                    GuidanceProblem.Gimbal.ConstraintSet constraints = guidance.gimbal.constraints[i] = new GuidanceProblem.Gimbal.ConstraintSet();
                    guidance.cnames[constraints.bnx] = "bnx_" + i;
                    guidance.cnames[constraints.bny] = "bny_" + i;
                    guidance.cnames[constraints.bpx] = "bpx_" + i;
                    guidance.cnames[constraints.bpy] = "bpy_" + i;
                    guidance.cnames[constraints.tnx] = "tnx_" + i;
                    guidance.cnames[constraints.tny] = "tny_" + i;
                    guidance.cnames[constraints.tpx] = "tpx_" + i;
                    guidance.cnames[constraints.tpy] = "tpy_" + i;
                    for (int j = 0; j < 8; ++j)
                    {
                        guidance.thrust.problem.constraints.Add(constraints.c[j]);
                        guidance.torque.problem.constraints.Add(constraints.c[j]);
                        guidance.mono.problem.constraints.Add(constraints.c[j]);
                        guidance.fuel.problem.constraints.Add(constraints.c[j]);
                    }
                    //Construct points:
                    Vector3d origin = Vector3d.zero;
                    Vector3d apo = Vector3d.back;
                    double range = stateWorker.constants.gimbal[i].gimbalRange * (Math.PI / 180.0);
                    double s_t = Math.Sin(range);
                    double c_t = Math.Cos(range);

                    Vector3d[] lowerNormals = new Vector3d[] {
                                new Vector3d(c_t, 0.0, s_t),//px
                                new Vector3d(0.0, c_t, s_t),//py
                                new Vector3d(-c_t, 0.0, s_t),//nx
                                new Vector3d(0.0, -c_t, s_t),//ny
                                new Vector3d(c_t, 0.0, s_t),//px
                            };
                    //angle limits:
                    for (int j = 0; j < 4; ++j)
                    {
                        Vector3d normal = lowerNormals[j];
                        normal.Normalize();
                        //Equation is dot(p, normal) <= 0
                        // :. dot(p, normal) + s = 0, s >= 0
                        CLP.Constraint c = constraints.c[j];
                        c.f[NX] = -(c.f[PX] = normal.x); //x dir pair
                        c.f[NY] = -(c.f[PY] = normal.y); //y dir pair
                        c.f[NZ] = -normal.z; // thrust is neg z
                        c.f[guidance.gimbal.variables.slack[j][i]] = 1.0; //slack
                        c.RHS = 0.0; // plane intersects origin
                    }

                    //calc upper plane normal - px:
                    Vector3d upxn;
                    {
                        Vector3d pyc = Vector3d.Cross(lowerNormals[1], lowerNormals[0]);
                        pyc.z += 1.0;
                        Vector3d nyc = Vector3d.Cross(lowerNormals[0], lowerNormals[3]);
                        nyc.z += 1.0;
                        upxn = Vector3d.Cross(pyc, nyc);
                        upxn.Normalize();
                    }
                    Vector3d[] upperNormals = new Vector3d[] {
                                new Vector3d(upxn.x, 0.0, upxn.z),
                                new Vector3d(0.0, upxn.x, upxn.z),
                                new Vector3d(-upxn.x, 0.0, upxn.z),
                                new Vector3d(0.0, -upxn.x, upxn.z),
                            };
                    double uRHS = -upxn.z;
                    for (int j = 0; j < 4; ++j)
                    {
                        Vector3d normal = upperNormals[j];
                        normal.Normalize();
                        //Equation is dot(p, normal) <= dot((0,0,-1),normal)
                        // :. dot(p, normal) + s = dot((0,0,-1),normal), s >= 0
                        CLP.Constraint c = constraints.c[j + 4];
                        c.f[NX] = -(c.f[PX] = normal.x); //x dir pair
                        c.f[NY] = -(c.f[PY] = normal.y); //y dir pair
                        c.f[NZ] = -normal.z; // thrust is neg z
                        c.f[guidance.gimbal.variables.slack[j + 4][i]] = 1.0; //slack
                        c.RHS = uRHS;// Vector3d.Dot(normal, new Vector3d(0.0, 0.0, -1.0));
                    }
                }
                #endregion

                guidance.thrust.solver = new CLP.Solvers.MaxLPSolve(guidance.thrust.problem, guidance.allVariables);
                guidance.torque.solver = new CLP.Solvers.MaxLPSolve(guidance.torque.problem, guidance.allVariables);
                guidance.mono.solver = new CLP.Solvers.MaxLPSolve(guidance.mono.problem, guidance.allVariables);
                guidance.fuel.solver = new CLP.Solvers.MaxLPSolve(guidance.fuel.problem, guidance.allVariables);
                #endregion
                #region Docking
                #region Variables
                docking.cnames = new Dictionary<CLP.Constraint, string>();
                docking.vnames = new Dictionary<CLP.BoundedVariable, string>();
                docking.portVariables = guidance.portVariables;//new CLP.BoundedVariable[stateWorker.portCount];
                //docking.portVariables.populate(_ => new CLP.BoundedVariable(0.0, 1.0));
                for (int i = 0; i < stateWorker.portCount; ++i)
                    docking.vnames[docking.portVariables[i]] = "p_" + i;
                docking.wheelVariables = guidance.wheelVariables;
                docking.vnames[docking.wheelVariables.x] = "wheel.x";
                docking.vnames[docking.wheelVariables.y] = "wheel.x";
                docking.vnames[docking.wheelVariables.z] = "wheel.z";
                docking.allVariables = docking.portVariables.Concat(new[] { docking.wheelVariables.x, docking.wheelVariables.y, docking.wheelVariables.z });
                #endregion
                #region Thrust
                docking.thrust.problem = new CLP.Problem();
                docking.thrust.moment = new TriConstraint();
                docking.cnames[docking.thrust.moment.x] = "moment.x";
                docking.cnames[docking.thrust.moment.y] = "moment.y";
                docking.cnames[docking.thrust.moment.z] = "moment.z";
                docking.thrust.thrust = new BiConstraint();
                docking.cnames[docking.thrust.thrust.x] = "thrust.x";
                docking.cnames[docking.thrust.thrust.y] = "thrust.y";
                docking.thrust.problem.constraints.AddRange(new CLP.Constraint[]{
                    docking.thrust.moment.x,
                    docking.thrust.moment.y,
                    docking.thrust.moment.z,
                    docking.thrust.thrust.x,
                    docking.thrust.thrust.y,
                });
                docking.thrust.solver = new CLP.Solvers.MaxLPSolve(docking.thrust.problem, docking.allVariables);
                #endregion
                #region Torque
                docking.torque.problem = new CLP.Problem();
                docking.torque.thrust = new TriConstraint();
                docking.cnames[docking.torque.thrust.x] = "thrust.x";
                docking.cnames[docking.torque.thrust.y] = "thrust.y";
                docking.cnames[docking.torque.thrust.z] = "thrust.z";
                docking.torque.moment = new BiConstraint();
                docking.cnames[docking.torque.moment.x] = "moment.x";
                docking.cnames[docking.torque.moment.y] = "moment.y";
                docking.torque.problem.constraints.AddRange(new CLP.Constraint[]{
                    docking.torque.thrust.x,
                    docking.torque.thrust.y,
                    docking.torque.thrust.z,
                    docking.torque.moment.x,
                    docking.torque.moment.y
                });
                docking.torque.solver = new CLP.Solvers.MaxLPSolve(docking.torque.problem, docking.allVariables);
                #endregion
                #region Mono
                docking.mono.problem = new CLP.Problem();
                docking.mono.thrust = docking.torque.thrust;
                docking.mono.moment = new TriConstraint(
                    docking.thrust.moment.x.shallow_clone(),
                    docking.thrust.moment.y.shallow_clone(),
                    docking.thrust.moment.z.shallow_clone());
                docking.cnames[docking.mono.moment.x] = "moment.x";
                docking.cnames[docking.mono.moment.y] = "moment.y";
                docking.cnames[docking.mono.moment.z] = "moment.z";
                docking.mono.problem.constraints.AddRange(new CLP.Constraint[]{
                    docking.mono.thrust.x,
                    docking.mono.thrust.y,
                    docking.mono.thrust.z,
                    docking.mono.moment.x,
                    docking.mono.moment.y,
                    docking.mono.moment.z
                });
                docking.mono.solver = new CLP.Solvers.MaxLPSolve(docking.mono.problem, docking.allVariables);
                #endregion
                #endregion
                #endregion
            }

            guidance.wheelVariables.x.lowerBound = -stateWorker.wheelStrength.x;
            guidance.wheelVariables.x.upperBound = stateWorker.wheelStrength.x;
            guidance.wheelVariables.y.lowerBound = -stateWorker.wheelStrength.y;
            guidance.wheelVariables.y.upperBound = stateWorker.wheelStrength.y;
            guidance.wheelVariables.z.lowerBound = -stateWorker.wheelStrength.z;
            guidance.wheelVariables.z.upperBound = stateWorker.wheelStrength.z;

            //Determine the mode to use - Guidance or docking:
            if (stateWorker.alwaysGuidance || stateWorker.throttleInput > 1E-5)
            {
                stage = "guidance";
                #region Guidance
                float targetThrottle = Mathf.Clamp(stateWorker.throttleInput / stateWorker.frameCount, 0.0f, 1.0f);
                Vector3 thrustDirection = Vector3.zero;
                #region Calculate target thrust direction
                for (int i = 0; i < stateWorker.staticCount; ++i)
                {
                    if (stateWorker.variables.static_[i].operable)
                    {
                        int thrustCount = stateWorker.constants.static_[i].thrustCount;
                        float thrust = stateWorker.constants.static_[i].maxThrust / thrustCount;
                        for (int j = 0; j < thrustCount; ++j)
                        {
                            thrustDirection += thrust * stateWorker.variables.static_[i].direction[j];
                        }
                    }
                }
                for (int i = 0; i < stateWorker.gimbalCount; ++i)
                {
                    if (stateWorker.variables.gimbal[i].operable)
                    {
                        int thrustCount = stateWorker.constants.gimbal[i].thrustCount;
                        float thrust = stateWorker.constants.gimbal[i].maxThrust / thrustCount;
                        for (int j = 0; j < thrustCount; ++j)
                        {
                            thrustDirection += thrust * (stateWorker.variables.gimbal[i].orientation[j] * Vector3.back);
                        }
                    }
                }
                thrustDirection.Normalize();
                #endregion

                if (thrustDirection.sqrMagnitude < 1E-6)
                {
                    stateWorker.status = SharedState.Status.BadEngineConfig;
                    return;
                }

                #region Set up coefficients for the reaction wheels
                //Reaction wheel torques need to be transformed by the reference frame vector
                if (stateWorker.wheelStrength.sqrMagnitude > 1E-5)
                {
                    Vector3d xcomp = _vessel.transform.InverseTransformDirection(_vessel.ReferenceTransform.rotation * new Vector3d(-1.0, 0.0, 0.0));
                    Vector3d ycomp = _vessel.transform.InverseTransformDirection(_vessel.ReferenceTransform.rotation * new Vector3d(0.0, -1.0, 0.0));
                    Vector3d zcomp = _vessel.transform.InverseTransformDirection(_vessel.ReferenceTransform.rotation * new Vector3d(0.0, 0.0, -1.0));
                    guidance.thrust.moment.x.f[guidance.wheelVariables.x] = xcomp.x;
                    guidance.thrust.moment.y.f[guidance.wheelVariables.x] = xcomp.y;
                    guidance.thrust.moment.z.f[guidance.wheelVariables.x] = xcomp.z;

                    guidance.thrust.moment.x.f[guidance.wheelVariables.y] = ycomp.x;
                    guidance.thrust.moment.y.f[guidance.wheelVariables.y] = ycomp.y;
                    guidance.thrust.moment.z.f[guidance.wheelVariables.y] = ycomp.z;

                    guidance.thrust.moment.x.f[guidance.wheelVariables.z] = zcomp.x;
                    guidance.thrust.moment.y.f[guidance.wheelVariables.z] = zcomp.y;
                    guidance.thrust.moment.z.f[guidance.wheelVariables.z] = zcomp.z;
                }
                else
                {
                    guidance.thrust.moment.x.f[guidance.wheelVariables.x] = 0.0;
                    guidance.thrust.moment.y.f[guidance.wheelVariables.x] = 0.0;
                    guidance.thrust.moment.z.f[guidance.wheelVariables.x] = 0.0;
                    
                    guidance.thrust.moment.x.f[guidance.wheelVariables.y] = 0.0;
                    guidance.thrust.moment.y.f[guidance.wheelVariables.y] = 0.0;
                    guidance.thrust.moment.z.f[guidance.wheelVariables.y] = 0.0;

                    guidance.thrust.moment.x.f[guidance.wheelVariables.z] = 0.0;
                    guidance.thrust.moment.y.f[guidance.wheelVariables.z] = 0.0;
                    guidance.thrust.moment.z.f[guidance.wheelVariables.z] = 0.0;
                }
                #endregion

                #region Set up coefficients for RCS ports
                for (int i = 0; i < stateWorker.portCount; ++i)
                {
                    CLP.BoundedVariable var = guidance.portVariables[i];

                    guidance.mono.problem.objective[var] = -stateWorker.variables.port[i].maxFlow;

                    if (stateWorker.variables.port[i].operable)
                    {
                        Vector3 force = stateWorker.constants.port[i].maxThrust * stateWorker.variables.port[i].direction;
                        Vector3 offset = stateWorker.variables.port[i].position - stateWorker.COM;
                        Vector3 moment = Vector3.Cross(offset, force);
                        guidance.thrust.problem.objective[var] = Vector3.Dot(force, thrustDirection);
                        guidance.thrust.moment.x.f[var] = moment.x;
                        guidance.thrust.moment.y.f[var] = moment.y;
                        guidance.thrust.moment.z.f[var] = moment.z;
                    }
                    else
                    {
                        guidance.thrust.problem.objective[var] = 0.0;
                        guidance.thrust.moment.x.f[var] = 0.0;
                        guidance.thrust.moment.y.f[var] = 0.0;
                        guidance.thrust.moment.z.f[var] = 0.0;
                    }
                }
                #endregion

                #region Set up coefficients for static engines
                for (int i = 0; i < stateWorker.staticCount; ++i)
                {
                    CLP.BoundedVariable var = guidance.staticVariables[i];

                    guidance.fuel.problem.objective[var] = -stateWorker.variables.static_[i].maxFlow;

                    if (stateWorker.variables.static_[i].operable)
                    {
                        double mag = 0.0;
                        double mX = 0.0, mY = 0.0, mZ = 0.0;

                        int thrustCount = stateWorker.constants.static_[i].thrustCount;
                        float thrust = stateWorker.constants.static_[i].maxThrust / thrustCount;
                        for (int j = 0; j < thrustCount; ++j)
                        {
                            Vector3 force = thrust * stateWorker.variables.static_[i].direction[j];
                            Vector3 offset = stateWorker.variables.static_[i].position[j] - stateWorker.COM;
                            Vector3 moment = Vector3.Cross(offset, force);
                            mag += Vector3.Dot(force, thrustDirection);
                            mX += moment.x;
                            mY += moment.y;
                            mZ += moment.z;
                        }
                        guidance.thrust.problem.objective[var] = mag;
                        guidance.thrust.moment.x.f[var] = mX;
                        guidance.thrust.moment.y.f[var] = mY;
                        guidance.thrust.moment.z.f[var] = mZ;
                    }
                    else
                    {
                        guidance.thrust.problem.objective[var] = 0.0;
                        guidance.thrust.moment.x.f[var] = 0.0;
                        guidance.thrust.moment.y.f[var] = 0.0;
                        guidance.thrust.moment.z.f[var] = 0.0;
                    }
                }
                #endregion

                #region Set up coefficients for gimballed engines
                {
                    //Debug.Log("Setting up gimbal constraints/objectives");
                    CLP.BoundedVariable[] vars;
                    double[] mag = new double[3];
                    double[] mX = new double[3], mY = new double[3], mZ = new double[3];
                    Vector3[] dir = new Vector3[3];
                    int i, j, k;
                    for (i = 0; i < stateWorker.gimbalCount; ++i)
                    {
                        CLP.BoundedVariable PX, NX, PY, NY, NZ;
                        vars = new CLP.BoundedVariable[]{
                                PX = guidance.gimbal.variables.PX[i],
                                PY = guidance.gimbal.variables.PY[i],
                                NZ = guidance.gimbal.variables.NZ[i],
                                NX = guidance.gimbal.variables.NX[i],
                                NY = guidance.gimbal.variables.NY[i]
                            };
                        // fuel minimisation:
                        // cost is a linear approximation of pythagoras.
                        guidance.fuel.problem.objective[guidance.gimbal.variables.NZ[i]] = -stateWorker.variables.gimbal[i].maxFlow;
                        if (stateWorker.variables.gimbal[i].locked)
                        {

                            if (stateWorker.variables.gimbal[i].operable)
                            {
                                double _mag = 0.0;
                                double _mX = 0.0, _mY = 0.0, _mZ = 0.0;

                                int thrustCount = stateWorker.constants.gimbal[i].thrustCount;
                                float thrust = stateWorker.constants.gimbal[i].maxThrust / thrustCount;
                                for (j = 0; j < thrustCount; ++j)
                                {
                                    Vector3 force = thrust * (stateWorker.variables.gimbal[i].orientation[j] * Vector3.back);
                                    Vector3 offset = stateWorker.variables.gimbal[i].position[j] - stateWorker.COM;
                                    Vector3 moment = Vector3.Cross(offset, force);
                                    _mag += Vector3.Dot(force, thrustDirection);
                                    _mX += moment.x;
                                    _mY += moment.y;
                                    _mZ += moment.z;
                                }
                                guidance.thrust.problem.objective[NZ] = _mag;
                                guidance.thrust.moment.x.f[NZ] = _mX;
                                guidance.thrust.moment.y.f[NZ] = _mY;
                                guidance.thrust.moment.z.f[NZ] = _mZ;
                            }
                            else
                            {
                                guidance.thrust.problem.objective[NZ] = 0.0;
                                guidance.thrust.moment.x.f[NZ] = 0.0;
                                guidance.thrust.moment.y.f[NZ] = 0.0;
                                guidance.thrust.moment.z.f[NZ] = 0.0;
                            }
                            guidance.thrust.problem.objective[PX] = 0.0;
                            guidance.thrust.problem.objective[NX] = 0.0;
                            guidance.thrust.problem.objective[PY] = 0.0;
                            guidance.thrust.problem.objective[NY] = 0.0;

                            guidance.thrust.moment.x.f[PX] = 0.0;
                            guidance.thrust.moment.x.f[NX] = 0.0;
                            guidance.thrust.moment.x.f[PY] = 0.0;
                            guidance.thrust.moment.x.f[NY] = 0.0;

                            guidance.thrust.moment.y.f[PX] = 0.0;
                            guidance.thrust.moment.y.f[NX] = 0.0;
                            guidance.thrust.moment.y.f[PY] = 0.0;
                            guidance.thrust.moment.y.f[NY] = 0.0;

                            guidance.thrust.moment.z.f[PX] = 0.0;
                            guidance.thrust.moment.z.f[NX] = 0.0;
                            guidance.thrust.moment.z.f[PY] = 0.0;
                            guidance.thrust.moment.z.f[NY] = 0.0;

                            PX.upperBound = 0.0;
                            NX.upperBound = 0.0;
                            PY.upperBound = 0.0;
                            NY.upperBound = 0.0;
                        }
                        else
                        {
                            PX.upperBound = double.PositiveInfinity;
                            NX.upperBound = double.PositiveInfinity;
                            PY.upperBound = double.PositiveInfinity;
                            NY.upperBound = double.PositiveInfinity;

                            // make sure that corner points match the original problem
                            double r = Math.Sin(stateWorker.constants.gimbal[i].gimbalRange);
                            double hscale = (Math.Sqrt(1.0 - 2.0 * r * r) * 0.5 - 0.5) / r; //should really precompute this
                            double hflow = stateWorker.variables.gimbal[i].maxFlow * hscale;
                            guidance.fuel.problem.objective[PX] = hflow;
                            guidance.fuel.problem.objective[NX] = hflow;
                            guidance.fuel.problem.objective[PY] = hflow;
                            guidance.fuel.problem.objective[NY] = hflow;
                            
                            if (stateWorker.variables.gimbal[i].operable)
                            {
                                mag.populate(0.0);
                                mX.populate(0.0);
                                mY.populate(0.0);
                                mZ.populate(0.0);
                                int thrustCount = stateWorker.constants.gimbal[i].thrustCount;
                                float thrust = stateWorker.constants.gimbal[i].maxThrust / thrustCount;
                                //Vector3[] ldir = new Vector3[] { new Vector3(1.0f, 0.0f, 0.0f), new Vector3(0.0f, 1.0f, 0.0f), Vector3.back };

                                for (j = 0; j < thrustCount; ++j)
                                {
                                    dir[0] = stateWorker.variables.gimbal[i].orientation[j] * Vector3.left;
                                    dir[1] = stateWorker.variables.gimbal[i].orientation[j] * Vector3.down;
                                    dir[2] = stateWorker.variables.gimbal[i].orientation[j] * Vector3.back;
                                    for (k = 0; k < 3; ++k)
                                    {
                                        Vector3 force = thrust * dir[k];
                                        Vector3 offset = stateWorker.variables.gimbal[i].position[j] - stateWorker.COM;
                                        Vector3 moment = Vector3.Cross(offset, force);
                                        mag[k] += Vector3.Dot(force, thrustDirection);
                                        mX[k] += moment.x;
                                        mY[k] += moment.y;
                                        mZ[k] += moment.z;
                                    }
                                }
                                guidance.thrust.problem.objective[PX] = mag[0];
                                guidance.thrust.problem.objective[NX] = -mag[0];
                                guidance.thrust.problem.objective[PY] = mag[1];
                                guidance.thrust.problem.objective[NY] = -mag[1];
                                guidance.thrust.problem.objective[NZ] = mag[2];

                                guidance.thrust.moment.x.f[PX] = mX[0];
                                guidance.thrust.moment.x.f[NX] = -mX[0];
                                guidance.thrust.moment.x.f[PY] = mX[1];
                                guidance.thrust.moment.x.f[NY] = -mX[1];
                                guidance.thrust.moment.x.f[NZ] = mX[2];

                                guidance.thrust.moment.y.f[PX] = mY[0];
                                guidance.thrust.moment.y.f[NX] = -mY[0];
                                guidance.thrust.moment.y.f[PY] = mY[1];
                                guidance.thrust.moment.y.f[NY] = -mY[1];
                                guidance.thrust.moment.y.f[NZ] = mY[2];

                                guidance.thrust.moment.z.f[PX] = mZ[0];
                                guidance.thrust.moment.z.f[NX] = -mZ[0];
                                guidance.thrust.moment.z.f[PY] = mZ[1];
                                guidance.thrust.moment.z.f[NY] = -mZ[1];
                                guidance.thrust.moment.z.f[NZ] = mZ[2];
                            }
                            else
                            {
                                guidance.thrust.problem.objective[PX] = 0.0;
                                guidance.thrust.problem.objective[NX] = 0.0;
                                guidance.thrust.problem.objective[PY] = 0.0;
                                guidance.thrust.problem.objective[NY] = 0.0;
                                guidance.thrust.problem.objective[NZ] = 0.0;

                                guidance.thrust.moment.x.f[PX] = 0.0;
                                guidance.thrust.moment.x.f[NX] = 0.0;
                                guidance.thrust.moment.x.f[PY] = 0.0;
                                guidance.thrust.moment.x.f[NY] = 0.0;
                                guidance.thrust.moment.x.f[NZ] = 0.0;

                                guidance.thrust.moment.y.f[PX] = 0.0;
                                guidance.thrust.moment.y.f[NX] = 0.0;
                                guidance.thrust.moment.y.f[PY] = 0.0;
                                guidance.thrust.moment.y.f[NY] = 0.0;
                                guidance.thrust.moment.y.f[NZ] = 0.0;

                                guidance.thrust.moment.z.f[PX] = 0.0;
                                guidance.thrust.moment.z.f[NX] = 0.0;
                                guidance.thrust.moment.z.f[PY] = 0.0;
                                guidance.thrust.moment.z.f[NY] = 0.0;
                                guidance.thrust.moment.z.f[NZ] = 0.0;
                            }
                        }
                    }
                }
                #endregion

                // torque solver dependencies
                bool torqueEnabled = false;
                Vector3 torqueInput = stateWorker.torqueInput / stateWorker.frameCount;

                float torqueInputMagnitude = torqueInput.magnitude;

                Vector3d torqueDirection = Vector3d.zero;
                int i_axis = 0, d_axis1 = 0, d_axis2 = 0;
                double d_coeff1 = 0, d_coeff2 = 0;
                if (torqueInputMagnitude > 1E-4)
                {
                    torqueEnabled = true;
                    torqueDirection = torqueInput / torqueInputMagnitude;
                    if (torqueInputMagnitude > 1.0f) torqueInputMagnitude = 1.0f;

                    // find best axis to use as measure - largest coefficient
                    Vector3d magDir = new Vector3d(Math.Abs(torqueDirection.x), Math.Abs(torqueDirection.y), Math.Abs(torqueDirection.z));
                    i_axis = magDir.x > magDir.y ? (magDir.x > magDir.z ? 0 : 2) : (magDir.y > magDir.z ? 1 : 2);
                    // other axes
                    d_axis1 = (i_axis + 1) % 3;
                    d_axis2 = (i_axis + 2) % 3;
                    d_coeff1 = torqueDirection[d_axis1] / torqueDirection[i_axis];
                    d_coeff2 = torqueDirection[d_axis2] / torqueDirection[i_axis];

                    foreach (CLP.BoundedVariable var in guidance.allVariables)
                    {
                        guidance.torque.problem.objective[var] = Vector3d.Dot(
                            new Vector3d(
                                guidance.thrust.moment.x.f[var],
                                guidance.thrust.moment.y.f[var],
                                guidance.thrust.moment.z.f[var]),
                            torqueDirection);
                        guidance.torque.moment.x.f[var] = guidance.thrust.moment[d_axis1].f[var] - d_coeff1 * guidance.thrust.moment[i_axis].f[var];
                        guidance.torque.moment.y.f[var] = guidance.thrust.moment[d_axis2].f[var] - d_coeff2 * guidance.thrust.moment[i_axis].f[var];
                    }
                }

                #region Solve to find maximum thrust
                guidance.thrust.solver.solve();

                if (guidance.thrust.solver.status != CLP.Solvers.Solver.Status.Optimal)
                {
                    stateWorker.status = SharedState.Status.ThrustSolverFailure;
                    return;
                }
                double maxThrust = guidance.thrust.solver.objective_value;
                //Debug.Log("Max thrust: " + maxThrust);
                double targetThrust = maxThrust * targetThrottle * 0.98;
                //Debug.Log("Target thrust: " + targetThrust);
                #endregion

                guidance.torque.thrust.RHS = targetThrust;

                Vector3d targetTorque = Vector3d.zero;
                double maxTorque = 0.0;
                if (torqueEnabled)
                {
                    #region Solve to find maximum torque
                    

                    guidance.torque.solver.solve();
                    if (guidance.torque.solver.status != CLP.Solvers.Solver.Status.Optimal)
                    {
                        stateWorker.status = SharedState.Status.TorqueSolverFailure;
                        return;
                    }
                    maxTorque = guidance.torque.solver.objective_value;
                    //Debug.Log("Max torque: " + maxTorque);
                    double targetTorqueMagnitude = 0.98 * torqueInputMagnitude * maxTorque;
                    //Debug.Log("Target torque: " + maxTorque);
                    targetTorque = targetTorqueMagnitude * torqueDirection / torqueDirection.magnitude;
                    #endregion
                }

                #region Solve to find minimum monopropellant usage
                guidance.mono.moment.x.RHS = targetTorque.x;
                guidance.mono.moment.y.RHS = targetTorque.y;
                guidance.mono.moment.z.RHS = targetTorque.z;
                //Debug.Log("Mono solver state:\n" + guidance.mono.solver.dump(guidance.vnames, guidance.cnames));
                guidance.mono.solver.solve();
                if (guidance.mono.solver.status != CLP.Solvers.Solver.Status.Optimal)
                {
                    stateWorker.status = SharedState.Status.MonoSolverFailure;
                    return;
                }
                double minRCS = -guidance.mono.solver.objective_value;
                //Debug.Log("Min mono: " + minRCS);
                #endregion

                #region Solve to find minimum fuel usage
                guidance.fuel.mono.RHS = -minRCS;
                //Debug.Log("Fuel solver state:\n" + guidance.fuel.solver.dump(guidance.vnames, guidance.cnames));
                guidance.fuel.solver.solve();
                if (guidance.fuel.solver.status != CLP.Solvers.Solver.Status.Optimal)
                {
                    stateWorker.status = SharedState.Status.FuelSolverFailure;
                    return;
                }
                //Debug.Log("Min fuel: " + -guidance.fuel.solver.objective_value);
                #endregion

                #region Set final output values
                stage = "guidance, set output values";
                stateWorker.wheelTorque.x = (float)guidance.fuel.solver.values[guidance.wheelVariables.x];
                stateWorker.wheelTorque.y = (float)guidance.fuel.solver.values[guidance.wheelVariables.y];
                stateWorker.wheelTorque.z = (float)guidance.fuel.solver.values[guidance.wheelVariables.z];
                for (int i = 0; i < stateWorker.portCount; ++i)
                    stateWorker.portThrusts[i] = guidance.fuel.solver.values[guidance.portVariables[i]];
                for (int i = 0; i < stateWorker.staticCount; ++i)
                    stateWorker.staticThrusts[i] = guidance.fuel.solver.values[guidance.staticVariables[i]];
                for (int i = 0; i < stateWorker.gimbalCount; ++i)
                {
                    Vector3d thrust = new Vector3d(
                        guidance.fuel.solver.values[guidance.gimbal.variables.PX[i]] - guidance.fuel.solver.values[guidance.gimbal.variables.NX[i]],
                        guidance.fuel.solver.values[guidance.gimbal.variables.PY[i]] - guidance.fuel.solver.values[guidance.gimbal.variables.NY[i]],
                        -guidance.fuel.solver.values[guidance.gimbal.variables.NZ[i]]);
                    double mag = thrust.magnitude;
                    stateWorker.gimbalThrusts[i] = mag;
                    //calculate angles:
                    double rx = Math.Atan(thrust.y / thrust.z);
                    double ry = Math.Atan(thrust.x / (mag * Math.Cos(rx)));
                    stateWorker.gimbalSettings[i].x = (float)(rx * (180.0 / Math.PI));
                    stateWorker.gimbalSettings[i].y = (float)(ry * (180.0 / Math.PI));
                }

                stateWorker.status = SharedState.Status.Solved;
                #endregion

                #endregion
            }
            else
            {
                #region Docking

                Vector3 linearInput = stateWorker.linearInput / stateWorker.frameCount;
                Vector3 linearDirection = Vector3.zero;
                float linearMagnitude = 0.0f;
                bool thrustEnabled = false;

                Vector3 torqueInput = stateWorker.torqueInput / stateWorker.frameCount;
                Vector3 torqueDirection = Vector3.zero;
                float torqueMagnitude = 0.0f;
                bool torqueEnabled = false;

                if (linearInput.sqrMagnitude > 1E-6)
                {
                    thrustEnabled = true;
                    linearMagnitude = linearInput.magnitude;
                    linearDirection = linearInput / linearMagnitude;
                    if (linearMagnitude > 1.0f) linearMagnitude = 1.0f;
                }

                if (torqueInput.sqrMagnitude > 1E-6)
                {
                    torqueEnabled = true;
                    torqueMagnitude = torqueInput.magnitude;
                    torqueDirection = torqueInput / torqueMagnitude;
                    if (torqueMagnitude > 1.0f) torqueMagnitude = 1.0f;
                }

                #region Set up coefficients for the reaction wheels
                //Reaction wheel torques need to be transformed by the reference frame vector
                if (stateWorker.wheelStrength.sqrMagnitude > 1E-5)
                {
                    Vector3d xcomp = _vessel.transform.InverseTransformDirection(_vessel.ReferenceTransform.rotation * new Vector3d(-1.0, 0.0, 0.0));
                    Vector3d ycomp = _vessel.transform.InverseTransformDirection(_vessel.ReferenceTransform.rotation * new Vector3d(0.0, -1.0, 0.0));
                    Vector3d zcomp = _vessel.transform.InverseTransformDirection(_vessel.ReferenceTransform.rotation * new Vector3d(0.0, 0.0, -1.0));
                    var x = docking.wheelVariables.x;
                    var y = docking.wheelVariables.y;
                    var z = docking.wheelVariables.z;
                    docking.thrust.moment.x.f[x] = xcomp.x;
                    docking.thrust.moment.y.f[x] = xcomp.y;
                    docking.thrust.moment.z.f[x] = xcomp.z;

                    docking.thrust.moment.x.f[y] = ycomp.x;
                    docking.thrust.moment.y.f[y] = ycomp.y;
                    docking.thrust.moment.z.f[y] = ycomp.z;

                    docking.thrust.moment.x.f[z] = zcomp.x;
                    docking.thrust.moment.y.f[z] = zcomp.y;
                    docking.thrust.moment.z.f[z] = zcomp.z;

                    docking.torque.problem.objective[x] = Vector3.Dot(xcomp, torqueDirection);
                    docking.torque.problem.objective[y] = Vector3.Dot(ycomp, torqueDirection);
                    docking.torque.problem.objective[z] = Vector3.Dot(zcomp, torqueDirection);
                }
                else
                {
                    var x = docking.wheelVariables.x;
                    var y = docking.wheelVariables.y;
                    var z = docking.wheelVariables.z;

                    docking.thrust.moment.x.f[x] = 0.0;
                    docking.thrust.moment.y.f[x] = 0.0;
                    docking.thrust.moment.z.f[x] = 0.0;

                    docking.thrust.moment.x.f[y] = 0.0;
                    docking.thrust.moment.y.f[y] = 0.0;
                    docking.thrust.moment.z.f[y] = 0.0;

                    docking.thrust.moment.x.f[z] = 0.0;
                    docking.thrust.moment.y.f[z] = 0.0;
                    docking.thrust.moment.z.f[z] = 0.0;

                    docking.torque.problem.objective[x] = 0.0;
                    docking.torque.problem.objective[y] = 0.0;
                    docking.torque.problem.objective[z] = 0.0;
                }
                #endregion

                #region Set up coefficients for RCS ports
                for (int i = 0; i < stateWorker.portCount; ++i)
                {
                    CLP.BoundedVariable var = docking.portVariables[i];

                    docking.mono.problem.objective[var] = -stateWorker.variables.port[i].maxFlow;

                    if (stateWorker.variables.port[i].operable)
                    {
                        Vector3 force = stateWorker.constants.port[i].maxThrust * stateWorker.variables.port[i].direction;
                        Vector3 offset = stateWorker.variables.port[i].position - stateWorker.COM;
                        Vector3 moment = Vector3.Cross(offset, force);
                        
                        docking.torque.thrust.x.f[var] = force.x;
                        docking.torque.thrust.y.f[var] = force.y;
                        docking.torque.thrust.z.f[var] = force.z;
                        docking.thrust.moment.x.f[var] = moment.x;
                        docking.thrust.moment.y.f[var] = moment.y;
                        docking.thrust.moment.z.f[var] = moment.z;

                        docking.thrust.problem.objective[var] = Vector3.Dot(force, linearDirection);
                        docking.torque.problem.objective[var] = Vector3.Dot(moment, torqueDirection);
                    }
                    else
                    {
                        docking.torque.thrust.x.f[var] = 0.0;
                        docking.torque.thrust.y.f[var] = 0.0;
                        docking.torque.thrust.z.f[var] = 0.0;
                        docking.thrust.moment.x.f[var] = 0.0;
                        docking.thrust.moment.y.f[var] = 0.0;
                        docking.thrust.moment.z.f[var] = 0.0;

                        docking.thrust.problem.objective[var] = 0.0;
                        docking.torque.problem.objective[var] = 0.0;
                    }
                }

                Vector3 targetThrust = Vector3.zero;
                Vector3 targetTorque = Vector3.zero;

                if (thrustEnabled)
                {
                    Vector3 magDir = new Vector3(Mathf.Abs(linearDirection.x), Mathf.Abs(linearDirection.y), Mathf.Abs(linearDirection.z));
                    int i_axis = magDir.x > magDir.y ? (magDir.x > magDir.z ? 0 : 2) : (magDir.y > magDir.z ? 1 : 2);
                    // other axes
                    int d_axis1 = (i_axis + 1) % 3;
                    int d_axis2 = (i_axis + 2) % 3;
                    float d_coeff1 = linearDirection[d_axis1] / linearDirection[i_axis];
                    float d_coeff2 = linearDirection[d_axis2] / linearDirection[i_axis];
                    for (int i = 0; i < stateWorker.portCount; ++i)
                    {
                        CLP.BoundedVariable var = docking.portVariables[i];
                        docking.thrust.thrust.x.f[var] = docking.torque.thrust[d_axis1].f[var] - d_coeff1 * docking.torque.thrust[i_axis].f[var];
                        docking.thrust.thrust.y.f[var] = docking.torque.thrust[d_axis2].f[var] - d_coeff2 * docking.torque.thrust[i_axis].f[var];
                    }

                    docking.thrust.solver.solve();

                    if (docking.thrust.solver.status != CLP.Solvers.Solver.Status.Optimal)
                    {
                        stateWorker.status = SharedState.Status.ThrustSolverFailure;
                        return;
                    }

                    double maxThrust = docking.thrust.solver.objective_value;
                    //Debug.Log("Max thrust: " + maxThrust);
                    targetThrust = (maxThrust * linearMagnitude * 0.98) * (Vector3d)linearDirection;
                    //Debug.Log("Target thrust: " + targetThrust);
                }

                docking.torque.thrust.x.RHS = targetThrust.x;
                docking.torque.thrust.y.RHS = targetThrust.y;
                docking.torque.thrust.z.RHS = targetThrust.z;

                if (torqueEnabled)
                {
                    Vector3 magDir = new Vector3(Mathf.Abs(torqueDirection.x), Mathf.Abs(torqueDirection.y), Mathf.Abs(torqueDirection.z));
                    int i_axis = magDir.x > magDir.y ? (magDir.x > magDir.z ? 0 : 2) : (magDir.y > magDir.z ? 1 : 2);
                    // other axes
                    int d_axis1 = (i_axis + 1) % 3;
                    int d_axis2 = (i_axis + 2) % 3;
                    float d_coeff1 = torqueDirection[d_axis1] / torqueDirection[i_axis];
                    float d_coeff2 = torqueDirection[d_axis2] / torqueDirection[i_axis];
                    foreach (CLP.BoundedVariable var in docking.allVariables)
                    {
                        docking.torque.moment.x.f[var] = docking.thrust.moment[d_axis1].f[var] - d_coeff1 * docking.thrust.moment[i_axis].f[var];
                        docking.torque.moment.y.f[var] = docking.thrust.moment[d_axis2].f[var] - d_coeff2 * docking.thrust.moment[i_axis].f[var];
                    }

                    //Debug.Log("Torque solver state:\n" + docking.torque.solver.dump(docking.vnames, docking.cnames));
                    docking.torque.solver.solve();

                    if (docking.torque.solver.status != CLP.Solvers.Solver.Status.Optimal)
                    {
                        stateWorker.status = SharedState.Status.TorqueSolverFailure;
                        return;
                    }

                    double maxTorque = docking.torque.solver.objective_value;
                    //Debug.Log("Max torque: " + maxTorque);
                    targetTorque = (maxTorque * torqueMagnitude * 0.98) * (Vector3d)torqueDirection;
                    //Debug.Log("Target torque: " + targetTorque);
                }

                docking.mono.moment.x.RHS = targetTorque.x;
                docking.mono.moment.y.RHS = targetTorque.y;
                docking.mono.moment.z.RHS = targetTorque.z;

                docking.mono.solver.solve();
                if (docking.mono.solver.status != CLP.Solvers.Solver.Status.Optimal)
                {
                    stateWorker.status = SharedState.Status.FuelSolverFailure;
                    return;
                }

                stateWorker.wheelTorque.x = (float)docking.mono.solver.values[docking.wheelVariables.x];
                stateWorker.wheelTorque.y = (float)docking.mono.solver.values[docking.wheelVariables.y];
                stateWorker.wheelTorque.z = (float)docking.mono.solver.values[docking.wheelVariables.z];
                for (int i = 0; i < stateWorker.portCount; ++i)
                    stateWorker.portThrusts[i] = docking.mono.solver.values[docking.portVariables[i]];
                for (int i = 0; i < stateWorker.staticCount; ++i)
                    stateWorker.staticThrusts[i] = 0.0;
                for (int i = 0; i < stateWorker.gimbalCount; ++i)
                {
                    stateWorker.gimbalThrusts[i] = 0.0;
                    stateWorker.gimbalSettings[i].x = 0.0f;
                    stateWorker.gimbalSettings[i].y = 0.0f;
                }
                stateWorker.status = SharedState.Status.Solved;
                #endregion
                #endregion
            }
        }

        private void copyOutput()
        {
            stateLocal.wheelTorque = stateWorker.wheelTorque;
            stateWorker.portThrusts.CopyTo(stateLocal.portThrusts, 0);
            stateWorker.staticThrusts.CopyTo(stateLocal.staticThrusts, 0);
            stateWorker.gimbalThrusts.CopyTo(stateLocal.gimbalThrusts, 0);
            stateWorker.gimbalSettings.CopyTo(stateLocal.gimbalSettings, 0);
            stateLocal.status = stateWorker.status;
        }

        public CombinedMode()
        {
            worker = new SequentialIterativeWorker(copyInput, work, copyOutput);
        }
        public override Vessel vessel
        {
            get
            {
                return _vessel;
            }
            set
            {
                _vessel = value;
            }
        }

        public override void onStructuralChange()
        {
            layoutChanged = true;
        }

        public override void onFrame()
        {
            try
            {
                #region detect mode-specific changes
                
                #endregion
                worker.update(resultsAvailable =>
                    {
                        #region Update input

                        #region update control input state
                        Vector3 linearInput = new Vector3(_vessel.ctrlState.X, _vessel.ctrlState.Z, _vessel.ctrlState.Y);
                        Vector3 angularInput = new Vector3(_vessel.ctrlState.pitch, _vessel.ctrlState.roll, _vessel.ctrlState.yaw);
                        linearInput.Scale(rcsThrustStrength);
                        angularInput.Scale(_vessel.ctrlState.mainThrottle > 1E-5 ? guidanceTorqueStrength : dockingTorqueStrength);
                        stateLocal.linearInput += -_vessel.transform.InverseTransformDirection(_vessel.ReferenceTransform.rotation * linearInput);
                        stateLocal.torqueInput += -_vessel.transform.InverseTransformDirection(_vessel.ReferenceTransform.rotation * angularInput);
                        stateLocal.throttleInput += _vessel.ctrlState.mainThrottle * throttleStrength;
                        ++stateLocal.frameCount;
                        #endregion

                        stateLocal.layoutChanged = layoutChanged;
                        if (layoutChanged)
                        {
                            wheels = FlightTimeEnhancer.getAllModules<ReactionWheelController>(_vessel).ToArray();
                            portRefs = _vessel.parts.SelectMany(p => p.Modules.Cast<PartModule>().Where(m => m is MyRCSModule).SelectMany(_m =>
                            {
                                MyRCSModule m = (MyRCSModule)_m;
                                int i = 0;
                                return m.thrusterTransforms.Select(t => new PortRef { index = i++, module = m });
                            })).ToArray();
                            stateLocal.portCount = portRefs.Count();
                            staticEngines = ModuleKCAvionics.getAllModules<ThrottleController>(_vessel).ToArray();
                            stateLocal.staticCount = staticEngines.Count();
                            gimballedEngines = ModuleKCAvionics.getAllModules<GimballedEngineController>(_vessel).ToArray();
                            stateLocal.gimbalCount = gimballedEngines.Count();

                            if (stateLocal.constants.port == null || stateLocal.constants.port.Count() != stateLocal.portCount)
                            {
                                stateLocal.constants.port = new SharedState.Constants.Port[stateLocal.portCount];
                                stateLocal.variables.port = new SharedState.Variables.Port[stateLocal.portCount];
                            }
                            if (stateLocal.constants.static_ == null || stateLocal.constants.static_.Count() != stateLocal.staticCount)
                            {
                                stateLocal.constants.static_ = new SharedState.Constants.StaticEngine[stateLocal.staticCount];
                                stateLocal.variables.static_ = new SharedState.Variables.StaticEngine[stateLocal.staticCount];
                            }
                            if (stateLocal.constants.gimbal == null || stateLocal.constants.gimbal.Count() != stateLocal.gimbalCount)
                            {
                                stateLocal.constants.gimbal = new SharedState.Constants.GimballedEngine[stateLocal.gimbalCount];
                                stateLocal.variables.gimbal = new SharedState.Variables.GimballedEngine[stateLocal.gimbalCount];
                            }

                            //update constants
                            for (int i = 0; i < stateLocal.portCount; ++i)
                            {
                                stateLocal.constants.port[i].maxThrust = portRefs[i].module.thrusterPower;
                            }
                            for (int i = 0; i < stateLocal.staticCount; ++i)
                            {
                                ModuleEngines engine = staticEngines[i].getEngine();
                                stateLocal.constants.static_[i].maxThrust = engine.maxThrust;
                                stateLocal.constants.static_[i].minThrust = engine.minThrust;
                                int thrustCount = engine.thrustTransforms.Count;
                                stateLocal.constants.static_[i].thrustCount = thrustCount;
                                stateLocal.variables.static_[i].direction = new Vector3[thrustCount];
                                stateLocal.variables.static_[i].position = new Vector3[thrustCount];
                            }
                            for (int i = 0; i < stateLocal.gimbalCount; ++i)
                            {
                                stateLocal.constants.gimbal[i].maxThrust = gimballedEngines[i].engine.maxThrust;
                                stateLocal.constants.gimbal[i].minThrust = gimballedEngines[i].engine.minThrust;
                                stateLocal.constants.gimbal[i].gimbalRange = gimballedEngines[i].gimbal.gimbalRange;
                                int thrustCount = gimballedEngines[i].thrustOriginTransforms.Count();
                                stateLocal.constants.gimbal[i].thrustCount = thrustCount;
                                stateLocal.variables.gimbal[i].orientation = new Quaternion[thrustCount];
                                stateLocal.variables.gimbal[i].position = new Vector3[thrustCount];
                            }

                            //allocate output
                            stateLocal.portThrusts = new double[stateLocal.portCount];
                            stateLocal.staticThrusts = new double[stateLocal.staticCount];
                            stateLocal.gimbalThrusts = new double[stateLocal.gimbalCount];
                            stateLocal.gimbalSettings = new Vector2[stateLocal.gimbalCount];

                            layoutChanged = false;
                        }

                        //update variables
                        stateLocal.alwaysGuidance = alwaysGuidance;
                        {
                            Vector3 total = Vector3.zero;
                            for (int i = 0; i < wheels.Count(); ++i)
                                if (wheels[i].operational)
                                    total += wheels[i].rotPower;
                            if (stateLocal.wheelStrength != total)
                            {
                                stateLocal.layoutChanged = true;
                            }
                            stateLocal.wheelStrength = total;
                        }
                        for (int i = 0; i < stateLocal.portCount; ++i)
                        {
                            MyRCSModule m = portRefs[i].module;
                            Transform t = m.thrusterTransforms[portRefs[i].index];
                            stateLocal.variables.port[i].direction = vessel.transform.InverseTransformDirection(-t.up);
                            stateLocal.variables.port[i].position = vessel.transform.InverseTransformPoint(t.position);
                            stateLocal.variables.port[i].maxFlow = m.realISP > 0 ? m.thrusterPower / (m.realISP * m.G) : 0.0f;
                            stateLocal.variables.port[i].operable = _vessel.ActionGroups[KSPActionGroup.RCS] && m.operational && m.isEnabled;
                        }
                        for (int i = 0; i < stateLocal.staticCount; ++i)
                        {
                            BaseThrottleController m = staticEngines[i];
                            int thrustCount = stateLocal.constants.static_[i].thrustCount;
                            for (int j = 0; j < thrustCount; ++j)
                            {
                                stateLocal.variables.static_[i].direction[j] = vessel.transform.InverseTransformDirection(m.getDirection(j));
                                stateLocal.variables.static_[i].position[j] = vessel.transform.InverseTransformPoint(m.getPosition(j));
                            }
                            ModuleEngines engine = m.getEngine();
                            stateLocal.variables.static_[i].maxFlow = engine.realIsp > 0 ? engine.maxThrust / (engine.realIsp * engine.G) : 0.0f;
                            stateLocal.variables.static_[i].operable = m.operational;
                        }
                        for (int i = 0; i < stateLocal.gimbalCount; ++i)
                        {
                            GimballedEngineController m = gimballedEngines[i];
                            int thrustCount = stateLocal.constants.gimbal[i].thrustCount;
                            for (int j = 0; j < thrustCount; ++j)
                            {
                                Transform t = m.thrustOriginTransforms[j];
                                stateLocal.variables.gimbal[i].orientation[j] = Quaternion.Inverse(vessel.transform.rotation) * t.rotation;
                                stateLocal.variables.gimbal[i].position[j] = vessel.transform.InverseTransformPoint(t.position);
                            }
                            stateLocal.variables.gimbal[i].maxFlow = m.engine.realIsp > 0 ? m.engine.maxThrust / (m.engine.realIsp * m.engine.G) : 0.0f;
                            stateLocal.variables.gimbal[i].operable = m.operational;
                            stateLocal.variables.gimbal[i].locked = m.gimbal.gimbalLock;
                        }

                        stateLocal.COM = vessel.transform.InverseTransformPoint(vessel.CoM + vessel.rb_velocity * Time.deltaTime);
                        #endregion

                        if (resultsAvailable)
                        {
                            #region update output state
                            status = stateLocal.status;
                            
                            if (stateLocal.status == SharedState.Status.Solved)
                            {
                                #region Update reaction wheel controllers
                                Vector3 torqueLeft = stateLocal.wheelTorque;
                                for (int i = 0; i < wheels.Count(); ++i)
                                {
                                    ReactionWheelController wheel = wheels[i];
                                    if (torqueLeft.x == 0.0f)
                                        wheel.overrideInput.x = 0.0f;
                                    else if (Mathf.Abs(torqueLeft.x) < wheel.rotPower.x)
                                    {
                                        wheel.overrideInput.x = torqueLeft.x / wheel.rotPower.x;
                                        torqueLeft.x = 0.0f;
                                    }
                                    else
                                    {
                                        wheel.overrideInput.x = Mathf.Sign(torqueLeft.x);
                                        torqueLeft.x -= wheel.overrideInput.x * wheel.rotPower.x;
                                    }

                                    if (torqueLeft.y == 0.0f)
                                        wheel.overrideInput.y = 0.0f;
                                    else if (Mathf.Abs(torqueLeft.y) < wheel.rotPower.y)
                                    {
                                        wheel.overrideInput.y = torqueLeft.y / wheel.rotPower.y;
                                        torqueLeft.y = 0.0f;
                                    }
                                    else
                                    {
                                        wheel.overrideInput.y = Mathf.Sign(torqueLeft.y);
                                        torqueLeft.y -= wheel.overrideInput.y * wheel.rotPower.y;
                                    }

                                    if (torqueLeft.z == 0.0f)
                                        wheel.overrideInput.z = 0.0f;
                                    else if (Mathf.Abs(torqueLeft.z) < wheel.rotPower.z)
                                    {
                                        wheel.overrideInput.z = torqueLeft.z / wheel.rotPower.z;
                                        torqueLeft.z = 0.0f;
                                    }
                                    else
                                    {
                                        wheel.overrideInput.z = Mathf.Sign(torqueLeft.z);
                                        torqueLeft.z -= wheel.overrideInput.z * wheel.rotPower.z;
                                    }
                                }
                                #endregion

                                for (int i = 0; i < stateLocal.portCount; ++i)
                                {
                                    float thrust = (float)stateLocal.portThrusts[i];
                                    MyRCSModule module = portRefs[i].module;
                                    int index = portRefs[i].index;
                                    module.thrustForces[index] = thrust;
                                }

                                for (int i = 0; i < stateLocal.staticCount; ++i)
                                {
                                    BaseThrottleController module = staticEngines[i];
                                    if (module.operational)
                                        module.overrideThrottle = (float)stateLocal.staticThrusts[i];
                                    else
                                        module.overrideThrottle = 0.0f;
                                }

                                for (int i = 0; i < stateLocal.gimbalCount; ++i)
                                {
                                    GimballedEngineController module = gimballedEngines[i];
                                    if (module.operational)
                                    {
                                        module.overrideThrottle = (float)stateLocal.gimbalThrusts[i];
                                        if (module.overrideThrottle > 1E-5)
                                        {
                                            module.overrideAngleX = (float)stateLocal.gimbalSettings[i].x / stateLocal.constants.gimbal[i].gimbalRange;
                                            module.overrideAngleY = (float)stateLocal.gimbalSettings[i].y / stateLocal.constants.gimbal[i].gimbalRange;
                                        }
                                        else
                                        {
                                            module.overrideAngleX = 0.0f;
                                            module.overrideAngleY = 0.0f;
                                        }
                                    }
                                    else
                                    {
                                        module.overrideThrottle = 0.0f;
                                        module.overrideAngleX = 0.0f;
                                        module.overrideAngleY = 0.0f;
                                    }
                                }
                            }
                            else
                            {
                                for (int i = 0; i < wheels.Count(); ++i)
                                {
                                    wheels[i].overrideInput = Vector3.zero;
                                }
                                for (int i = 0; i < stateLocal.portCount; ++i)
                                {
                                    MyRCSModule module = portRefs[i].module;
                                    int index = portRefs[i].index;
                                    module.thrustForces[index] = 0.0f;
                                }
                                for (int i = 0; i < stateLocal.staticCount; ++i)
                                {
                                    BaseThrottleController module = staticEngines[i];
                                    module.overrideThrottle = 0.0f;
                                }
                                for (int i = 0; i < stateLocal.gimbalCount; ++i)
                                {
                                    GimballedEngineController module = gimballedEngines[i];
                                    module.overrideThrottle = 0.0f;
                                    module.overrideAngleX = 0.0f;
                                    module.overrideAngleY = 0.0f;
                                }                                
                            }
                            #endregion
                        }
                        return true;
                    });
            }
            catch (Exception)
            {
                Debug.Log("CombinedMode update: Error at stage " + stage);
                throw;
            }
        }

        public override void onGUI()
        {
            string statusText = "Unknown";
            switch (status)
            {
                case SharedState.Status.InputIsZero:
                    statusText = "No input";
                    break;
                case SharedState.Status.BadEngineConfig:
                    statusText = "Invalid engine state";
                    break;
                case SharedState.Status.Solved:
                    statusText = "Nominal";
                    break;
                case SharedState.Status.ThrustSolverFailure:
                    statusText = "Internal Error 01";
                    break;
                case SharedState.Status.TorqueSolverFailure:
                    statusText = "Internal Error 02";
                    break;
                case SharedState.Status.MonoSolverFailure:
                    statusText = "Internal Error 03";
                    break;
                case SharedState.Status.FuelSolverFailure:
                    statusText = "Internal Error 04";
                    break;
            }
            GUILayout.Label("Status: " + statusText);
            alwaysGuidance = GUILayout.Toggle(alwaysGuidance, "Guidance only");
            GUILayout.Label("Throttle strength:");
            throttleStrength = GUILayout.HorizontalSlider(throttleStrength, 0.0f, 1.0f);

            GUILayout.Label("Guidance torque strength");
            GUILayout.Label("(Pitch, Roll, Yaw):");
            guidanceTorqueStrength.x = GUILayout.HorizontalSlider(guidanceTorqueStrength.x, 0.0f, 1.0f);
            guidanceTorqueStrength.y = GUILayout.HorizontalSlider(guidanceTorqueStrength.y, 0.0f, 1.0f);
            guidanceTorqueStrength.z = GUILayout.HorizontalSlider(guidanceTorqueStrength.z, 0.0f, 1.0f);

            GUILayout.Label("Docking torque strength");
            GUILayout.Label("(Pitch, Roll, Yaw):");
            dockingTorqueStrength.x = GUILayout.HorizontalSlider(dockingTorqueStrength.x, 0.0f, 1.0f);
            dockingTorqueStrength.y = GUILayout.HorizontalSlider(dockingTorqueStrength.y, 0.0f, 1.0f);
            dockingTorqueStrength.z = GUILayout.HorizontalSlider(dockingTorqueStrength.z, 0.0f, 1.0f);

            GUILayout.Label("RCS thrust strength");
            GUILayout.Label("(X, Y, Z):");
            rcsThrustStrength.x = GUILayout.HorizontalSlider(rcsThrustStrength.x, 0.0f, 1.0f);
            rcsThrustStrength.y = GUILayout.HorizontalSlider(rcsThrustStrength.y, 0.0f, 1.0f);
            rcsThrustStrength.z = GUILayout.HorizontalSlider(rcsThrustStrength.z, 0.0f, 1.0f);
        }

        public override void start()
        {
            worker.start();
        }

        public override bool running
        {
            get { return worker.running; }
        }

        public override void stop()
        {
            worker.stop();
            worker.reset();
            layoutChanged = true;
        }
        private Vector3 loadV3(ConfigNode node)
        {
            Vector3 result;
            result.x = float.Parse(node.GetValue("x"));
            result.y = float.Parse(node.GetValue("y"));
            result.z = float.Parse(node.GetValue("z"));
            return result;
        }
        private void saveV3(ConfigNode node, Vector3 v)
        {
            node.AddValue("x", v.x);
            node.AddValue("y", v.y);
            node.AddValue("z", v.z);
        }
        public override void Load(ConfigNode node)
        {
            alwaysGuidance = bool.Parse(node.GetValue("alwaysGuidance"));
            throttleStrength = float.Parse(node.GetValue("throttleStrength"));
            guidanceTorqueStrength = loadV3(node.GetNode("guidanceTorqueStrength"));
            dockingTorqueStrength = loadV3(node.GetNode("dockingTorqueStrength"));
            rcsThrustStrength = loadV3(node.GetNode("rcsThrustStrength"));
        }

        public override void Save(ConfigNode node)
        {
            node.AddValue("alwaysGuidance", alwaysGuidance);
            node.AddValue("throttleStrength", throttleStrength);
            saveV3(node.AddNode("guidanceTorqueStrength"), guidanceTorqueStrength);
            saveV3(node.AddNode("dockingTorqueStrength"), dockingTorqueStrength);
            saveV3(node.AddNode("rcsThrustStrength"), rcsThrustStrength);
        }

        public override IEnumerable<PartEffectorModule> getDesiredEffectors()
        {
            return _vessel.parts.SelectMany(p => p.Modules.Cast<PartModule>().Where(m => m is ThrottleController || m is GimballedEngineController || m is ReactionWheelController || m is MyRCSModule).Cast<PartEffectorModule>());
        }
    }
}

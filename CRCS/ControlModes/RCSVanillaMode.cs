﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace KerbCom.RCSControlModes
{
    /// <summary>
    /// Controls RCS ports in a manner identical to the stock RCS behaviour
    /// </summary>
    [ControlMode.Details(guiName = "Stock RCS control", guiHelp = 
@"Stock RCS control:

This mode behaves in an identical manner to stock RCS behaviour. It also provides strength settings for each axis for translation and rotation, for user preference."    
    )]
    class RCSVanillaMode : ControlMode
    {
        bool active = false;
        private Vessel _vessel;
        float strength = 1.0f;
        bool angular = true;
        public override Vessel vessel
        {
            get
            {
                return _vessel;
            }
            set
            {
                _vessel = value;
            }
        }

        public override void Load(ConfigNode node)
        {
            strength = float.Parse(node.GetValue("strength"));
            angular = bool.Parse(node.GetValue("angular"));
        }

        public override void Save(ConfigNode node)
        {
            node.AddValue("strength", strength.ToString());
            node.AddValue("angular", angular.ToString());
        }

        class PortRef
        {
            public MyRCSModule module;
            public int index;
        }

        PortRef[] ports = new PortRef[0];

        public override void onStructuralChange()
        {
            ports = _vessel.parts.SelectMany(p => p.Modules.Cast<PartModule>().Where(m => m is MyRCSModule).SelectMany(_m =>
            {
                MyRCSModule m = (MyRCSModule)_m;
                int i = 0;
                return m.thrusterTransforms.Select(t => new PortRef { index = i++, module = m });
            })).ToArray();
        }

        public override void onFrame()
        {
            if (active)
            {
                Vector3 targetAngular = vessel.ReferenceTransform.rotation * new Vector3(vessel.ctrlState.pitch, vessel.ctrlState.roll, vessel.ctrlState.yaw);
                Vector3 targetLinear = vessel.ReferenceTransform.rotation * new Vector3(vessel.ctrlState.X, vessel.ctrlState.Z, vessel.ctrlState.Y);
                foreach (PortRef port in ports)
                {
                    int i = port.index;
                    Transform t = port.module.thrusterTransforms[i];
                    Part part = port.module.part;
                    Vector3 targetTorque = Vector3.Cross(targetAngular, part.transform.position - (vessel.CoM + vessel.rb_velocity * Time.deltaTime));
                    port.module.thrustForces[i] = (
                        (angular ? Mathf.Max(Vector3.Dot(t.up, targetTorque), 0f) : 0f)
                        + Mathf.Max(Vector3.Dot(t.up, targetLinear), 0f)
                        ) * strength;
                }
            }
        }

        public override void onGUI()
        {
            GUILayout.Label(active ? "Active" : "Inactive");
            GUILayout.Label("Ports controlled: " + ports.Count());
            GUILayout.Label("Strength:");
            strength = GUILayout.HorizontalSlider(strength, 0.0f, 1.0f);
            GUILayout.Label(String.Format("{0}%",(int)(strength*100)));
            angular = GUILayout.Toggle(angular, "Angular thrust");
        }
        
        public override void start()
        {
            active = true;
        }

        public override bool running
        {
            get { return active; }
        }

        public override void stop()
        {
            active = false;
        }

        public override IEnumerable<PartEffectorModule> getDesiredEffectors()
        {
            return ModuleKCAvionics.getAllModules<MyRCSModule>(_vessel).Cast<PartEffectorModule>();
        }
    }
}

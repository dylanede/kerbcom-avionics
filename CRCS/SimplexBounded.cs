﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KerbCom
{
    /// <summary>
    /// Solves maximisation linear programming problems using the Simplex method.
    /// Has explicit support for upper bounds on variables.
    /// </summary>
    public class SimplexBounded
    {
        public class State
        {
            public int n, m;
            public double[,] equations;// (m+1) by (n+1)
            public double[] upper_bounds;// n
            public int[] basic_vars;// m
            public bool[] at_ub;// n
            //stats
            public int iteration_count = 0;
            public bool optimal = false;
            public bool unconstrained = false;
            public override string ToString()
            {
                string[] equations_ = Enumerable.Repeat("", equations.GetLength(0)).ToArray();
                equations.process((ref double v, int i, int j) => equations_[i] += (j==0 ? "" : ", ") + v.ToString());
                string r = "SimplexBounded.State:\n" +
                    "n: " + n + "\n" +
                    "m: " + m + "\n" +
                    "equations: " + string.Join(",\n", equations_) + "\n" +
                    "upper_bounds: " + string.Join(", ", upper_bounds.Select(v => v.ToString()).ToArray()) + "\n" +
                    "basic_vars: " + string.Join(", ", basic_vars.Select(v => v.ToString()).ToArray()) + "\n" +
                    "at_ub: " + string.Join(", ", at_ub.Select(v => v.ToString()).ToArray()) + "\n" +
                    "iteration_count: " + iteration_count + "\n" +
                    "optimal: " + optimal + "\n" +
                    "unconstrained: " + unconstrained;
                return r;
            }
        }
        public static State createState(int n, int m, double defaultUpperBound = double.PositiveInfinity)
        {
            State state = new State();
            state.n = n;
            state.m = m;
            state.equations = new double[m + 1, n + 1];
            state.basic_vars = new int[m];
            state.upper_bounds = Enumerable.Repeat(defaultUpperBound, n).ToArray(); // new double[n];
            state.at_ub = new bool[n];
            return state;
        }
        public static State createState(int n, int m, double[,] equations, int[] basic_vars, double[] upper_bounds, bool[] at_ub)
        {
            State state = new State();
            state.n = n;
            state.m = m;
            if (equations != null)
                state.equations = equations;
            else
                state.equations = new double[m + 1, n + 1];
            if (basic_vars != null)
                state.basic_vars = basic_vars;
            else
                state.basic_vars = new int[m];
            if (upper_bounds != null)
                state.upper_bounds = upper_bounds;
            else
                state.upper_bounds = new double[n];
            if (at_ub != null)
                state.at_ub = at_ub;
            else
                state.at_ub = new bool[n];
            return state;
        }

        public static void solve(State state) // assumes tableau is proper and feasible
        {
            if (state.n == 0)
                return;
            if (state.m == 0)
            {
                state.unconstrained = true;
                return;
            }

            int np1 = state.n + 1;
            int mp1 = state.m + 1;
            while (!state.optimal && !state.unconstrained)
            {
                // check optimality and find entering basic variable
                bool increase = !state.at_ub[0];
                double dcoeff = state.at_ub[0] ? state.equations[0, 0] : -state.equations[0, 0];
                int entering_i = 0;
                for (int i = 1; i < state.n; ++i)
                {
                    bool at_ub = state.at_ub[i];
                    double c = at_ub ? state.equations[0, i] : -state.equations[0, i];
                    if (c > dcoeff)
                    {
                        dcoeff = c;
                        entering_i = i;
                        increase = !at_ub;
                    }
                }
                if (dcoeff <= 0.0)
                {
                    state.optimal = true;
                    break;
                }

                // find leaving basic variable using MRT
                bool leaving_at_ub = false;
                double min_mrt = state.upper_bounds[entering_i];
                int leaving_j = -1; // -1 indicates the entering variable is also the leaving variable - i.e. it leaves immediately at the opposite bound.
                for (int j = 0; j < state.m; ++j)
                {
                    int _j = j + 1;
                    double rate = state.equations[_j, entering_i];
                    int basic_i = state.basic_vars[j];
                    //calculate value of variable
                    double value = state.equations[_j, state.n];//RHS
                    for (int i = 0; i < state.n; ++i)
                        if (i != basic_i && state.at_ub[i])
                            value -= state.equations[_j, i] * state.upper_bounds[i];
                    //double value = state.value[basic_i];
                    double mrt;
                    bool to_ub;
                    if (increase ^ (rate < 0.0))
                    {
                        mrt = value / rate;
                        to_ub = false;
                    }
                    else
                    {
                        double ub = state.upper_bounds[basic_i];
                        mrt = (ub - value) / rate;
                        to_ub = true;
                    }
                    mrt = Math.Abs(mrt);
                    if (mrt < min_mrt)
                    {
                        min_mrt = mrt;
                        leaving_j = j;
                        leaving_at_ub = to_ub;
                    }
                }
                if (min_mrt == double.PositiveInfinity) // unconstrained solution
                {
                    state.unconstrained = true;
                    break;
                }
                if (leaving_j < 0) //flip entering variable to its other bound, as it is also the leaving variable.
                {
                    state.at_ub[entering_i] = !state.at_ub[entering_i];
                }
                else
                {
                    // update tableau

                    int leaving_i = state.basic_vars[leaving_j];
                    state.at_ub[leaving_i] = leaving_at_ub;
                    state.at_ub[entering_i] = false;
                    state.basic_vars[leaving_j] = entering_i; // update basic variable list
                    int ljp1 = leaving_j + 1;

                    //ensure pivot element is 1 by dividing its equation
                    double pivot_value = state.equations[ljp1, entering_i];
                    for (int i = 0; i < np1; ++i)
                    {
                        state.equations[ljp1, i] /= pivot_value;
                    }

                    // remove entering variable from all equations except its own using Gaussian elimination
                    // this puts the tableau back in proper form

                    for (int _j = 0; _j < mp1; ++_j)
                    {
                        if (_j != ljp1)
                        {
                            double factor = state.equations[_j, entering_i];
                            for (int i = 0; i < np1; ++i)
                            {
                                state.equations[_j, i] -= factor * state.equations[ljp1, i];
                            }
                        }
                    }
                }
                ++state.iteration_count;
            }
        }

        public static bool canonicise(SimplexBounded.State state, int ej, int basic_i)
        {
            int _ej = ej + 1;
            double basic_value = state.equations[_ej, basic_i];
            if (Math.Abs(basic_value) <= double.Epsilon * 10)
                return false;
            int np1 = state.n + 1;
            int mp1 = state.m + 1;
            //normalise
            for (int i = 0; i < np1; ++i)
            {
                state.equations[_ej, i] /= basic_value;
            }

            // remove basic variable from all equations except its own using Gaussian elimination

            for (int _j = 0; _j < mp1; ++_j)
            {
                if (_j != _ej)
                {
                    double factor = state.equations[_j, basic_i];
                    if (factor != 0.0)
                        for (int i = 0; i < np1; ++i)
                        {
                            state.equations[_j, i] -= factor * state.equations[_ej, i];
                        }
                }
            }
            state.basic_vars[ej] = basic_i;
            return true;
        }

        public static double getValue(State state)
        {
            double value = state.equations[0, state.n];//RHS
            for (int i = 0; i < state.n; ++i)
                if (state.at_ub[i])
                    value -= state.equations[0, i] * state.upper_bounds[i];
            return value;
        }
        public static void getVariableValues(State state, ref double[] valuesOut)
        {
            if (valuesOut == null || valuesOut.Length != state.n)
                valuesOut = new double[state.n];
            for (int i = 0; i < state.n; ++i)
            {
                valuesOut[i] = state.at_ub[i] ? state.upper_bounds[i] : 0;
            }
            for (int j = 0; j < state.m; ++j)
            {
                int _j = j + 1;
                int basic_i = state.basic_vars[j];
                double value = state.equations[_j, state.n];//RHS
                for (int i = 0; i < state.n; ++i)
                    if (i != basic_i && state.at_ub[i])
                        value -= state.equations[_j, i] * state.upper_bounds[i];
                valuesOut[basic_i] = value;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace KerbCom
{
    class ReactionWheelController : PartEffectorModule
    {
        public void ensureAwake()
        {
            OnAwake();
        }

        public void ensureStarted(StartState state)
        {
            OnAwake();
            OnStart(state);
        }

        [KSPField(guiName = "Operational", guiActive = true)]
        public bool operational = false;

        public ModuleReactionWheel wheel;

        public Vector3 rotPower;

        public Vector3 overrideInput;

        private Vector3 ctrlInput;

        private double[] rates;

        private bool awoken = false, started = false;

        public override void OnAwake()
        {
            if (awoken)
                return;
            awoken = true;
        }
        public override void OnStart(StartState state)
        {
            if (started) return;
            started = true;
            wheel = part.Modules.Cast<PartModule>().Where(m => m is ModuleReactionWheel).Cast<ModuleReactionWheel>().First();
            rotPower = new Vector3(wheel.PitchTorque, wheel.RollTorque, wheel.YawTorque);
            //wheel.PitchTorque = 0.0f;
            //wheel.RollTorque = 0.0f;
            //wheel.YawTorque = 0.0f;
            rates = new double[wheel.inputResources.Count];
            for (int i = 0; i < rates.Length; ++i)
            {
                rates[i] = wheel.inputResources[i].rate;
                wheel.inputResources[i].rate = 0.0001f;
            }
            ctrlInput = Vector3.zero;
            overrideInput = Vector3.zero;
        }
        public override void OnUpdate()
        {
            base.OnUpdate();
            ctrlInput.x = vessel.ctrlState.pitch;
            ctrlInput.y = vessel.ctrlState.roll;
            ctrlInput.z = vessel.ctrlState.yaw;
        }
        public override void FixedUpdate()
        {
            base.FixedUpdate();
            Vector3 input = controlState == ControlState.Local ? ctrlInput : overrideInput;
            //Debug.Log("Wheel input: " + input);
            double req = Mathf.Abs(input.x) + Mathf.Abs(input.y) + Mathf.Abs(input.z);
            //double creq = Mathf.Abs(ctrlInput.x) + Mathf.Abs(ctrlInput.y) + Mathf.Abs(ctrlInput.z); // to counter the stock usage.
            req *= TimeWarp.deltaTime;
            bool hasPower = true;
            operational = wheel.State == ModuleReactionWheel.WheelState.Active &&wheel.inputResources.Where(res => !res.available).Count() == 0;
            if (!operational)
                return;
            if(!CheatOptions.InfiniteFuel)
                for (int i = 0; i < wheel.inputResources.Count; ++i)
                {
                    ModuleResource res = wheel.inputResources[i];
                    double rate = rates[i];//res.rate;
                    double resreq = req * rate;
                    //double cresreq = creq * rate;
                    //part.RequestResource(res.id, -cresreq);
                    double supply = part.RequestResource(res.id, resreq);
                    if (supply < resreq * 0.9)
                    {
                        hasPower = false;
                        break;
                    }
                }
            if (hasPower)
            {
                //Debug.Log("Wheel has power");
                Vector3 t = vessel.ReferenceTransform.rotation * (Vector3.Scale(input, -rotPower));
                rigidbody.AddTorque(t, ForceMode.Force);
                Vector3 comp = vessel.ReferenceTransform.rotation * (Vector3.Scale(ctrlInput, rotPower));
                rigidbody.AddTorque(comp, ForceMode.Force);
            }
            else
            {
                //Debug.Log("Wheel has no power");
            }
        }
    }
}

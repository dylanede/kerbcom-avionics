﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KerbCom
{
    static class ArrayHelpers
    {
        public static void populate<T>(this T[] a, T value)
        {
            int size = a.GetLength(0);
            for (int i = 0; i < size; ++i)
                a[i] = value;
        }
        public static void populate(this CLP.IVector a, double value)
        {
            int size = a.size;
            for (int i = 0; i < size; ++i)
                a[i] = value;
        }
        public static void populate<T>(this T[] a, Func<int, T> gen)
        {
            int size = a.Length;
            for (int i = 0; i < size; ++i)
                a[i] = gen(i);
        }
        public delegate void Processor<T>(ref T value, int i, int j);
        public static void process<T>(this T[,] a, Processor<T> f) {
            int sizex = a.GetLength(0);
            int sizey = a.GetLength(1);
            for (int i = 0; i < sizex; ++i)
                for (int j = 0; j < sizey; ++j)
                    f(ref a[i, j], i, j);
        }
    }
}

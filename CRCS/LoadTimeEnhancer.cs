﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace KerbCom
{
    [KSPAddonFixed(KSPAddon.Startup.Instantly, true, typeof(LoadTimeEnhancer))]
    class LoadTimeEnhancer : MonoBehaviour
    {
        bool loaded = false;
        public void OnGUI()
        {
            if (loaded)
                return;
            enhance();
            loaded = true;
        }

        private void enhance()
        {
            foreach (var urlconfig in GameDatabase.Instance.root.AllConfigs)
            {
                if (urlconfig.config.name == "PART")
                {
                    var config = urlconfig.config;
                    List<ConfigNode> engineConfigs = new List<ConfigNode>();
                    List<string> transformNames = new List<string>();
                    bool thrustTransform = false;
                    foreach (ConfigNode node in config.GetNodes("MODULE"))
                    {
                        string name = node.GetValue("name");
                        if (name == typeof(ModuleRCS).Name)
                        {
                            //Debug.Log("Enhancer: Part " + urlconfig.name + " has a ModuleRCS");
                            node.SetValue("name", typeof(MyRCSModule).Name);
                        }
                        else if (name == typeof(ModuleReactionWheel).Name)
                        {
                            //Debug.Log("Enhancer: Part " + urlconfig.name + " has a ModuleReactionWheel");
                            ConfigNode aug = config.AddNode("MODULE");
                            aug.SetValue("name", typeof(ReactionWheelController).Name);
                        }
                        else if (name == typeof(ModuleEngines).Name)
                        {
                            engineConfigs.Add(node);
                        }
                        else if (name == typeof(ModuleGimbal).Name)
                        {
                            if (node.HasValue("gimbalTransformName"))
                                transformNames.Add(node.GetValue("gimbalTransformName"));
                            else
                                thrustTransform = true;
                        }
                    }
                    foreach (var engine in engineConfigs)
                    {
                        bool gimballed;
                        string tname;
                        if (engine.HasValue("thrustVectorTransformName"))
                        {
                            tname = engine.GetValue("thrustVectorTransformName");
                            gimballed = transformNames.Contains(tname);
                        }
                        else
                        {
                            gimballed = thrustTransform;
                            tname = "thrustTransform";
                        }
                        ConfigNode aug = config.AddNode("MODULE");
                        aug.SetValue("thrustVectorTransformName", tname);
                        if (gimballed)
                        {
                            //Debug.Log("Enhancer: Part " + urlconfig.name + " has a gimballed engine");
                            aug.SetValue("name", typeof(GimballedEngineController).Name);
                        }
                        else
                        {
                            //Debug.Log("Enhancer: Part " + urlconfig.name + " has a static engine");
                            aug.SetValue("name", typeof(ThrottleController).Name);
                        }
                    }
                }
            }
        }
    }
}

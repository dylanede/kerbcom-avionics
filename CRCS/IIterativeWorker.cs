﻿using System;
namespace KerbCom
{
    interface IIterativeWorker
    {
        void reset();
        bool running { get; }
        void signal();
        void start();
        void stop();
        bool update(Func<bool, bool> updater, bool wait = false);
    }
}

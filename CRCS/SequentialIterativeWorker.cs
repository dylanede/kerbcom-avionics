﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KerbCom
{
    class SequentialIterativeWorker : IIterativeWorker
    {
        //private bool shouldStop = false;
        private bool resultsAvailable = false;
        private bool dataAvailable = false;
        private Action prework;
        private Action work;
        private Action postwork;
        bool _running = false;

        public SequentialIterativeWorker(Action prework, Action work, Action postwork)
        {
            this.prework = prework;
            this.work = work;
            this.postwork = postwork;
        }

        public void reset()
        {
            dataAvailable = false;
            resultsAvailable = false;
            //throw new NotImplementedException();
        }

        public bool running
        {
            get { return _running; }
        }

        public void signal()
        {
            //throw new NotImplementedException();
        }

        public void start()
        {
            _running = true;
        }

        public void stop()
        {
            _running = false;
        }

        public bool update(Func<bool, bool> updater, bool wait = false)
        {
            if (dataAvailable)
            {
                dataAvailable = false;
                prework();
                work();
                postwork();
                resultsAvailable = true;
            }
            dataAvailable = updater(resultsAvailable);
            bool result = resultsAvailable;
            resultsAvailable = false;
            return result;
        }
    }
}

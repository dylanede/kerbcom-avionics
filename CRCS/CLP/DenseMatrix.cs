﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KerbCom.CLP
{
    public class DenseMatrix : IMatrix
    {
        double[,] data;
        int m, n;
        public DenseMatrix(int rows, int cols)
        {
            m = rows;
            n = cols;
            data = new double[m, n];
        }
        public double this[int i, int j]
        {
            get
            {
                if (i < 0 || j < 0 || i >= m || j >= n)
                    throw new IndexOutOfRangeException();
                return data[i, j];
            }
            set
            {
                if (i < 0 || j < 0 || i >= m || j >= n)
                    throw new IndexOutOfRangeException();
                data[i, j] = value;
            }
        }

        public int rows
        {
            get
            {
                return m;
            }
            set
            {
                m = value;
            }
        }

        public int cols
        {
            get
            {
                return n;
            }
            set
            {
                n = value;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KerbCom.CLP
{
    class VectorView : IVector
    {
        private IVector r;
        private int[] p;
        private int m;

        public void setReference(IVector reference)
        {
            r = reference;
        }

        public void setView(int[] columns)
        {
            p = columns;
        }

        public VectorView(IVector reference, int[] view)
        {
            r = reference;
            p = view;
            m = view.Length;
        }

        public double this[int i]
        {
            get
            {
                return r[p[i]];
            }
            set
            {
                r[p[i]] = value;
            }
        }

        public int size
        {
            get
            {
                return m;
            }
            set
            {
                m = value;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KerbCom.CLP
{
    public interface IVector
    {
        double this[int i]
        {
            get;
            set;
        }
        int size
        {
            get;
            set;
        }
    }
}

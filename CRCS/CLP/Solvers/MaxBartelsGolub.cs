﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KerbCom.CLP.Solvers
{
    public class MaxBartelsGolub : KerbCom.CLP.Solvers.Solver
    {
        private Constraint[] constraints;

        private Dictionary<BoundedVariable, int> index_for_var;

        private Status _status;

        public override Status status
        {
            get
            {
                return _status;
            }
        }

        private ReadOnlyIndexer<BoundedVariable, double> real_value_accessor;

        public override ReadOnlyIndexer<BoundedVariable, double> values
        {
            get
            {
                return real_value_accessor;
            }
        }

        private bool optimal = false;
        private bool unconstrained = false;
        public int iteration_count = 0;


        private int constraint_count;
        private int actual_constraint_count;
        private int variable_count;
        private int actual_variable_count;
        private int artificial_variable_offset;

        private IVector constraint_constant_terms;
        
        private double objective_constant_term;
        
        //Working data:
        private IVector upper_bounds;
        private IVector objective;
        private IVector phase1;
        bool[] in_basis;
        private IMatrix A;
        private IVector b;
        IMatrix LU, mtemp;
        int[] p;
        IVector d, w, temp;//, c_tilde;
        private int B_count, V_count;
        int[] B_indices, V_indices;
        
        
        
        //Data views:
        MatrixMultiColumnView B, V;

        //output
        IVector solution;

        public MaxBartelsGolub(Problem problem, IEnumerable<BoundedVariable> variables = null)
        {
            this.problem = problem;
            if (variables != null)
                this.variables = new HashSet<BoundedVariable>(variables).ToArray();
            else
            {
                var set = new HashSet<BoundedVariable>();
                foreach (Constraint c in problem.constraints)
                    foreach (BoundedVariable v in c.f.terms.Keys)
                        set.Add(v);
                this.variables = set.ToArray();
            }
            index_for_var = new Dictionary<BoundedVariable, int>();
            for (int i = 0; i < this.variables.Length; ++i)
                index_for_var[this.variables[i]] = i;

            real_value_accessor = new ReadOnlyIndexer<BoundedVariable, double>(
            (BoundedVariable v) =>
            {
                return solution[index_for_var[v]] + v.lowerBound;
            },
            this.variables);
            constraints = problem.constraints.ToArray();

            int upper_bound_count = 0;
            foreach (BoundedVariable v in this.variables)
            {
                if (v.upperBound != double.PositiveInfinity)
                    ++upper_bound_count;
            }
            actual_constraint_count = problem.constraints.Count;
            constraint_count = actual_constraint_count + upper_bound_count;
            actual_variable_count = this.variables.Count();
            variable_count = actual_variable_count + constraint_count + upper_bound_count;
            artificial_variable_offset = actual_variable_count + upper_bound_count;
            

            A = new HashMatrix(constraint_count, variable_count);
            b = new DenseVector(constraint_count);
            objective = new DenseVector(variable_count);
            phase1 = new DenseVector(variable_count);
            upper_bounds = new DenseVector(variable_count);
            for (int i = actual_variable_count; i < variable_count; ++i)
                upper_bounds[i] = double.PositiveInfinity;
            constraint_constant_terms = new DenseVector(actual_constraint_count);

            B_count = constraint_count;
            V_count = variable_count - constraint_count;

            in_basis = new bool[variable_count];
            B_indices = new int[B_count];
            B = new MatrixMultiColumnView(A, B_indices);//new HashMatrix(B_count, B_count);
            V_indices = new int[V_count];
            V = new MatrixMultiColumnView(A, V_indices);//new HashMatrix(B_count, V_count);
            
            LU = new HashMatrix(B_count, B_count);
            p = new int[B_count];
            mtemp = new HashMatrix(B_count, V_count);
            d = new DenseVector(B_count);
            temp = new DenseVector(V_count);
            //c_B = new DenseVector(B_count);
            //c_tilde = new DenseVector(variable_count);
            w = new DenseVector(B_count);
            solution = new DenseVector(variable_count);
        }
        private bool canonicise(int ej, int basic_i)
        {
            double basic_value = A[ej, basic_i];
            if (Math.Abs(basic_value) <= double.Epsilon * 10)
                return false;
            //normalise
            for (int i = 0; i < variable_count; ++i)
            {
                A[ej, i] /= basic_value;
            }
            b[ej] /= basic_value;

            // remove basic variable from all equations except its own using Gaussian elimination

            for (int j = 0; j < constraint_count; ++j)
            {
                if (j != ej)
                {
                    double factor = A[j, basic_i];
                    for (int i = 0; i < variable_count; ++i)
                        A[j, i] -= factor * A[ej, i];
                    b[j] -= factor * b[ej];
                }
            }
            {
                double factor = objective[basic_i];
                for (int i = 0; i < variable_count; ++i)
                    objective[i] -= factor * A[ej, i];
            }
            {
                double factor = phase1[basic_i];
                for (int i = 0; i < variable_count; ++i)
                    phase1[i] -= factor * A[ej, i];
            }
            return true;
        }
        private void prepare()
        {
            objective_constant_term = 0.0;
            for (int i = 0, ub_i = 0; i < actual_variable_count; ++i)
            {
                BoundedVariable v = this.variables[i];
                double upper_bound = v.upperBound - v.lowerBound;
                upper_bounds[i] = upper_bound;
                if (upper_bound != double.PositiveInfinity)
                {
                    int j = actual_constraint_count + ub_i;
                    A[j, i] = 1.0;
                    A[j, actual_variable_count + ub_i] = 1.0;
                    b[j] = upper_bound;
                    ++ub_i;
                }
                for (int j = 0; j < actual_constraint_count; ++j)
                {
                    A[j, i] = 0.0;
                }
                double n_obj_v = -problem.objective[v];
                objective_constant_term += n_obj_v * v.lowerBound;
                objective[i] = n_obj_v;
            }
            for (int j = 0; j < actual_constraint_count; ++j)
            {
                Constraint c = constraints[j];
                constraint_constant_terms[j] = 0.0;
                foreach (KeyValuePair<BoundedVariable, double> kv in c.f.terms)
                {
                    constraint_constant_terms[j] += kv.Value * kv.Key.lowerBound;
                    int i = index_for_var[kv.Key];
                    A[j, i] = kv.Value;
                }
                b[j] = constraints[j].RHS - constraint_constant_terms[j];
            }
            //objective_RHS = 0.0;
            for (int i = 0; i < artificial_variable_offset; ++i)
                V_indices[i] = i;
            {
                //set up artificial objective and variables
                for (int j = 0; j < constraint_count; ++j)
                {
                    A[j, j + artificial_variable_offset] = b[j] >= 0 ? 1.0 : -1.0;
                    //for (int i = artificial_variable_offset; i < variable_count; ++i)
                    //    A[j, i] = i - artificial_variable_offset == j ? (b[j] >= 0 ? 1.0 : -1.0) : 0.0;
                    objective[artificial_variable_offset + j] = 0.0;
                    phase1[artificial_variable_offset + j] = 1.0;
                    B_indices[j] = artificial_variable_offset + j;
                }
                for (int i = 0; i < artificial_variable_offset; ++i)
                    phase1[i] = 0.0;
                //phase1_RHS = 0.0;
                /*
                //put in proper form
                for (int j = 0; j < constraint_count; ++j)
                {
                    //double factor = tableau[j, actual_variable_count + j];
                    //for (int i = 0; i < actual_variable_count; ++i)
                    //    phase1[i] -= factor * tableau[j, i];
                    //phase1_RHS -= factor * tableau_RHS[j];
                    if (!canonicise(j, artificial_variable_offset + j))
                        ;// Debug.Log("Artificial variable error");
                }
                */
            }
        }
        private void internal_solve(IVector c)
        {
            VectorView c_B = new VectorView(c, B_indices);
            VectorView solution_B = new VectorView(solution, B_indices);
            MatrixColumnView A_j = new MatrixColumnView(A, 0);
            //VectorView c_V = new VectorView(c, V_indices);
            //VectorView c_tilde_V = new VectorView(c_tilde, V_indices);
            while (true)
            {
                Matrix.copy(B, LU);
                Matrix.partial_pivot_LU_decompose(LU, p);
                Matrix.LUsolve(LU, p, b, d, temp);
                Matrix.LUdivide(LU, p, V, mtemp, temp);
                Matrix.mulrow(c_B, mtemp, temp);

                //find entering variable
                double cj = 0.0;
                int mn_j = B_indices[0];
                for (int i = 0; i < V_count; ++i)
                {
                    int v_i = V_indices[i];
                    double c_tilde_v_i = c[v_i] - temp[i];
                    if (c_tilde_v_i < cj)
                    {
                        cj = c_tilde_v_i;
                        mn_j = v_i;
                    }
                }
                if (cj >= 0) // optimal
                {
                    solution.populate(0);
                    for (int i = 0; i < B_count; ++i)
                        solution_B[i] = d[i];
                    optimal = true;
                    break;
                }

                //find leaving variable
                int mn_i = -1;
                double mn = double.PositiveInfinity;
                A_j.setColumn(mn_j);
                Matrix.LUsolve(LU, p, A_j, w, temp);
                for (int i = 0; i < constraint_count; ++i)
                {
                    if (w[i] > 0)
                    {
                        double mrt = d[i] / w[i];
                        if (mrt < mn)
                        {
                            mn_i = i;
                            mn = mrt;
                        }
                    }
                }
                if (mn_i == -1) // unconstrained
                {
                    unconstrained = true;
                    break;
                }

                //switch variables
                int mn_k = B_indices[mn_i];
                B_indices[mn_i] = mn_j;
                for (int i = 0; i < V_count; ++i)
                    if (V_indices[i] == mn_j)
                    {
                        V_indices[i] = mn_k;
                        break;
                    }
                ++iteration_count;
            }
        }
        private double getPhase1Value()
        {
            double value = 0.0;
            for (int i = 0; i < variable_count; ++i)
                value += phase1[i] * solution[i];
            return value;
        }

        bool real_objective_value_valid = false;
        private double real_objective_value;
        private void updateRealObjectiveValue()
        {
            double value = 0.0;
            for (int i = 0; i < variable_count; ++i)
                value += -objective[i] * solution[i];
            real_objective_value = value + objective_constant_term;
            real_objective_value_valid = false;
        }

        public override double objective_value
        {
            get
            {
                if (!real_objective_value_valid)
                    updateRealObjectiveValue();
                return real_objective_value;
            }
        }

        public override void solve()
        {
            prepare();
            variable_count = artificial_variable_offset + constraint_count;
            mtemp.cols = V.cols = V_count = variable_count - constraint_count;
            optimal = false;
            unconstrained = false;
            iteration_count = 0;

            internal_solve(phase1);

            if (!optimal)
            {
                if (unconstrained)
                    _status = Status.Unconstrained;
                else
                    _status = Status.Error;
                return;
            }
            variable_count = artificial_variable_offset;
            mtemp.cols = V.cols = V_count = variable_count - constraint_count;
            in_basis.populate(false);
            {
                // Cull the artificial variables from the basis.
                // This may require reallocation of null variables.
                // If this is not possible, phase 1 failed.
                int reallocate = 0;
                for (int i = 0; i < B_count; ++i)
                {
                    int b_i = B_indices[i];
                    if (b_i >= variable_count)
                    {
                        if (solution[b_i] != 0.0)
                        {
                            _status = Status.Infeasible;
                            return;
                        }
                        ++reallocate;
                    }
                    in_basis[B_indices[i]] = true;
                }
                for (int j = 0; j < variable_count; ++j)
                {
                    if (reallocate == 0)
                        break;
                    if (!in_basis[j])
                    {
                        --reallocate;
                        in_basis[j] = true;
                    }
                }
            }
            {
                int b_i = 0, v_i = 0;
                for (int i = 0; i < variable_count; ++i)
                {
                    if (in_basis[i])
                        B_indices[b_i++] = i;
                    else
                        V_indices[v_i++] = i;
                }
            }
            optimal = false;
            unconstrained = false;
            internal_solve(objective);
            if (!optimal)
            {
                if (unconstrained)
                    _status = Status.Unconstrained;
                else
                    _status = Status.Error;
            }
            else
            {
                _status = Status.Optimal;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
namespace KerbCom.CLP.Solvers
{
    public class MaxBoundedBartelsGolub : KerbCom.CLP.Solvers.Solver
    {
        private Constraint[] constraints;
        
        private Dictionary<BoundedVariable, int> index_for_var;

        private Status _status;

        public override Status status
        {
            get
            {
                return _status;
            }
        }

        private ReadOnlyIndexer<BoundedVariable, double> real_value_accessor;

        public override ReadOnlyIndexer<BoundedVariable, double> values
        {
            get
            {
                return real_value_accessor;
            }
        }

        private bool optimal = false;
        private bool unconstrained = false;
        public int iteration_count = 0;


        private int constraint_count;
        private int variable_count;
        private int actual_variable_count;

        private IVector constraint_constant_terms;

        private double objective_constant_term;

        //Working data:
        bool[] at_ub;
        private IVector upper_bounds;
        private IVector objective;
        private IVector phase1;
        bool[] in_basis;
        private IMatrix A;
        private IVector b;
        private IVector b_alt;
        IMatrix LU, mtemp;
        int[] p;
        IVector x_B, w, temp;
        private int B_count, V_count;
        int[] B_indices, V_indices;



        //Data views:
        MatrixMultiColumnView B, V;

        //output
        IVector solution;

        public string dump()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("A = \n" + MatrixExt.ToString(A));
            sb.AppendLine("b = \n" + MatrixExt.ToString(b));
            sb.AppendLine("B_indices = \n" + MatrixExt.ToString(B_indices));
            sb.AppendLine("at_ub = \n" + MatrixExt.ToString(at_ub));
            return sb.ToString();
        }

        public MaxBoundedBartelsGolub(Problem problem, IEnumerable<BoundedVariable> variables = null)
        {
            this.problem = problem;
            if (variables != null)
                this.variables = new HashSet<BoundedVariable>(variables).ToArray();
            else
            {
                var set = new HashSet<BoundedVariable>();
                foreach (Constraint c in problem.constraints)
                    foreach (BoundedVariable v in c.f.terms.Keys)
                        set.Add(v);
                this.variables = set.ToArray();
            }
            index_for_var = new Dictionary<BoundedVariable, int>();
            for (int i = 0; i < this.variables.Length; ++i)
                index_for_var[this.variables[i]] = i;

            real_value_accessor = new ReadOnlyIndexer<BoundedVariable, double>(
            (BoundedVariable v) =>
            {
                return solution[index_for_var[v]] + v.lowerBound;
            },
            this.variables);
            constraints = problem.constraints.ToArray();

            int upper_bound_count = 0;
            foreach (BoundedVariable v in this.variables)
            {
                if (v.upperBound != double.PositiveInfinity)
                    ++upper_bound_count;
            }
            constraint_count = problem.constraints.Count;
            actual_variable_count = this.variables.Count();
            variable_count = actual_variable_count + constraint_count;

            A = new DenseMatrix(constraint_count, variable_count);
            b = new DenseVector(constraint_count);
            objective = new DenseVector(variable_count);
            phase1 = new DenseVector(variable_count);
            at_ub = new bool[variable_count];
            upper_bounds = new DenseVector(variable_count);
            for (int i = actual_variable_count; i < variable_count; ++i)
                upper_bounds[i] = double.PositiveInfinity;
            constraint_constant_terms = new DenseVector(constraint_count);

            B_count = constraint_count;
            V_count = variable_count - constraint_count;

            in_basis = new bool[variable_count];
            B_indices = new int[B_count];
            B = new MatrixMultiColumnView(A, B_indices);
            V_indices = new int[V_count];
            V = new MatrixMultiColumnView(A, V_indices);

            LU = new DenseMatrix(B_count, B_count);
            p = new int[B_count];
            mtemp = new DenseMatrix(B_count, V_count);
            x_B = new DenseVector(B_count);
            temp = new DenseVector(V_count);
            w = new DenseVector(B_count);
            //b_alt and w share space
            b_alt = w;//new DenseVector(constraint_count);
            solution = new DenseVector(variable_count);
        }

        private void prepare()
        {
            at_ub.populate(false);
            objective_constant_term = 0.0;
            for (int i = 0; i < actual_variable_count; ++i)
            {
                BoundedVariable v = this.variables[i];
                upper_bounds[i] = v.upperBound - v.lowerBound;
                for (int j = 0; j < constraint_count; ++j)
                {
                    A[j, i] = 0.0;
                }
                double n_obj_v = -problem.objective[v];
                objective_constant_term += n_obj_v * v.lowerBound;
                objective[i] = n_obj_v;
                V_indices[i] = i;
                phase1[i] = 0.0;
            }
            for (int j = 0; j < constraint_count; ++j)
            {
                Constraint c = constraints[j];
                constraint_constant_terms[j] = 0.0;
                foreach (KeyValuePair<BoundedVariable, double> kv in c.f.terms)
                {
                    constraint_constant_terms[j] += kv.Value * kv.Key.lowerBound;
                    int i = index_for_var[kv.Key];
                    A[j, i] = kv.Value;
                }
                b[j] = constraints[j].RHS - constraint_constant_terms[j];

                //set up artificial objective and variables
                A[j, j + actual_variable_count] = b[j] >= 0 ? 1.0 : -1.0;
                objective[actual_variable_count + j] = 0.0;
                phase1[actual_variable_count + j] = 1.0;
                B_indices[j] = actual_variable_count + j;
            }
        }
        private void internal_solve(IVector c)
        {
            VectorView c_B = new VectorView(c, B_indices);
            VectorView solution_B = new VectorView(solution, B_indices);
            MatrixColumnView A_j = new MatrixColumnView(A, 0);
            //VectorView c_V = new VectorView(c, V_indices);
            //VectorView c_tilde_V = new VectorView(c_tilde, V_indices);
            //Console.WriteLine("Starting phase.");
            //Console.WriteLine("A = \n" + MatrixExt.ToString(A));
            while (true)
            {
                if (iteration_count > 500)
                    break;
                //Console.WriteLine("Iteration: " + iteration_count);
                Matrix.copy(B, LU);
                //Console.WriteLine("B_indices = \n" + MatrixExt.ToString(B_indices));
                //Console.WriteLine("B = \n" + MatrixExt.ToString(B));
                Matrix.partial_pivot_LU_decompose(LU, p); // PLU = PB
                //Console.WriteLine("LU = \n" + MatrixExt.ToString(LU));
                //Console.WriteLine("p = \n" + MatrixExt.ToString(p));
                for (int i = 0; i < B_count; ++i) // b_alt = (b - V * x_V)
                {
                    b_alt[i] = b[i];
                    for (int j = 0; j < V_count; ++j)
                    {
                        int v_i = V_indices[j];
                        if(at_ub[v_i])
                            b_alt[i] -= V[i, j] * upper_bounds[v_i];
                    }
                }
                //Console.WriteLine("b - V * x_V = \n" + MatrixExt.ToString(b_alt));
                Matrix.LUsolve(LU, p, b_alt, x_B, temp); // x_B = ((LU)^-1) * P * (b - V * x_V)
                //Console.WriteLine("x_B = \n" + MatrixExt.ToString(x_B));

                //find entering variable
                Matrix.LUdivide(LU, p, V, mtemp, temp); // mtemp = ((LU)^-1)PV
                Matrix.mulrow(c_B, mtemp, temp); // temp = c_B((LU)^-1)PV

                double cj = double.PositiveInfinity;
                int mn_j = -1;
                bool entering_from_ub = false;
                for (int i = 0; i < V_count; ++i)
                {
                    int v_i = V_indices[i];
                    bool ub = at_ub[v_i];
                    double norm_c_tilde_v_i = c[v_i] - temp[i]; // c_tilde_V = c_V - c_B((LU)^-1)PV
                    if (ub)
                        norm_c_tilde_v_i = -norm_c_tilde_v_i;
                    if (norm_c_tilde_v_i < cj)
                    {
                        entering_from_ub = ub;
                        cj = norm_c_tilde_v_i;
                        mn_j = v_i;
                    }
                }

                if (cj >= -1E-5)
                {
                    for(int i = 0; i < V_count; ++i) {
                        int v_i = V_indices[i];
                        solution[v_i] = at_ub[v_i] ? upper_bounds[v_i] : 0.0;
                    }
                    for (int i = 0; i < B_count; ++i)
                        solution_B[i] = x_B[i];
                    optimal = true;
                    break;
                }

                //find leaving variable
                A_j.setColumn(mn_j);

                Matrix.LUsolve(LU, p, A_j, w, temp);

                int mn_i = -1;
                double mn = upper_bounds[mn_j];
                bool leaving_at_ub = false;
                for (int i = 0; i < constraint_count; ++i)
                {
                    double mrt;
                    bool to_ub;
                    if (entering_from_ub ^ (w[i] > 1E-5))
                    {
                        mrt = x_B[i] / w[i];
                        to_ub = false;
                    }
                    else
                    {
                        mrt = (upper_bounds[B_indices[i]] - x_B[i]) / w[i];
                        to_ub = true;
                    }
                    mrt = Math.Abs(mrt);
                    if (mrt < mn)
                    {
                        mn_i = i;
                        mn = mrt;
                        leaving_at_ub = to_ub;
                    }
                }
                if (mn == double.PositiveInfinity) //unconstrained
                {
                    unconstrained = true;
                    break;
                }

                if (mn_i == -1) // entering variable leaves immediately at opposite bound.
                {
                    at_ub[mn_j] = !at_ub[mn_j];
                }
                else // switch variables
                {
                    int mn_k = B_indices[mn_i];
                    B_indices[mn_i] = mn_j;
                    for (int i = 0; i < V_count; ++i)
                        if (V_indices[i] == mn_j)
                        {
                            V_indices[i] = mn_k;
                            break;
                        }
                }
                ++iteration_count;
            }
        }
        private double getPhase1Value()
        {
            double value = 0.0;
            for (int i = 0; i < variable_count; ++i)
                value += phase1[i] * solution[i];
            return value;
        }

        bool real_objective_value_valid = false;
        private double real_objective_value;
        private void updateRealObjectiveValue()
        {
            double value = 0.0;
            for (int i = 0; i < variable_count; ++i)
                value += -objective[i] * solution[i];
            real_objective_value = value + objective_constant_term;
            real_objective_value_valid = false;
        }

        public override double objective_value
        {
            get
            {
                if (!real_objective_value_valid)
                    updateRealObjectiveValue();
                return real_objective_value;
            }
        }

        public override void solve()
        {
            prepare();
            B.cols = B_count = constraint_count;
            variable_count = actual_variable_count + constraint_count;
            mtemp.cols = V.cols = V_count = variable_count - B_count;
            optimal = false;
            unconstrained = false;
            iteration_count = 0;

            internal_solve(phase1);

            if (!optimal)
            {
                if (unconstrained)
                    _status = Status.Unconstrained;
                else
                    _status = Status.Error;
                return;
            }
            variable_count = actual_variable_count;
            mtemp.cols = V.cols = V_count = variable_count - B_count;
            in_basis.populate(false);
            {
                // Cull the artificial variables from the basis.
                // This may require reallocation of null variables.
                // If this is not possible, phase 1 failed.
                int reallocate = 0;
                for (int i = 0; i < B_count; ++i)
                {
                    int b_i = B_indices[i];
                    if (b_i >= variable_count)
                    {
                        if (solution[b_i] != 0.0)
                        {
                            _status = Status.Infeasible;
                            return;
                        }
                        ++reallocate;
                    }
                    in_basis[B_indices[i]] = true;
                }
                for (int j = 0; j < variable_count; ++j)
                {
                    if (reallocate == 0)
                        break;
                    if (!in_basis[j])
                    {
                        --reallocate;
                        in_basis[j] = true;
                    }
                }
            }
            {
                int b_i = 0, v_i = 0;
                for (int i = 0; i < variable_count; ++i)
                {
                    if (in_basis[i])
                        B_indices[b_i++] = i;
                    else
                        V_indices[v_i++] = i;
                }
            }
            optimal = false;
            unconstrained = false;
            internal_solve(objective);
            //Debug.Log("Iteration count: " + iteration_count);
            if (!optimal)
            {
                if (unconstrained)
                    _status = Status.Unconstrained;
                else
                    _status = Status.Error;
            }
            else
            {
                _status = Status.Optimal;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace KerbCom.CLP.Solvers
{
    public class MaxPrimalSimplex : Solver
    {
        
        private Constraint[] constraints;
        
        private Dictionary<BoundedVariable, int> index_for_var;

        private Status _status;

        public override Status status
        {
            get
            {
                return _status;
            }
        }

        private bool optimal = false;
        private bool unconstrained = false;
        public int iteration_count = 0;

        private int constraint_count;
        private int variable_count;
        private int actual_variable_count;
        private double[,] tableau;
        private double[] tableau_RHS;
        private double[] constraint_constant_terms;
        private double[] objective;
        private double objective_RHS;
        private double objective_constant_term;
        private double[] phase1;
        private double phase1_RHS;
        private double[] upper_bounds;
        private bool[] at_ub;
        private int[] basic_variables;

        public override string ToString()
        {
            string[] tableau_ = Enumerable.Repeat("", tableau.GetLength(0)).ToArray();
            tableau.process((ref double v, int i, int j) => tableau_[i] += (j == 0 ? "" : ", ") + v.ToString());
            string r = "SimplexBoundedTwoPhase.State:\n" +
                "variable_count: " + variable_count + "\n" +
                "actual_variable_count: " + actual_variable_count + "\n" +
                "constraint_count: " + constraint_count + "\n" +
                "objective:\n" + string.Join(", ", objective.Select(v => v.ToString()).ToArray()) + "\n" +
                "objective_RHS: " + objective_RHS + "\n" +
                "objective_value: " + objective_value + "\n" +
                "phase1_value: " + getPhase1Value() + "\n" +
                "tableau:\n" + string.Join(",\n", tableau_) + "\n" +
                "tableau_RHS: " + string.Join(", ", tableau_RHS.Select(v => v.ToString()).ToArray()) + "\n" +
                "upper_bounds:\n" + string.Join(", ", upper_bounds.Select(v => v.ToString()).ToArray()) + "\n" +
                "basic_variables:\n" + string.Join(", ", basic_variables.Select(v => v.ToString()).ToArray()) + "\n" +
                "at_ub:\n" + string.Join(", ", at_ub.Select(v => v.ToString()).ToArray()) + "\n" +
                "iteration_count: " + iteration_count + "\n" +
                "optimal: " + optimal + "\n" +
                "unconstrained: " + unconstrained;
            return r;
        }

        public int getVariableID(BoundedVariable id)
        {
            return index_for_var[id];
        }

        public double getValue(int id)
        {
            if(!real_values_valid)
                updateRealValues();
            return real_values[id];
        }

        bool real_values_valid = false;
        private double[] real_values;
        private ReadOnlyIndexer<BoundedVariable, double> real_value_accessor;

        public override ReadOnlyIndexer<BoundedVariable, double> values
        {
            get
            {
                return real_value_accessor;
            }
        }

        private void updateRealValues()
        {
            if (real_values == null || real_values.Length != actual_variable_count)
                real_values = new double[actual_variable_count];
            for (int i = 0; i < actual_variable_count; ++i)
            {
                real_values[i] = at_ub[i] ? variables[i].upperBound : variables[i].lowerBound;
            }
            for (int j = 0; j < constraint_count; ++j)
            {
                int basic_i = basic_variables[j];
                if (basic_i < variable_count)
                {
                    double value = tableau_RHS[j];
                    for (int i = 0; i < variable_count; ++i)
                        if (i != basic_i && at_ub[i])
                            value -= tableau[j, i] * upper_bounds[i];
                    if(basic_i < actual_variable_count)
                        real_values[basic_i] = value + variables[basic_i].lowerBound;
                }
            }
            real_values_valid = true;
        }

        bool real_objective_value_valid = false;
        private double real_objective_value;
        private void updateRealObjectiveValue()
        {
            double value = objective_RHS;
            for (int i = 0; i < variable_count; ++i)
                if (at_ub[i])
                    value -= objective[i] * upper_bounds[i];
            real_objective_value = value + objective_constant_term;
            real_objective_value_valid = false;
        }

        public override double objective_value
        {
            get
            {
                if (!real_objective_value_valid)
                    updateRealObjectiveValue();
                return real_objective_value;
            }
        }

        public MaxPrimalSimplex(Problem problem, IEnumerable<BoundedVariable> variables = null)
        {
            this.problem = problem;
            if (variables != null)
                this.variables = new HashSet<BoundedVariable>(variables).ToArray();
            else
            {
                var set = new HashSet<BoundedVariable>();
                foreach (Constraint c in problem.constraints)
                    foreach (BoundedVariable v in c.f.terms.Keys)
                        set.Add(v);
                this.variables = set.ToArray();
            }
            index_for_var = new Dictionary<BoundedVariable, int>();
            for (int i = 0; i < this.variables.Length; ++i)
                index_for_var[this.variables[i]] = i;

            real_value_accessor = new ReadOnlyIndexer<BoundedVariable, double>(
            (BoundedVariable v) =>
            {
                if (!real_values_valid)
                    updateRealValues();
                return real_values[index_for_var[v]];
            },
            this.variables);
            constraints = problem.constraints.ToArray();
            actual_variable_count = this.variables.Count();
            variable_count = actual_variable_count + constraints.Count();

            constraint_count = problem.constraints.Count;

            tableau = new double[constraint_count, variable_count];
            tableau_RHS = new double[constraint_count];
            objective = new double[variable_count];
            phase1 = new double[variable_count];
            at_ub = new bool[variable_count];
            basic_variables = new int[constraint_count];
            upper_bounds = new double[variable_count];
            for (int i = this.variables.Count(); i < variable_count; ++i)
                upper_bounds[i] = double.PositiveInfinity;
            constraint_constant_terms = new double[constraint_count];
        }
        private bool canonicise(int ej, int basic_i)
        {
            double basic_value = tableau[ej, basic_i];
            if (Math.Abs(basic_value) <= double.Epsilon * 10)
                return false;
            //normalise
            for (int i = 0; i < variable_count; ++i)
            {
                tableau[ej, i] /= basic_value;
            }
            tableau_RHS[ej] /= basic_value;

            // remove basic variable from all equations except its own using Gaussian elimination

            for (int j = 0; j < constraint_count; ++j)
            {
                if (j != ej)
                {
                    double factor = tableau[j, basic_i];
                    for (int i = 0; i < variable_count; ++i)
                        tableau[j, i] -= factor * tableau[ej, i];
                    tableau_RHS[j] -= factor * tableau_RHS[ej];
                }
            }
            {
                double factor = objective[basic_i];
                for (int i = 0; i < variable_count; ++i)
                    objective[i] -= factor * tableau[ej, i];
                objective_RHS -= factor * tableau_RHS[ej];
            }
            {
                double factor = phase1[basic_i];
                for (int i = 0; i < variable_count; ++i)
                    phase1[i] -= factor * tableau[ej, i];
                phase1_RHS -= factor * tableau_RHS[ej];
            }
            basic_variables[ej] = basic_i;
            return true;
        }
        private void prepare()
        {
            variable_count = actual_variable_count + constraints.Count();
            objective_constant_term = 0.0;
            for (int i = 0; i < actual_variable_count; ++i)
            {
                BoundedVariable v = this.variables[i];
                upper_bounds[i] = v.upperBound - v.lowerBound;
                for (int j = 0; j < constraint_count; ++j)
                {
                    tableau[j, i] = 0.0;
                }
                double n_obj_v = -problem.objective[v];
                objective_constant_term += n_obj_v * v.lowerBound;
                objective[i] = n_obj_v;
            }
            for (int j = 0; j < constraint_count; ++j)
            {
                Constraint c = constraints[j];
                constraint_constant_terms[j] = 0.0;
                foreach (KeyValuePair<BoundedVariable, double> kv in c.f.terms)
                {
                    constraint_constant_terms[j] += kv.Value * kv.Key.lowerBound;
                    int i = index_for_var[kv.Key];
                    tableau[j, i] = kv.Value;
                }
                tableau_RHS[j] = constraints[j].RHS - constraint_constant_terms[j];
            }
            objective_RHS = 0.0;

            at_ub.populate(false);
            
            {
                //set up artificial objective and variables
                for (int j = 0; j < constraint_count; ++j)
                {
                    for (int i = actual_variable_count; i < variable_count; ++i)
                        tableau[j, i] = i - actual_variable_count == j ? (tableau_RHS[j] >= 0 ? 1.0 : -1.0) : 0.0;
                    objective[actual_variable_count + j] = 0.0;
                    phase1[actual_variable_count + j] = 1.0;
                }
                for (int i = 0; i < actual_variable_count; ++i)
                    phase1[i] = 0.0;
                phase1_RHS = 0.0;
                //Debug.Log(this.ToString());
                //put in proper form
                for (int j = 0; j < constraint_count; ++j)
                {
                    //double factor = tableau[j, actual_variable_count + j];
                    //for (int i = 0; i < actual_variable_count; ++i)
                    //    phase1[i] -= factor * tableau[j, i];
                    //phase1_RHS -= factor * tableau_RHS[j];
                    if (!canonicise(j, actual_variable_count + j))
                        Debug.Log("Artificial variable error");
                }
            }
        }
        private double getPhase1Value()
        {
            double value = phase1_RHS;
            for (int i = 0; i < variable_count; ++i)
                if (at_ub[i])
                    value -= phase1[i] * upper_bounds[i];
            return value;
        }
        public override void solve()
        {
            prepare();
            optimal = false;
            unconstrained = false;
            iteration_count = 0;
            internal_solve(phase1, ref phase1_RHS, objective, ref objective_RHS);
            if (!optimal || Math.Abs(getPhase1Value()) > 1E-5)
            {
                _status = Status.Infeasible;
                return;
            }
            optimal = false;
            unconstrained = false;
            variable_count = actual_variable_count;
            internal_solve(objective, ref objective_RHS, phase1, ref phase1_RHS);
            //Debug.Log("Iteration count: " + iteration_count);
            if (!optimal)
            {
                if (unconstrained)
                    _status = Status.Unconstrained;
                else
                    _status = Status.Error;
            }
            else
            {
                _status = Status.Optimal;
            }
        }
        private void internal_solve(double[] objective, ref double objective_RHS, double[] other_objective, ref double other_objective_RHS)
        {
            real_values_valid = false;
            real_objective_value_valid = false;
            if (variable_count == 0)
                return;
            if (constraint_count == 0)
            {
                unconstrained = true;
                return;
            }

            while (!optimal && !unconstrained)
            {
                // check optimality and find entering basic variable
                bool increase = !at_ub[0];
                double dcoeff = at_ub[0] ? objective[0] : -objective[0];
                int entering_i = 0;
                for (int i = 1; i < variable_count; ++i)
                {
                    bool _at_ub = at_ub[i];
                    double c = _at_ub ? objective[i] : -objective[i];
                    if (c > dcoeff)
                    {
                        dcoeff = c;
                        entering_i = i;
                        increase = !_at_ub;
                    }
                    //Bland's rule
                    //if (dcoeff > 0.0)
                    //    break;
                }
                if (dcoeff <= 0.0)
                {
                    optimal = true;
                    break;
                }

                // find leaving basic variable using MRT
                bool leaving_at_ub = false;
                double min_mrt = upper_bounds[entering_i];
                int leaving_j = -1; // -1 indicates the entering variable is also the leaving variable - i.e. it leaves immediately at the opposite bound.
                for (int j = 0; j < constraint_count; ++j)
                {
                    double rate = tableau[j, entering_i];
                    int basic_i = basic_variables[j];
                    //calculate value of variable
                    double value = tableau_RHS[j];//RHS
                    for (int i = 0; i < variable_count; ++i)
                        if (i != basic_i && at_ub[i])
                            value -= tableau[j, i] * upper_bounds[i];
                    //double value = value[basic_i];
                    double mrt;
                    bool to_ub;
                    if (increase ^ (rate < 0.0))
                    {
                        mrt = value / rate;
                        to_ub = false;
                    }
                    else
                    {
                        double ub = upper_bounds[basic_i];
                        mrt = (ub - value) / rate;
                        to_ub = true;
                    }
                    mrt = Math.Abs(mrt);
                    if (mrt < min_mrt)
                    {
                        min_mrt = mrt;
                        leaving_j = j;
                        leaving_at_ub = to_ub;
                    }
                }
                if (min_mrt == double.PositiveInfinity) // unconstrained solution
                {
                    unconstrained = true;
                    break;
                }
                if (leaving_j < 0) //flip entering variable to its other bound, as it is also the leaving variable.
                {
                    at_ub[entering_i] = !at_ub[entering_i];
                }
                else
                {
                    // update tableau

                    int leaving_i = basic_variables[leaving_j];
                    at_ub[leaving_i] = leaving_at_ub;
                    at_ub[entering_i] = false;
                    basic_variables[leaving_j] = entering_i; // update basic variable list

                    //ensure pivot element is 1 by dividing its equation
                    double pivot_value = tableau[leaving_j, entering_i];
                    for (int i = 0; i < variable_count; ++i)
                    {
                        tableau[leaving_j, i] /= pivot_value;
                    }
                    tableau_RHS[leaving_j] /= pivot_value;

                    // remove entering variable from all equations except its own using Gaussian elimination
                    // this puts the tableau back in proper form

                    for (int j = 0; j < constraint_count; ++j)
                    {
                        if (j != leaving_j)
                        {
                            double factor = tableau[j, entering_i];
                            for (int i = 0; i < variable_count; ++i)
                                tableau[j, i] -= factor * tableau[leaving_j, i];
                            tableau_RHS[j] -= factor * tableau_RHS[leaving_j];
                        }
                    }
                    {
                        double factor = objective[entering_i];
                        for (int i = 0; i < variable_count; ++i)
                            objective[i] -= factor * tableau[leaving_j, i];
                        objective_RHS -= factor * tableau_RHS[leaving_j];
                    }
                    {
                        double factor = other_objective[entering_i];
                        for (int i = 0; i < variable_count; ++i)
                            other_objective[i] -= factor * tableau[leaving_j, i];
                        other_objective_RHS -= factor * tableau_RHS[leaving_j];
                    }
                }
                ++iteration_count;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KerbCom.CLP
{
    class DenseVector : IVector
    {
        double[] data;
        int n;
        public DenseVector(int size)
        {
            n = size;
            data = new double[n];
        }
        public double this[int i]
        {
            get
            {
                return data[i];
            }
            set
            {
                data[i] = value;
            }
        }

        public int size
        {
            get
            {
                return n;
            }
            set
            {
                n = value;
            }
        }
    }
}

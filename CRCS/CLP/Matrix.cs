﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace KerbCom.CLP
{
    /// <summary>
    /// Static functions for manipulating matrices
    /// </summary>
    public class Matrix
    {
        const double TINY = 1E-20;

        public static void split_LU(IMatrix LU, IMatrix L, IMatrix U)
        {
            int n = LU.rows;
            for (int i = 0; i < n; ++i)
                for (int j = 0; j < n; ++j)
                    if (i > j)
                        L[i, j] = LU[i, j];
                    else if (i == j)
                    {
                        L[i, j] = 1.0;
                        U[i, j] = LU[i, j];
                    } else
                        U[i, j] = LU[i, j];

        }
        public static void mul(IMatrix A, IMatrix B, IMatrix C)
        {
            int n = A.rows;
            int m = B.cols;
            int kn = A.cols;
            for (int i = 0; i < n; ++i)
                for (int j = 0; j < m; ++j)
                {
                    C[i, j] = 0.0;
                    for (int k = 0; k < kn; ++k)
                        C[i, j] += A[i, k] * B[k, j];
                }

        }
        public static void mulrowcol(IVector a, IVector b, double c)
        {
            int kn = a.size;
            c = 0.0;
            for (int k = 0; k < kn; ++k)
                c += a[k] * b[k];
        }
        public static void mulcolrow(IVector a, IVector b, IMatrix C)
        {
            int n = a.size;
            int m = b.size;
            for (int i = 0; i < n; ++i)
                for (int j = 0; j < m; ++j)
                    C[i, j] = a[i] * b[j];
        }
        public static void mulrow(IVector a, IMatrix B, IVector c)
        {
            int m = B.cols;
            mulrow(a, B, m, c);
        }
        public static void mulrow(IVector a, IMatrix B, int m, IVector c)
        {
            int kn = a.size;
            for (int j = 0; j < m; ++j)
            {
                c[j] = 0.0;
                for (int k = 0; k < kn; ++k)
                    c[j] += a[k] * B[k, j];
            }
        }
        public static void mulcol(IMatrix A, IVector b, IVector c)
        {
            int n = A.rows;
            int kn = A.cols;
            for (int i = 0; i < n; ++i)
            {
                c[i] = 0.0;
                for (int k = 0; k < kn; ++k)
                    c[i] += A[i, k] * b[k];
            }
        }
        public static void sub(IMatrix A, IMatrix B, IMatrix C)
        {
            int n = A.rows;
            int m = B.cols;
            for (int i = 0; i < n; ++i)
                for (int j = 0; j < m; ++j)
                {
                    C[i, j] = A[i, j] - B[i, j];
                }
        }
        public static string printable(IMatrix mat)
        {
            StringBuilder sb = new StringBuilder();
            int n = mat.rows;
            int m = mat.cols;
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < m; ++j)
                {
                    sb.Append(mat[i, j] + "\t");
                }
                sb.Append("\n");
            }
            return sb.ToString();
        }
        public static void simple_LU_decompose(IMatrix mat)
        {
            int n = mat.rows;
            int n_m1 = n - 1;
            for (int k = 0; k < n_m1; ++k)
            {
                double d_elem = mat[k, k];
                for (int x = k + 1; x < n; ++x)
                    mat[x, k] /= d_elem;

                for (int i = k + 1; i < n; ++i)
                    for (int j = k + 1; j < n; ++j)
                        mat[i, j] -= mat[i, k] * mat[k, j];
            }
        }

        public static void reorder(IMatrix mat, IMatrix result, int[] p, int[] q)
        {
            int n = mat.rows;
            int m = mat.cols;
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < m; ++j)
                {
                    result[p[i], q[j]] = mat[i, j];
                }
            }
        }

#if FULL_PIVOTING
        public static void full_pivot_LU_decompose(IMatrix a, int[] p, int[] q)
#else
        public static void partial_pivot_LU_decompose(IMatrix a, int[] p)
#endif
        {
            int i, j, k;
            int n = a.rows;
            //Debug.Log("LU Decompose: rows: " + a.rows + ", cols: " + a.cols + ", p.length: " + p.Length);
            for (i = 0; i < n; ++i)
                p[i] = i;
#if FULL_PIVOTING
            int pi, pj, tmp;
#else
            int pi, tmp;
#endif

            double max;
            double ftmp;
            for (k = 0; k < n; ++k)
            {

                
#if FULL_PIVOTING
                pi = -1;
                pj = -1;
                max = 0.0;
                //find pivot in submatrix a(k:n,k:n)
                for (i = k; i < n; i++)
                {
                    for (j = k; j < n; j++)
                    {
                        double mag = Math.Abs(a[i, j]);
                        if (mag > max)
                        {
                            max = mag;
                            pi = i;
                            pj = j;
                        }
                    }
                }
#else
                //find pivot in submatrix a(k:n,k)
                pi = k;
                max = Math.Abs(a[k,k]);
                for (i = k + 1; i < n; ++i)
                {
                    double mag = Math.Abs(a[i, k]);
                    if (mag > max)
                    {
                        max = mag;
                        pi = i;
                    }
                }
#endif
                //Swap Row
                tmp = p[k];
                p[k] = p[pi];
                p[pi] = tmp;
                for (j = 0; j < n; ++j)
                {
                    ftmp = a[k, j];
                    a[k, j] = a[pi, j];
                    a[pi, j] = ftmp;
                }
#if FULL_PIVOTING
                //Swap Col
                tmp = q[k];
                q[k] = q[pj];
                q[pj] = tmp;
                for (i = 0; i < n; i++)
                {
                    ftmp = a[i, k];
                    a[i, k] = a[i, pj];
                    a[i, pj] = ftmp;
                }
#endif
                //check pivot size and decompose
                if ((Math.Abs(a[k, k]) > TINY))
                {
                    for (i = k + 1; i < n; ++i)
                    {
                        //Column normalisation
                        ftmp = a[i, k] /= a[k, k];
                        for (j = k + 1; j < n; ++j)
                        {
                            //a(ik)*a(kj) subtracted from lower right submatrix elements
                            a[i, j] -= (ftmp * a[k, j]);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Solves Ux = b, given U is upper triangular
        /// </summary>
        /// <param name="U"></param>
        /// <param name="b"></param>
        /// <param name="x"></param>
        public static void backsolve(IMatrix U, IVector b, IVector x)
        {
            int n = U.rows;
            for (int i = n - 1; i >= 0; --i)
            {
                double value = b[i];
                for(int j = i + 1; j < n; ++j)
                    value -= x[j] * U[i, j];
                x[i] = value / U[i, i];
            }
        }
        /// <summary>
        /// Solves Lx = b, given L is lower triangular
        /// </summary>
        /// <param name="L"></param>
        /// <param name="b"></param>
        /// <param name="x"></param>
        public static void forwardsolve(IMatrix L, IVector b, IVector x)
        {
            int n = L.rows;
            for (int i = 0; i < n; ++i)
            {
                double value = b[i];
                for (int j = 0; j < i; ++j)
                    value -= x[j] * L[i, j];
                x[i] = value / L[i, i];
            }
        }

        /// <summary>
        /// Solves x in LUx = b, given L is lower triangular, U is upper triangular
        /// </summary>
        /// <param name="LU">Combined LU representation - L diagonal is 1.</param>
        /// <param name="b"></param>
        /// <param name="x"></param>
        /// <param name="temp">Space used by the algorithm</param>
        public static void LUsolve(IMatrix LU, IVector b, IVector x, IVector temp = null)
        {
            IVector y = temp;
            int n = LU.rows;
            if (y == null)
                y = new DenseVector(n);
            for (int i = 0; i < n; ++i)
            {
                double value = b[i];
                for (int j = 0; j < i; ++j)
                    value -= y[j] * LU[i, j];
                y[i] = value;
            }

            for (int i = n - 1; i >= 0; --i)
            {
                double value = y[i];
                for (int j = i + 1; j < n; ++j)
                    value -= x[j] * LU[i, j];
                x[i] = value / LU[i, i];
            }
        }

        public static void LUsolve(IMatrix LU, int[] p, IVector b, IVector x, IVector temp = null)
        {
            IVector y = temp;
            int n = LU.rows;
            if (y == null)
                y = new DenseVector(n);
            for (int i = 0; i < n; ++i)
            {
                double value = b[p[i]];
                for (int j = 0; j < i; ++j)
                    value -= y[j] * LU[i, j];
                y[i] = value;
            }

            for (int i = n - 1; i >= 0; --i)
            {
                double value = y[i];
                for (int j = i + 1; j < n; ++j)
                    value -= x[j] * LU[i, j];
                x[i] = value / LU[i, i];
            }
        }

        /// <summary>
        /// Solves X in LUX = B, given L and U are an LU decomposition
        /// </summary>
        /// <param name="LU">Combined LU representation - L diagonal is 1.</param>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <param name="temp">Space used by the algorithm</param>
        public static void LUdivide(IMatrix LU, IMatrix B, IMatrix X, IVector temp = null)
        {
            int n = LU.rows;
            int m = B.cols;
            IVector y = temp;
            if (y == null)
                y = new DenseVector(n);
            for (int j = 0; j < m; ++j)
            {
                for (int i = 0; i < n; ++i)
                {
                    double value = B[i,j];
                    for (int k = 0; k < i; ++k)
                        value -= y[k] * LU[i, k];
                    y[i] = value;
                }
                for (int i = n - 1; i >= 0; --i)
                {
                    double value = y[i];
                    for (int k = i + 1; k < n; ++k)
                        value -= X[k,j] * LU[i, k];
                    X[i,j] = value / LU[i, i];
                }
            }
        }
        public static void LUdivide(IMatrix LU, int[] p, IMatrix B, IMatrix X, IVector temp = null)
        {
            int n = LU.rows;
            int m = B.cols;
            IVector y = temp;
            if (y == null)
                y = new DenseVector(n);
            for (int j = 0; j < m; ++j)
            {
                for (int i = 0; i < n; ++i)
                {
                    double value = B[p[i], j];
                    for (int k = 0; k < i; ++k)
                        value -= y[k] * LU[i, k];
                    y[i] = value;
                }
                for (int i = n - 1; i >= 0; --i)
                {
                    double value = y[i];
                    for (int k = i + 1; k < n; ++k)
                        value -= X[k, j] * LU[i, k];
                    X[i, j] = value / LU[i, i];
                }
            }
        }


        /// <summary>
        /// Solves LUx = b, given L is lower triangular, U is upper triangular
        /// </summary>
        /// <param name="L"></param>
        /// <param name="U"></param>
        /// <param name="b"></param>
        /// <param name="x"></param>
        /// <param name="temp">Space used by the algorithm</param>
        public static void LUsolve(IMatrix L, IMatrix U, IVector b, IVector x, IVector temp = null)
        {
            IVector y = temp;
            if (y == null)
                y = new DenseVector(L.rows);
            forwardsolve(L, b, y);
            backsolve(U, y, x);
        }

        public static void copy(IMatrix from, IMatrix to)
        {
            int n = from.rows;
            int m = from.cols;

            for (int i = 0; i < n; ++i)
                for (int j = 0; j < m; ++j)
                    to[i, j] = from[i, j];
        }

        public static void select_columns(IMatrix from, IMatrix to, int[] columns)
        {
            int nc = columns.Length;
            select_columns(from, to, columns, nc);
        }
        public static void select_columns(IMatrix from, IMatrix to, int[] columns, int nc)
        {
            int n = to.rows;
            for (int j = 0; j < nc; ++j)
                for (int i = 0; i < n; ++i)
                    to[i, j] = from[i, columns[j]];
        }
    }
}

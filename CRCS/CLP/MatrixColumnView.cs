﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KerbCom.CLP
{
    class MatrixColumnView : IVector
    {
        private IMatrix r;
        private int j;
        private int n;

        public void setReference(IMatrix reference)
        {
            r = reference;
        }
        public void setColumn(int j)
        {
            this.j = j;
        }

        public MatrixColumnView(IMatrix reference, int j)
        {
            r = reference;
            n = reference.rows;
        }

        public double this[int i]
        {
            get
            {
                return r[i, j];
            }
            set
            {
                r[i, j] = value;
            }
        }

        public int size
        {
            get
            {
                return n;
            }
            set
            {
                n = value;
            }
        }
    }
}

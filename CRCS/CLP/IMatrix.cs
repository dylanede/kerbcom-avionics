﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KerbCom.CLP
{
    public interface IMatrix
    {
        double this[int i, int j]
        {
            get;
            set;
        }
        int rows
        {
            get;
            set;
        }
        int cols
        {
            get;
            set;
        }
    }
    public class MatrixExt
    {
        public static String ToString(IMatrix mat)
        {
            string result = "";

            for (int i = 0; i < mat.rows; ++i)
            {
                result += mat[i, 0];
                for (int j = 1; j < mat.cols; ++j)
                {
                    result += ", " + mat[i, j];
                }
                if(i != mat.rows - 1)
                    result += ",\n";
            }
            return result;
        }
        public static String ToString(IVector v)
        {
            string result = v[0].ToString();
            for (int j = 1; j < v.size; ++j)
            {
                result += ", " + v[j];
            }
            return result;
        }
        public static String ToString<T>(T[] v)
        {
            string result = v[0].ToString();
            for (int j = 1; j < v.Length; ++j)
            {
                result += ", " + v[j];
            }
            return result;
        }
    }
}

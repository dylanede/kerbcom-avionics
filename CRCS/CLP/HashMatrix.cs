﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KerbCom.CLP
{
    class HashMatrix : IMatrix
    {
        private struct Point {
            public int x, y;
            public Point(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
        }

        Dictionary<Point, double> data;

        int m, n;

        public HashMatrix(int rows, int cols)
        {
            data = new Dictionary<Point, double>();
            m = rows;
            n = cols;
        }

        public double this[int i, int j]
        {
            get
            {
                if (i < 0 || j < 0 || i >= m || j >= n)
                    throw new IndexOutOfRangeException();
                double value;
                data.TryGetValue(new Point(i, j), out value);
                return value;
            }
            set
            {
                if (i < 0 || j < 0 || i >= m || j >= n)
                    throw new IndexOutOfRangeException();
                if (value == 0.0)
                    data.Remove(new Point(i, j));
                else
                    data[new Point(i, j)] = value;
            }
        }

        public int rows
        {
            get
            {
                return m;
            }
            set
            {
                m = value;
            }
        }

        public int cols
        {
            get
            {
                return n;
            }
            set
            {
                n = value;
            }
        }
    }
}

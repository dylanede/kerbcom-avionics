﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KerbCom.CLP
{
    class MatrixMultiColumnView : IMatrix
    {
        private IMatrix r;
        private int[] p;
        private int m;

        public void setReference(IMatrix reference)
        {
            r = reference;
        }

        public void setColumns(int[] columns)
        {
            p = columns;
        }

        public MatrixMultiColumnView(IMatrix reference, int[] columns)
        {
            r = reference;
            p = columns;
            m = columns.Length;
        }

        public double this[int i, int j]
        {
            get
            {
                return r[i, p[j]];
            }
            set
            {
                r[i, p[j]] = value;
            }
        }

        public int rows
        {
            get
            {
                return r.rows;
            }
            set
            {
                r.rows = value;
            }
        }

        public int cols
        {
            get
            {
                return m;
            }
            set
            {
                m = value;
            }
        }
    }
}

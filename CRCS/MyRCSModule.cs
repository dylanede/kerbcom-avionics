﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace KerbCom
{
    /// <summary>
    /// This class is designed to behave like ModuleRCS, to the point of having a nearly identical interface, except that thrustForces can be used as input
    /// </summary>
    class MyRCSModule : PartEffectorModule
    {
        public List<Transform> thrusterTransforms;
        public List<FXGroup> thrusterFX;
        public List<float> thrustForces;

        [KSPField(isPersistant = false)]
        public string thrusterTransformName = "RCSthruster";
        [KSPField(isPersistant = false)]
        public float thrusterPower = 1f;
        [KSPField(isPersistant = false)]
        public string resourceName = "MonoPropellant";

        [KSPField(isPersistant = false)]
        public string fxPrefabName = "fx_gasJet_white";
        [KSPField(isPersistant = false)]
        public string fxPrefix = "rcsGroup";
        [KSPField(isPersistant = false)]
        public Vector3 fxOffset = Vector3.zero;
        [KSPField(isPersistant = false)]
        public Vector3 fxOffsetRot = Vector3.zero;

        [KSPField(isPersistant = false)]
        public bool isJustForShow;
        [KSPField(isPersistant = false)]
        public bool requiresFuel = true;
        [KSPField(isPersistant = true)]
        public new bool isEnabled = true;

        [KSPField(guiName = "RCS Isp", guiUnits = "s", guiFormat = "F1", guiActive = true)]
        public float realISP;
        [KSPField(guiName = "Operational", guiActive = true)]
        public bool operational = true;
        public float G = 9.82f;
        public float resourceMass;

        [KSPField(isPersistant = false)]
        public FloatCurve atmosphereCurve;

        private int resCode = 0;

        private bool inEditor = false;

        protected bool started = false, awoken = false;

        public void ensureAwake()
        {
            OnAwake();
        }

        public void ensureStarted(StartState state)
        {
            OnAwake();
            OnStart(state);
        }

        public void migrateFrom(ModuleRCS m)
        {
            thrusterTransforms = m.thrusterTransforms;
            thrusterFX = m.thrusterFX;
            thrustForces = m.thrustForces;
            thrusterTransformName = m.thrusterTransformName;
            thrusterPower = m.thrusterPower;
            resourceName = m.resourceName;
            fxPrefabName = m.fxPrefabName;
            fxPrefix = m.fxPrefix;
            fxOffset = m.fxOffset;
            fxOffsetRot = m.fxOffsetRot;
            isJustForShow = m.isJustForShow;
            requiresFuel = m.requiresFuel;
            isEnabled = m.isEnabled;
            realISP = m.realISP;
            G = m.G;
            resourceMass = m.resourceMass;
            atmosphereCurve = m.atmosphereCurve;
            resCode = resourceName.GetHashCode();
            double testFuel = part.RequestResource(resCode, 1);
            if (testFuel > 0)
            {
                operational = true;
                part.RequestResource(resCode, -testFuel);
            }
            else
            {
                operational = false;
            }
            Events["Disable"].active = m.Events["Disable"].active;
            Events["Enable"].active = m.Events["Enable"].active;
            Actions["ToggleAction"].actionGroup = m.Actions["ToggleAction"].actionGroup;
            if (thrusterTransforms != null)
            {
                awoken = true;
                if (Events["Disable"].active != Events["Disable"].active)
                    started = true;
            }
        }

        public override void OnAwake()
        {
            if (awoken)
                return;
            awoken = true;
            if (atmosphereCurve == null)
                atmosphereCurve = new FloatCurve();
            resCode = resourceName.GetHashCode();
            thrustForces = new List<float>();
            thrusterTransforms = new List<Transform>();
            thrusterFX = new List<FXGroup>();
        }

        public override void OnStart(StartState state)
        {
            if (started)
                return;
            started = true;

            if (state == StartState.Editor)
            {
                inEditor = true;
                return;
            }
            resourceMass = PartResourceLibrary.Instance.GetDefinition(resCode).density;

            thrusterTransforms = new List<Transform>(part.FindModelTransforms(thrusterTransformName));
            int i = 0;
            foreach (Transform trans in thrusterTransforms)
            {
                GameObject emitter = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Effects/" + fxPrefabName));
                emitter.transform.parent = trans;
                emitter.transform.localPosition = fxOffset;
                emitter.transform.localRotation = Quaternion.Euler(fxOffsetRot);
                FXGroup group = new FXGroup(fxPrefix + i++);
                group.fxEmitters.Add(emitter);
                thrusterFX.Add(group);
                part.fxGroups.Add(group);
            }

            if (isEnabled)
                Enable();
            else
                Disable();
        }

        public override void FixedUpdate()
        {
            base.FixedUpdate();
            if (inEditor)
                return;
            realISP = atmosphereCurve.Evaluate((float)vessel.staticPressure);

            Vector3 inputLinear = vessel.ReferenceTransform.rotation * new Vector3(vessel.ctrlState.X, vessel.ctrlState.Z, vessel.ctrlState.Y);
            Vector3 inputAngular = vessel.ReferenceTransform.rotation * new Vector3(vessel.ctrlState.pitch, vessel.ctrlState.roll, vessel.ctrlState.yaw);

            if (thrustForces.Count != thrusterTransforms.Count)
                thrustForces = Enumerable.Repeat(0.0f, thrusterTransforms.Count).ToList();

            if (TimeWarp.CurrentRate > 1.0f && TimeWarp.WarpMode == TimeWarp.Modes.HIGH)
            {
                foreach (FXGroup fx in thrusterFX)
                    fx.setActive(false);
                return;
            }

            if (isEnabled && part.isControllable && vessel.ActionGroups[KSPActionGroup.RCS])
            {
                int count = thrusterTransforms.Count;
                Vector3 torque = Vector3.Cross(inputAngular, part.transform.position - (vessel.CoM + vessel.rb_velocity * Time.deltaTime));
                float fuelCoeff = (thrusterPower * Time.deltaTime) / (resourceMass * realISP * G);
                for (int i = 0; i < count; ++i)
                {
                    if (controlState == ControlState.Local)
                    {
                        thrustForces[i] = Mathf.Max(Vector3.Dot(thrusterTransforms[i].up, torque), 0f);
                        thrustForces[i] = thrustForces[i] + Mathf.Max(Vector3.Dot(thrusterTransforms[i].up, inputLinear), 0f);
                    }
                    thrustForces[i] = Mathf.Clamp(thrustForces[i], 0.0f, 1.0f);

                    if (thrustForces[i] > 0)
                    {
                        float effect = thrustForces[i];
                        if (!isJustForShow)
                        {
                            if (!CheatOptions.InfiniteRCS)
                            {
                                float fuelNeeded = fuelCoeff * thrustForces[i];
                                float fuelAvailable = part.RequestResource(resCode, fuelNeeded);
                                effect *= fuelAvailable / fuelNeeded;
                            }
                            Vector3 force = (-thrusterPower * effect * thrustForces[i]) * thrusterTransforms[i].up;
                            Vector3 position = thrusterTransforms[i].transform.position;
                            part.Rigidbody.AddForceAtPosition(force, position, ForceMode.Force);
                        }
                        thrusterFX[i].Power = effect;
                        thrusterFX[i].setActive(effect > 0f);
                    }
                    else
                    {
                        thrusterFX[i].setActive(false);
                    }

                }
            }
            else
            {
                foreach (FXGroup fx in thrusterFX)
                    fx.setActive(false);
            }

            if (CheatOptions.InfiniteRCS)
                operational = true;
            else
            {
                double testFuel = part.RequestResource(resCode, 1);
                if (testFuel > 0)
                {
                    operational = true;
                    part.RequestResource(resCode, -testFuel);
                }
                else
                {
                    operational = false;
                }
            }

        }

        [KSPEvent(guiName = "Disable RCS Port", guiActive = true)]
        public void Disable()
        {
            isEnabled = false;
            Events["Disable"].active = false;
            Events["Enable"].active = true;
        }
        [KSPEvent(guiName = "Enable RCS Port", guiActive = true)]
        public void Enable()
        {
            isEnabled = true;
            Events["Disable"].active = true;
            Events["Enable"].active = false;
        }
        [KSPAction("Toggle RCS Thrust")]
        public void ToggleAction(KSPActionParam param)
        {
            if (param.type == KSPActionType.Activate)
                Enable();
            else
                Disable();
        }
    }
}

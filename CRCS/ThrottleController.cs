﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace KerbCom
{
    class ThrottleController : BaseThrottleController
    {
        public void ensureAwake()
        {
            OnAwake();
        }

        public void ensureStarted(StartState state)
        {
            OnAwake();
            OnStart(state);
        }

        public bool throttleLocked;
        private bool useEngineResponseTime;
        private float engineAccelerationSpeed, engineDecelerationSpeed;
        public ModuleEngines engine;
        private bool awoken = false, started = false;

        [KSPField(isPersistant = false)]
        public string thrustVectorTransformName = "thrustTransform";

        public override void OnAwake()
        {
            if (awoken)
                return;
            awoken = true;
        }
        public override void OnStart(StartState state)
        {
            if (started) return;
            started = true;
            //find the correct ModuleEngines
            engine = part.Modules.Cast<PartModule>().Where(m => m is ModuleEngines && ((ModuleEngines)m).thrustVectorTransformName == thrustVectorTransformName).Cast<ModuleEngines>().First();
            throttleLocked = engine.throttleLocked;
            useEngineResponseTime = engine.useEngineResponseTime;
            if (!throttleLocked)
            {
                if (useEngineResponseTime)
                {
                    engineAccelerationSpeed = engine.engineAccelerationSpeed;
                    engineDecelerationSpeed = engine.engineDecelerationSpeed;
                }
                engine.useEngineResponseTime = true;
                engine.engineAccelerationSpeed = 0.0f;
                engine.engineDecelerationSpeed = 0.0f;
            }
        }

        public override void FixedUpdate()
        {
            base.FixedUpdate();

            operational = engine.EngineIgnited && !engine.flameout;
            engine.realIsp = engine.atmosphereCurve.Evaluate((float)vessel.staticPressure);
            if (!throttleLocked)
            {
                if (operational)
                {
                    float throttle = controlState == ControlState.Local ? engine.requestedThrottle : overrideThrottle;
                    if (useEngineResponseTime)
                    {
                        if (engine.currentThrottle > throttle)
                            engine.currentThrottle = Mathf.Lerp(engine.currentThrottle, throttle, engineDecelerationSpeed * Time.deltaTime);
                        else
                            engine.currentThrottle = Mathf.Lerp(engine.currentThrottle, throttle, engineAccelerationSpeed * Time.deltaTime);
                    }
                    else
                    {
                        engine.currentThrottle = throttle;
                    }
                    if (engine.currentThrottle == float.NaN)
                        engine.currentThrottle = 0.0f;
                    else
                        engine.currentThrottle = Mathf.Clamp(engine.currentThrottle, 0.0f, 1.0f);
                }
                else
                {
                    engine.currentThrottle = 0.001f;
                }
            }
        }

        public override Vector3 getDirection(int index)
        {
            return -engine.thrustTransforms[index].forward;
        }

        public override Vector3 getPosition(int index)
        {
            return engine.thrustTransforms[index].position;
        }

        public override ModuleEngines getEngine()
        {
            return engine;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace KerbCom
{
    abstract class BaseThrottleController : PartEffectorModule
    {
        [KSPField(isPersistant = true)]
        public float overrideThrottle;
        [KSPField(guiName = "Operational", guiActive = true)]
        public bool operational = false;

        public abstract Vector3 getDirection(int index);
        public abstract Vector3 getPosition(int index);
        public abstract ModuleEngines getEngine();
    }
}

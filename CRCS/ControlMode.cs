﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KerbCom
{
    abstract class ControlMode : IConfigNode
    {
        public abstract Vessel vessel { get; set; }
        public abstract void onStructuralChange();
        public abstract void onFrame();
        public abstract void onGUI();
        public abstract void start();
        public abstract bool running { get; }
        public abstract void stop();

        public abstract void Load(ConfigNode node);
        public abstract void Save(ConfigNode node);

        [System.AttributeUsage(System.AttributeTargets.Class | System.AttributeTargets.Struct, Inherited = false)]
        public sealed class Details : System.Attribute
        {
            private string _guiName = null;
            public string guiName
            {
                get
                {
                    if (_guiName == null)
                        if (type != null)
                            return type.Name;
                        else
                            return "Invalid control mode";
                    else
                        return _guiName;
                }
                set
                {
                    _guiName = value;
                }
            }
            private string _guiHelp = null;
            public string guiHelp
            {
                get
                {
                    if (_guiHelp == null)
                        if (type != null)
                            return "No information provided.";
                        else
                            return "Invalid control mode.";
                    else
                        return _guiHelp;
                }
                set
                {
                    _guiHelp = value;
                }
            }
            public Type type = null;
            public Type getClassType() { return type; }
            public Details()
            {
            }
        }
        protected ControlMode()
        {
            Type type = this.GetType();
            System.Attribute[] attrs = System.Attribute.GetCustomAttributes(type);
            var detailsList = attrs.Where(a => a is ControlMode.Details).Select(a => (ControlMode.Details)a);
            if (detailsList.Count() != 1)
                throw new ArgumentException("ControlMode classes must use the ControlMode.Details attribute exactly once.");
            else
            {
                Details details = detailsList.Single();
                if (details.type == null) details.type = this.GetType();
            }
        }

        public abstract IEnumerable<PartEffectorModule> getDesiredEffectors();

        public ControlMode.Details details
        {
            get
            {
                return (ControlMode.Details)System.Attribute.GetCustomAttributes(this.GetType()).Where(a => a is ControlMode.Details).Single();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace KerbCom
{
    class GimballedEngineController : BaseThrottleController
    {
        [KSPField(isPersistant = false, guiActive = true, guiName = "Enhanced Gimbal")]
        public bool enhancedGimbal = true;

        #region Overrides
        [KSPField(isPersistant = true)]
        public float overrideAngleX = 0;
        [KSPField(isPersistant = true)]
        public float overrideAngleY = 0;
        #endregion

        #region Private state
        private bool useEngineResponseTime;
        private float engineAccelerationSpeed, engineDecelerationSpeed;
        private bool awoken = false, started = false;
        private bool initialised = false;

        private GameObject[] originObjects;
        private Transform[] thrustTransforms;
        #endregion

        #region Configuration
        [KSPField(isPersistant = false)]
        public string thrustVectorTransformName = "thrustTransform";
        #endregion

        #region Queryable state
        public ReadOnlyIndexer<Transform> thrustOriginTransforms;
        public ModuleEngines engine;
        public ModuleGimbal gimbal;
        #endregion
        
        #region Initialisation
        public void ensureAwake()
        {
            OnAwake();
        }

        public void ensureStarted(StartState state)
        {
            OnAwake();
            OnStart(state);
        }
        public override void OnAwake()
        {
            if (awoken)
                return;
            awoken = true;
            thrustOriginTransforms = new ReadOnlyIndexer<Transform>(i => originObjects[i].transform, i => i >= 0 && i < originObjects.Length);
        }
        private void initGimbal()
        {
            if (initialised)
                return;

            //ModuleGimbal does not behave very realistically, so instead of controlling it,
            //it is easier to just override it. Plus gimbal response time is difficult to add to the solver correctly.
            if (gimbal == null)
                Debug.Log("Error: gimbal is null");
            if(gimbal.gimbalTransforms == null)
                Debug.Log("Error: gimbal.gimbalTransforms is null");

            //copy original transforms
            originObjects = new GameObject[gimbal.gimbalTransforms.Count];
            thrustTransforms = new Transform[gimbal.gimbalTransforms.Count];
            //Why is initRots protected? The following line is just to avoid having to make a subclass of ModuleGimbal.
            List<Quaternion> initRots = (List<Quaternion>)(typeof(ModuleGimbal).GetField("initRots", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).GetValue(gimbal));
            if (initRots == null)
                Debug.Log("Error: gimbal.initRots is null");
            for (int i = 0; i < originObjects.Count(); ++i)
            {
                Transform actual = gimbal.gimbalTransforms[i];
                thrustTransforms[i] = actual;
                actual.localRotation = initRots[i];
                originObjects[i] = new GameObject();
                Transform origin = originObjects[i].transform;
                origin.parent = actual.parent;
                origin.localPosition = actual.localPosition;
                origin.localRotation = actual.localRotation;
                origin.localScale = actual.localScale;
            }
            //Debug.Log("Gimballed Engine: " + gimbal.gimbalTransforms.Count + " thrust transforms");
            if (gimbal.gimbalTransforms.Count == 0)
                Debug.Log("Gimballed Engine: Failed init. Will try again later.");
            else
                initialised = true;
            //stop ModuleGimbal from controlling anything:
            gimbal.gimbalTransforms.Clear();
        }
        public override void OnStart(StartState state)
        {
            if (started) return;
            started = true;
            Debug.Log("Gimbal controller starting");
            //find the correct ModuleEngines
            engine = part.Modules.Cast<PartModule>().Where(m => m is ModuleEngines && ((ModuleEngines)m).thrustVectorTransformName == thrustVectorTransformName).Cast<ModuleEngines>().First();
            //and the ModuleGimbal. This assumes that both use the same transforms.
            //The ideal solution would be to see which thrust transforms are affected by which gimbals,
            //though I know of no part that uses such a crazy configuration that this is required.
            gimbal = part.Modules.Cast<PartModule>().Where(m => m is ModuleGimbal && ((ModuleGimbal)m).gimbalTransformName == thrustVectorTransformName).Cast<ModuleGimbal>().First();
            if (gimbal == null)
                Debug.Log("Error in start: gimbal is null");
            else
                Debug.Log("Gimbal is not null");
            useEngineResponseTime = engine.useEngineResponseTime;
            if (useEngineResponseTime)
            {
                engineAccelerationSpeed = engine.engineAccelerationSpeed;
                engineDecelerationSpeed = engine.engineDecelerationSpeed;
            }
            engine.useEngineResponseTime = true;
            engine.engineAccelerationSpeed = 0.0f;
            engine.engineDecelerationSpeed = 0.0f;
            initGimbal();
        }
        #endregion

        public override void FixedUpdate()
        {
            base.FixedUpdate();

            if (!initialised)
                initGimbal();
            operational = engine.EngineIgnited && !engine.flameout;
            engine.realIsp = engine.atmosphereCurve.Evaluate((float)vessel.staticPressure);
            float throttle;
            float targetX, targetY;
            if (controlState == ControlState.Local)
            {
                throttle = engine.requestedThrottle;
                if (gimbal.gimbalLock)
                {
                    targetX = 0.0f; targetY = 0.0f;
                }
                else
                {
                    //To find a reasonable local gimbal behaviour, consider the change in torque produced by the gimbal.
                    //Approximate this linearly, as the true behaviour is sinsusoidal. Should be good enough for small angles.

                    //Find how the gimballing in each direction (X and Y) affects the torque produced by the engine.
                    Vector3 torqueDirX = Vector3.zero;
                    Vector3 torqueDirY = Vector3.zero;
                    Vector3 COM = vessel.transform.InverseTransformPoint(vessel.CoM + vessel.rb_velocity * Time.deltaTime);
                    float range = Mathf.Sin(gimbal.gimbalRange);
                    foreach (Transform t in thrustOriginTransforms)
                    {
                        Vector3 xUnitForce = vessel.transform.InverseTransformDirection(t.rotation * Quaternion.AngleAxis(gimbal.gimbalRange, Vector3.right) * -Vector3.forward);
                        Vector3 nxUnitForce = vessel.transform.InverseTransformDirection(t.rotation * Quaternion.AngleAxis(-gimbal.gimbalRange, Vector3.right) * -Vector3.forward);
                        Vector3 yUnitForce = vessel.transform.InverseTransformDirection(t.rotation * Quaternion.AngleAxis(gimbal.gimbalRange, Vector3.up) * -Vector3.forward);
                        Vector3 nyUnitForce = vessel.transform.InverseTransformDirection(t.rotation * Quaternion.AngleAxis(-gimbal.gimbalRange, Vector3.up) * -Vector3.forward);
                     
                        Vector3 offset = vessel.transform.InverseTransformPoint(t.position) - COM;
                        
                        Vector3 xUnitMoment = Vector3.Cross(offset, xUnitForce);
                        Vector3 nxUnitMoment = Vector3.Cross(offset, nxUnitForce);
                        Vector3 yUnitMoment = Vector3.Cross(offset, yUnitForce);
                        Vector3 nyUnitMoment = Vector3.Cross(offset, nyUnitForce);
                        
                        Vector3 xDiff = xUnitMoment - nxUnitMoment;
                        Vector3 yDiff = yUnitMoment - nyUnitMoment;
                        
                        torqueDirX += xDiff;
                        torqueDirY += yDiff;
                    }
                    torqueDirX.Normalize();
                    torqueDirY.Normalize();

                    //retarget the user input to vessel space:
                    Vector3 target = -vessel.transform.InverseTransformDirection(vessel.ReferenceTransform.rotation * new Vector3(vessel.ctrlState.pitch, vessel.ctrlState.roll, vessel.ctrlState.yaw));
                    Vector3 targetNorm = target.normalized;
                    //Project the target torque onto the approximate rectangular planar region of possible torques,
                    //then scale it by the length of the projection of its normalised direction to account for small projections.
                    Vector2 projected = new Vector2(Vector3.Dot(target, torqueDirX), Vector3.Dot(target, torqueDirY));
                    Vector2 projectedNorm = new Vector2(Vector3.Dot(targetNorm, torqueDirX), Vector3.Dot(targetNorm, torqueDirY));
                    float factor = projectedNorm.magnitude;
                    if (factor > 1E-2)
                        projected /= factor;
                    targetX = projected.x;
                    targetY = projected.y;
                }
            }
            else
            {
                throttle = overrideThrottle;
                targetX = overrideAngleX;
                targetY = overrideAngleY;
            }
            if (throttle == float.NaN)
                throttle = 0.0f;
            if (targetX == float.NaN)
                targetX = 0.0f;
            if (targetY== float.NaN)
                targetY = 0.0f;
            targetX = Mathf.Clamp(targetX, -1.0f, 1.0f);
            targetY = Mathf.Clamp(targetY, -1.0f, 1.0f);

            for (int i = 0; i < thrustTransforms.Count(); ++i )
            {
                Transform origin = thrustOriginTransforms[i];
                Transform actual = thrustTransforms[i];
                if (gimbal.gimbalLock)
                    actual.localRotation = origin.localRotation;
                else
                {
                    //Rotating like this confines the region of thrust directions very nearly to a square frustum.
                    //This is handy for any solvers that want to reason about gimbals.
                    //It is also more realistic for certain gimbal designs.
                    //Quaternion rotX = Quaternion.AngleAxis(targetX * gimbal.gimbalRange, Vector3.right);
                    //Quaternion rotY = Quaternion.AngleAxis(targetY * gimbal.gimbalRange, rotX * Vector3.up);
                    //Quaternion full = rotY * rotX * origin.localRotation;
                    //actual.localRotation = origin.localRotation * Quaternion.AngleAxis(vessel.ctrlState.yaw * gimbal.gimbalRange, origin.up) * Quaternion.AngleAxis(0.0f * gimbal.gimbalRange, origin.right);
                    actual.localRotation = origin.localRotation * Quaternion.AngleAxis(targetY * gimbal.gimbalRange, Vector3.up) * Quaternion.AngleAxis(targetX * gimbal.gimbalRange, Vector3.right);
                    //actual.localRotation = full;
                }
            }
            
            if (operational)
            {
                if (useEngineResponseTime)
                {
                    if (engine.currentThrottle > throttle)
                        engine.currentThrottle = Mathf.Lerp(engine.currentThrottle, throttle, engineDecelerationSpeed * Time.deltaTime);
                    else
                        engine.currentThrottle = Mathf.Lerp(engine.currentThrottle, throttle, engineAccelerationSpeed * Time.deltaTime);
                }
                else
                {
                    engine.currentThrottle = throttle;
                }
                if (engine.currentThrottle == float.NaN)
                    engine.currentThrottle = 0.0f;
                else
                    engine.currentThrottle = Mathf.Clamp(engine.currentThrottle, 0.0f, 1.0f);
            }
            else
            {
                engine.currentThrottle = 0.001f;
            }
        }

        #region Standardised interface
        public override Vector3 getDirection(int index)
        {
            return -thrustOriginTransforms[index].forward;
        }
        
        public override Vector3 getPosition(int index)
        {
            return thrustOriginTransforms[index].position;
        }

        public override ModuleEngines getEngine()
        {
            return engine;
        }
        #endregion
    }
}

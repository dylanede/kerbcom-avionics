﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace KerbCom
{
    [KSPAddonFixed(KSPAddon.Startup.Flight, false, typeof(FlightTimeEnhancer))]
    class FlightTimeEnhancer : MonoBehaviour
    {
        Dictionary<Vessel, int> vessels;
        public void Start()
        {
            //Debug.Log("Starting auto-enhancing!");
            vessels = new Dictionary<Vessel, int>();
            foreach (Vessel v in FlightGlobals.Vessels)
            {
                enhance(v);
                vessels.Add(v, v.Parts.Count);
            }
        }
        private void enhance(Vessel v)
        {
            PartModule.StartState s = PartModule.StartState.None;
            if (v.situation == Vessel.Situations.PRELAUNCH)
                s = PartModule.StartState.PreLaunch | PartModule.StartState.Landed;
            else if (v.situation == Vessel.Situations.DOCKED)
                s = PartModule.StartState.Docked;
            else if (v.situation == Vessel.Situations.ORBITING || v.situation == Vessel.Situations.ESCAPING)
                s = PartModule.StartState.Orbital;
            else if (v.situation == Vessel.Situations.SUB_ORBITAL)
                s = PartModule.StartState.SubOrbital;
            else if (v.situation == Vessel.Situations.SPLASHED)
                s = PartModule.StartState.Splashed;
            else if (v.situation == Vessel.Situations.FLYING)
                s = PartModule.StartState.Flying;
            else if (v.situation == Vessel.Situations.LANDED)
                s = PartModule.StartState.Landed;
            convertRCS(v, s);
            augmentEngines(v, s);
            augmentReactionWheels(v, s);
        }
        public void Update()
        {
            foreach (Vessel v in FlightGlobals.Vessels)
            {
                if (vessels.ContainsKey(v))
                {
                    if (v.Parts.Count != vessels[v])
                    {
                        enhance(v);
                        vessels[v] = v.Parts.Count;
                    }
                }
                else
                {
                    enhance(v);
                    vessels[v] = v.Parts.Count;
                }
            }
            List<Vessel> toremove = new List<Vessel>();
            foreach (KeyValuePair<Vessel,int> vn in vessels)
            {
                if (FlightGlobals.Vessels.Contains(vn.Key))
                {
                    if (vn.Key.parts.Count != vn.Value)
                        enhance(vn.Key);
                }
                else
                {
                    toremove.Add(vn.Key);
                }
            }
            foreach (Vessel v in toremove)
                vessels.Remove(v);
        }
        public static IEnumerable<T> getAllModules<T>(Vessel vessel) where T : PartModule
        {
            return vessel.parts.SelectMany(p => p.Modules.Cast<PartModule>().Where(m => m is T).Cast<T>());
        }

        private void convertRCS(Vessel vessel, PartModule.StartState startState)
        {
            List<ModuleRCS> allRCS = getAllModules<ModuleRCS>(vessel).ToList();
            foreach (ModuleRCS m in allRCS)
            {
                MyRCSModule rep = (MyRCSModule)m.part.AddModule(typeof(MyRCSModule).Name);
                rep.migrateFrom(m);
                rep.ensureStarted(startState);
                m.part.Modules.Remove(m);
            }
        }

        private void augmentEngines(Vessel vessel, PartModule.StartState startState)
        {
            List<ModuleEngines> allEngines = vessel.parts.
                Where(p => p.Modules.Cast<PartModule>().Where(m => m is ThrottleController || m is GimballedEngineController).Count() == 0).
                SelectMany(p => p.Modules.Cast<PartModule>().Where(m => m is ModuleEngines).Cast<ModuleEngines>()).ToList();
            foreach (ModuleEngines m in allEngines)
            {
                //Determine if a gimbal is available:
                ModuleGimbal gimbal = m.part.Modules.Cast<PartModule>().Where(g => g is ModuleGimbal && ((ModuleGimbal)g).gimbalTransformName == m.thrustVectorTransformName).Cast<ModuleGimbal>().FirstOrDefault();
                if (gimbal == null) //no gimbal
                {
                    ThrottleController controller = (ThrottleController)m.part.AddModule(typeof(ThrottleController).Name);
                    controller.thrustVectorTransformName = m.thrustVectorTransformName;
                    controller.ensureStarted(startState);
                }
                else // gimbal
                {
                    GimballedEngineController controller = (GimballedEngineController)m.part.AddModule(typeof(GimballedEngineController).Name);
                    controller.thrustVectorTransformName = m.thrustVectorTransformName;
                    controller.ensureStarted(startState);
                }
            }
        }

        private void augmentReactionWheels(Vessel vessel, PartModule.StartState startState)
        {
            List<ModuleReactionWheel> allWheels = vessel.parts.
                Where(p => p.Modules.Cast<PartModule>().Where(m => m is ReactionWheelController).Count() == 0).
                SelectMany(p => p.Modules.Cast<PartModule>().Where(m => m is ModuleReactionWheel).Cast<ModuleReactionWheel>()).ToList();

            foreach (ModuleReactionWheel wheel in allWheels)
            {
                ReactionWheelController controller = (ReactionWheelController)wheel.part.AddModule(typeof(ReactionWheelController).Name);
                controller.ensureStarted(startState);
            }
        }
    }
}

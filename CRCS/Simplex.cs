﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KerbCom
{
    /// <summary>
    /// Solves maximisation linear programming problems using the Simplex method.
    /// </summary>
    public class Simplex
    {
        public class State
        {
            public int n, m;
            public double[,] equations;// (m+1) by (n+1)
            public int[] basic_vars;// m
            
            //stats
            public int iteration_count = 0;
            public bool optimal = false;
            public bool unconstrained = false;
            public bool feasible = false;
        }
        public static State createFeasibleState(int n, int m)
        {
            State state = new State();
            state.n = n;
            state.m = m;
            state.equations = new double[m + 1, n + 1];
            state.basic_vars = new int[m];
            state.feasible = true;
            return state;
        }
        //public static State createUnfeasibleState(int n, int m, int equalities)
        //{
        //    State state = new State();
        //    state.equations = 
        //}
        public static void solve(State state) // assumes tableau is proper and feasible
        {
            int np1 = state.n + 1;
            int mp1 = state.m + 1;
            while (!state.optimal && !state.unconstrained)
            {
                // check optimality and find entering basic variable
                double min_coeff = state.equations[0, 0];
                int entering_i = 0;
                for (int i = 1; i < state.n; ++i)
                {
                    double c = state.equations[0, i];
                    if (c < min_coeff)
                    {
                        min_coeff = c;
                        entering_i = i;
                    }
                }
                if (min_coeff >= 0.0)
                {
                    state.optimal = true;
                    break;
                }

                // find leaving basic variable using MRT
                bool no_limit = true;
                double min_mrt = 0;
                int leaving_j = 0;
                for (int j = 0; j < state.m; ++j)
                {
                    int _j = j + 1;
                    if (state.equations[_j, entering_i] > 0.0)
                    {
                        double mrt = state.equations[_j, state.n] / state.equations[_j, entering_i];
                        if (no_limit || mrt < min_mrt)
                        {
                            min_mrt = mrt;
                            leaving_j = j;
                            no_limit = false;
                        }
                    }
                }
                if (no_limit) // unconstrained solution
                {
                    state.unconstrained = true;
                    break;
                }
                
                // update tableau

                state.basic_vars[leaving_j] = entering_i; // update basic variable list

                int ljp1 = leaving_j + 1;

                //ensure pivot element is 1 by dividing its equation
                double pivot_value = state.equations[ljp1, entering_i];
                for (int i = 0; i < np1; ++i)
                {
                    state.equations[ljp1, i] /= pivot_value;
                }

                // remove entering variable from all equations except its own using Gaussian elimination
                // this puts the tableau back in proper form
                
                for (int _j = 0; _j < mp1; ++_j)
                {
                    if (_j != ljp1)
                    {
                        double factor = state.equations[_j, entering_i];
                        for (int i = 0; i < np1; ++i)
                        {
                            state.equations[_j, i] -= factor * state.equations[ljp1, i];
                        }
                    }
                }
                ++state.iteration_count;
            }
        }

        public static double getValue(State state)
        {
            return state.equations[0, state.n];
        }

        public static void getVariableValues(State state, ref double[] valuesOut)
        {
            if (valuesOut == null || valuesOut.Length != state.n)
                valuesOut = new double[state.n];
            for (int i = 0; i < state.n; ++i)
            {
                valuesOut[i] = 0;
            }
            for (int j = 0; j < state.m; ++j)
            {
                int i = state.basic_vars[j];
                valuesOut[i] = state.equations[j + 1, state.n];
            }
        }
    }
}

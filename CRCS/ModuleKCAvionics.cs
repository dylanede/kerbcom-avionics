﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace KerbCom
{
    class ModuleKCAvionics : PartModule
    {
        [KSPField(isPersistant = true, guiActive = true, guiName = "Avionics in control")]
        public bool inControl = false;

        [KSPField(isPersistant = true, guiActive = true, guiName = "Avionics GUI visible")]
        public bool guiVisible = true;

        private StartState startState;
        bool inEditor = false;

        List<Pair<ControlMode, bool>> modes;
        Dictionary<ControlMode, IEnumerable<PartEffectorModule>> desiredEffectors;
        HashSet<PartEffectorModule> activeEffectors;

        private IEnumerable<ControlMode> activeModes
        {
            get
            {
                return modes.Where(kv => kv.second == true).Select(kv => kv.first);
            }
        }

        private int lastPartCount = 0;

        private bool VesselChanged
        {
            get
            {
                return vessel.Parts.Count != lastPartCount;
            }
        }

        public static IEnumerable<T> getAllModules<T>(Vessel vessel) where T : PartModule
        {
            return vessel.parts.SelectMany(p => p.Modules.Cast<PartModule>().Where(m => m is T).Cast<T>());
        }

        private void determineControl()
        {
            var allOnVessel = getAllModules<ModuleKCAvionics>(vessel);
            var hadControl = allOnVessel.Where(m => m.inControl);
            switch (hadControl.Count())
            {
                case 0: // Assume control, since no other has it
                    Debug.Log("KCA(ID:" + GetHashCode() + "): Assuming control over vessel \"" + vessel.id + "\"");
                    inControl = true;
                    break;
                case 1: // No conflict, so no change required
                    if (hadControl.First() == this)
                        Debug.Log("KCA(ID:" + GetHashCode() + "): Maintaining control over vessel \"" + vessel.id + "\"");
                    else
                        Debug.Log("KCA(ID:" + GetHashCode() + "): Deferring control over vessel \"" + vessel.id + "\" to KCA " + hadControl.First().GetHashCode());
                    break;
                default: // Conflict - elect first found
                    Debug.Log("KCA(ID:" + GetHashCode() + "): Initiating election for control over vessel \"" + vessel.id + "\"");
                    bool first = true;
                    foreach (ModuleKCAvionics m in hadControl)
                    {
                        if (first)
                        {
                            Debug.Log("KCA(ID:" + GetHashCode() + "): Given KCA " + m.GetHashCode()  +" control over vessel \"" + vessel.id + "\"");
                            m.inControl = true;
                            first = false;
                        }
                        else
                            m.inControl = false;
                    }
                    break;
            }
        }

        private void onStructuralChange()
        {
            determineControl();
            if (inControl)
            {
                HashSet<PartEffectorModule> toControl = new HashSet<PartEffectorModule>();
                List<ControlMode> toDisable = new List<ControlMode>();

                activeEffectors.Clear();
                foreach (var pair in modes)
                {
                    desiredEffectors[pair.first] = pair.first.getDesiredEffectors();
                }
                foreach (ControlMode mode in activeModes)
                {
                    activeEffectors.UnionWith(desiredEffectors[mode]);
                    IEnumerable<PartEffectorModule> desired = mode.getDesiredEffectors();
                    if (desired.Count() < 0)
                        toDisable.Add(mode);
                    else
                        toControl.UnionWith(desired);
                    
                }
                foreach (Pair<ControlMode, bool> mode in modes)
                {
                    if (toDisable.Contains(mode.first))
                        mode.second = false;
                }
                foreach (PartEffectorModule module in toControl)
                {
                    module.setController(this, () => this.vessel);
                }
                foreach (PartEffectorModule module in getAllModules<PartEffectorModule>(this.vessel).Except(toControl))
                {
                    module.setLocalControl();
                }
                foreach (ControlMode mode in activeModes)
                {
                    mode.onStructuralChange();
                }
            }
        }

        private bool initialised = false;
        private void init()
        {
            initialised = true;
            modes = (new List<ControlMode>() {
                    new RCSControlModes.RCSVanillaMode(),
                    //new RCSControlModes.RCSLinearMode(),
                    new RCSControlModes.RCSLinearPriorityMode(),
                    new EngineControlModes.EngineVanillaMode(),
                    //new EngineControlModes.EngineLinearMode(),
                    //new EngineControlModes.EngineAutoTrim(),
                    //new EngineControlModes.EngineGimbalMode(),
                    new ControlModes.CombinedMode()
                }).Select(k => new Pair<ControlMode, bool>(k, false)).ToList();
            activeEffectors = new HashSet<PartEffectorModule>();
            desiredEffectors = new Dictionary<ControlMode, IEnumerable<PartEffectorModule>>();
            foreach (var pair in modes)
                desiredEffectors.Add(pair.first, Enumerable.Empty<PartEffectorModule>());
        }

        public override void OnLoad(ConfigNode node)
        {
            if (!initialised)
                init();
            if (node != null)
            {
                ConfigNode activeList = node.GetNode("active");
                if(activeList != null)
                    foreach (string name in activeList.GetValues("mode"))
                    {
                        var pair = modes.Where(p => p.first.GetType().Name == name).FirstOrDefault();
                        if (pair != null)
                            pair.second = true;
                    }
                foreach (Pair<ControlMode,bool> pmode in modes)
                {
                    ControlMode mode = pmode.first;
                    ConfigNode subnode = node.GetNode(mode.GetType().Name);
                    if (subnode != null)
                        mode.Load(subnode);
                }
            }
        }
        public override void OnSave(ConfigNode node)
        {
            if (!initialised)
                init();
            if (node != null)
            {
                ConfigNode activeList = node.AddNode("active");
                foreach (Pair<ControlMode, bool> pair in modes)
                {
                    string name = pair.first.GetType().Name;
                    if (pair.second)
                        activeList.AddValue("mode", name);
                    ConfigNode subnode = node.AddNode(name);
                    pair.first.Save(subnode);
                }
            }
        }
        private bool firstUpdate = true;
        public void FixedUpdate()
        {
            if (inEditor)
                return;
            if (!startFinished)
                return;
            if (firstUpdate)
            {
                firstUpdate = false;
                if (guiVisible)
                    ShowGUI();
                else
                    HideGUI();
            }
            if (VesselChanged)
                onStructuralChange();
            lastPartCount = vessel.Parts.Count;

            if (inControl)
            {
                foreach (Pair<ControlMode, bool> pair in modes)
                {
                    ControlMode mode = pair.first;
                    if (pair.second)
                    {
                        if (!mode.running)
                        {
                            mode.onStructuralChange();
                            mode.start();
                        }
                        mode.onFrame();
                    }
                    else
                    {
                        if (mode.running)
                            mode.stop();
                    }
                }
            }
        }
        bool startFinished = false;
        public override void OnStart(StartState state)
        {
            Debug.Log("Starting KCA");
            startState = state;
            if ((state & StartState.Editor) == StartState.Editor || state == StartState.None)
            {
                inEditor = true;
                return;
            }

            if (!initialised)
                init();

            foreach (Pair<ControlMode, bool> pair in modes)
                pair.first.vessel = vessel;

            onStructuralChange();

            startFinished = true;


            RenderingManager.AddToPostDrawQueue(3, new Callback(OnDraw));
        }

        #region GUI
        private bool guiFirstDraw = true;
        private bool guiHelpEnabled = false;
        private const string guiDefaultHelpText = @"Please click the ""?"" button next to an element to learn more about it.";
        private string guiHelpText = guiDefaultHelpText;
        private Rect guiMainWindowRect;
        private Rect guiHelpWindowRect;
        private Queue<Callback> guiChanges = new Queue<Callback>();
        private void OnDraw()
        {
            if (guiFirstDraw)
            {
                guiMainWindowRect = new Rect(100, 100, 0, 0);
                guiHelpWindowRect = new Rect(400, 100, 0, 0);
                guiFirstDraw = false;
            }
            if (inControl && guiVisible && vessel.isActiveVessel)
            {
                GUI.skin = HighLogic.Skin;
                guiMainWindowRect = GUILayout.Window(13942, guiMainWindowRect, guiDrawMainWindow, "Avionics Control Panel");
                if (guiHelpEnabled)
                    guiHelpWindowRect = GUILayout.Window(13943, guiHelpWindowRect, guiDrawHelpWindow, "Avionics Help", GUILayout.MinWidth(300));
            }
        }
        private void guiDrawHelpWindow(int id)
        {
            GUI.skin.label.wordWrap = true;
            GUILayout.BeginVertical();
            if (GUILayout.Button("Click to close."))
                guiHelpEnabled = false;
            GUILayout.BeginVertical(GUI.skin.textArea);
            GUILayout.Label(guiHelpText);
            GUILayout.EndVertical();
            GUILayout.EndVertical();
            GUI.DragWindow();
            if (Event.current.type == EventType.Repaint)
            {
                guiMainWindowRect.width = guiMainWindowRect.height = 0;
            }
        }
        private void guiDrawMainWindow(int id)
        {
            GUI.skin.label.wordWrap = false;
            GUILayout.BeginHorizontal();
            GUILayout.BeginVertical();
            if (GUILayout.Button("Hide"))
                HideGUI();
            if (GUILayout.Toggle(guiHelpEnabled, "Help", GUI.skin.button) != guiHelpEnabled)
                guiChanges.Enqueue(() =>
                {
                    guiHelpEnabled = !guiHelpEnabled;
                    if (guiHelpEnabled)
                        guiHelpText = guiDefaultHelpText;
                });
#if NOTDEFINED
            if (GUILayout.Button(rcsControlEnabled ? "Disable RCS control" : "Enable RCS control"))
                if (rcsControlEnabled)
                    DisableRCS();
                else
                    EnableRCS();
            if (GUILayout.Button(engineControlEnabled ? "Disable engine control" : "Enable engine control"))
                if (engineControlEnabled)
                    DisableEngines();
                else
                    EnableEngines();
            if (GUILayout.Button(reactionWheelControlEnabled ? "Disable reaction wheel control" : "Enable reaction wheel control"))
                if (reactionWheelControlEnabled)
                    DisableReactionWheels();
                else
                    EnableReactionWheels();
#endif
            //GUILayout.EndVertical();
            //GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            if(guiHelpEnabled && GUILayout.Button("?", GUILayout.ExpandWidth(false)))
                guiHelpText = @"KerbCom Avionics organises control behaviour into separate modes. Each mode provides a means of control over one or more control effectors, such as RCS ports, engines, reaction wheels etc.

To enable an inactive mode, click its button. Its settings will appear to the right.

If a mode's button is disabled, this means that the control effectors it wishes to control either are not present on the vessel, or they are under the control of another mode. If they are in use by another mode, you must disable that mode in order to be able to enable this one.";
            GUILayout.Label("Modes:");
            GUILayout.EndHorizontal();
            
            foreach (Pair<ControlMode, bool> _pair in modes)
            {
                Pair<ControlMode, bool> pair = _pair;
                ControlMode mode = pair.first;
                GUILayout.BeginHorizontal();
                if (guiHelpEnabled && GUILayout.Button("?", GUILayout.ExpandWidth(false)))
                    guiHelpText = mode.details.guiHelp;
                var effectors = desiredEffectors[mode];
                if (effectors.Count() == 0 || (!pair.second && effectors.Any(e => activeEffectors.Contains(e))))
                {
                    GUI.enabled = false;
                    GUILayout.Toggle(pair.second, mode.details.guiName, GUI.skin.button);
                    GUI.enabled = true;
                }
                else
                {
                    bool newState = GUILayout.Toggle(pair.second, mode.details.guiName, GUI.skin.button);
                    if (newState != pair.second)
                        guiChanges.Enqueue(() =>
                        {
                            pair.second = newState;
                            onStructuralChange();
                        });
                }
                GUILayout.EndHorizontal();
            }

            GUILayout.EndVertical();
            foreach (ControlMode mode in activeModes)
            {
                GUILayout.BeginVertical();
                GUILayout.Label(mode.details.guiName);
                GUILayout.BeginVertical(GUI.skin.textArea);
                mode.onGUI();
                GUILayout.EndVertical();
                GUILayout.EndVertical();
            }
            GUILayout.EndHorizontal();
            GUI.DragWindow();
            if (Event.current.type == EventType.Repaint)
            {
                guiMainWindowRect.width = guiMainWindowRect.height = 0;
                while (guiChanges.Count > 0)
                    guiChanges.Dequeue()();
            }
        }
        #endregion


        #region Actions and Toggles

        [KSPEvent(guiName = "Hide Avionics GUI", guiActive = true)]
        public void HideGUI()
        {
            guiVisible = false;
            Events["HideGUI"].active = false;
            Events["ShowGUI"].active = true;
        }
        [KSPEvent(guiName = "Show Avionics GUI", guiActive = true)]
        public void ShowGUI()
        {
            guiVisible = true;
            Events["HideGUI"].active = true;
            Events["ShowGUI"].active = false;
        }
        [KSPAction("Toggle Avionics GUI")]
        public void ToggleGUI(KSPActionParam param)
        {
            if (param.type == KSPActionType.Activate)
                ShowGUI();
            else
                HideGUI();
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KerbCom
{
    /// <summary>
    /// Solves maximisation linear programming problems using the Simplex method.
    /// Has explicit support for upper bounds on variables.
    /// </summary>
    public class SimplexBoundedTwoPhase
    {
        public class State
        {
            public int n, m;
            public int n_obj = 0;
            public int active_obj;
            public double[] constraint_RHS; // m
            public double[,] objectives; // n_obj by n
            public double[] objective_RHS; // n_obj
            public double[,] constraints;// m by n
            public double[] upper_bounds;// n
            public int[] basic_vars;// m
            public bool[] at_ub;// n
            //stats
            public int iteration_count = 0;
            public bool optimal = false;
            public bool unconstrained = false;

            public override string ToString()
            {
                string[] objectives_ = Enumerable.Repeat("", objectives.GetLength(0)).ToArray();
                objectives.process((ref double v, int i, int j) => objectives_[i] += (j==0 ? "" : ", ") + v.ToString());
                string[] constraints_ = Enumerable.Repeat("", constraints.GetLength(0)).ToArray();
                constraints.process((ref double v, int i, int j) => constraints_[i] += (j == 0 ? "" : ", ") + v.ToString());
                string r = "SimplexBoundedTwoPhase.State:\n" +
                    "n: " + n + "\n" +
                    "m: " + m + "\n" +
                    "n_obj: " + n_obj + "\n" +
                    "active_obj: " + active_obj + "\n" +
                    "objectives: " + string.Join(",\n", objectives_) + "\n" +
                    "objective_RHS: " + string.Join(", ", objective_RHS.Select(v => v.ToString()).ToArray()) + "\n" +
                    "constraints: " + string.Join(",\n", constraints_) + "\n" +
                    "constraint_RHS: " + string.Join(", ", constraint_RHS.Select(v => v.ToString()).ToArray()) + "\n" +
                    "upper_bounds: " + string.Join(", ", upper_bounds.Select(v => v.ToString()).ToArray()) + "\n" +
                    "basic_vars: " + string.Join(", ", basic_vars.Select(v => v.ToString()).ToArray()) + "\n" +
                    "at_ub: " + string.Join(", ", at_ub.Select(v => v.ToString()).ToArray()) + "\n" +
                    "iteration_count: " + iteration_count + "\n" +
                    "optimal: " + optimal + "\n" +
                    "unconstrained: " + unconstrained;
                return r;
            }
        }
        public static State createState(int n, int m, int n_obj, double defaultUpperBound = double.PositiveInfinity)
        {
            State state = new State();
            state.n = n;
            state.m = m;
            state.n_obj = n_obj;
            state.constraints = new double[m, n];
            state.constraint_RHS = new double[m];
            state.objectives = new double[n_obj, n];
            state.objective_RHS = new double[n_obj];
            state.basic_vars = new int[m];
            state.upper_bounds = Enumerable.Repeat(defaultUpperBound, n).ToArray(); // new double[n];
            state.at_ub = new bool[n];
            return state;
        }
        public static State createState(int n, int m, int n_obj, double[,] constraints, double[] constraint_RHS, double[,] objectives, double[] objective_RHS, int[] basic_vars, double[] upper_bounds, bool[] at_ub)
        {
            State state = new State();
            state.n = n;
            state.m = m;
            state.n_obj = n_obj;
            if (constraints != null)
                state.constraints = constraints;
            else
                state.constraints = new double[m, n];
            if (constraint_RHS != null)
                state.constraint_RHS = constraint_RHS;
            else
                state.constraint_RHS = new double[m];
            if (objectives != null)
                state.objectives = objectives;
            else
                state.objectives = new double[n_obj, n];
            if (objective_RHS != null)
                state.objective_RHS = objective_RHS;
            else
                state.objective_RHS = new double[n_obj];
            if (basic_vars != null)
                state.basic_vars = basic_vars;
            else
                state.basic_vars = new int[m];
            if (upper_bounds != null)
                state.upper_bounds = upper_bounds;
            else
                state.upper_bounds = new double[n];
            if (at_ub != null)
                state.at_ub = at_ub;
            else
                state.at_ub = new bool[n];
            return state;
        }

        public static void solve(State state) // assumes tableau is proper and feasible
        {
            if (state.n == 0)
                return;
            if (state.m == 0)
            {
                state.unconstrained = true;
                return;
            }

            while (!state.optimal && !state.unconstrained)
            {
                // check optimality and find entering basic variable
                bool increase = !state.at_ub[0];
                double dcoeff = state.at_ub[0] ? state.objectives[state.active_obj, 0] : -state.objectives[state.active_obj, 0];
                int entering_i = 0;
                for (int i = 1; i < state.n; ++i)
                {
                    bool at_ub = state.at_ub[i];
                    double c = at_ub ? state.objectives[state.active_obj, i] : -state.objectives[state.active_obj, i];
                    if (c > dcoeff)
                    {
                        dcoeff = c;
                        entering_i = i;
                        increase = !at_ub;
                    }
                }
                if (dcoeff <= 0.0)
                {
                    state.optimal = true;
                    break;
                }

                // find leaving basic variable using MRT
                bool leaving_at_ub = false;
                double min_mrt = state.upper_bounds[entering_i];
                int leaving_j = -1; // -1 indicates the entering variable is also the leaving variable - i.e. it leaves immediately at the opposite bound.
                for (int j = 0; j < state.m; ++j)
                {
                    double rate = state.constraints[j, entering_i];
                    int basic_i = state.basic_vars[j];
                    //calculate value of variable
                    double value = state.constraint_RHS[j];//RHS
                    for (int i = 0; i < state.n; ++i)
                        if (i != basic_i && state.at_ub[i])
                            value -= state.constraints[j, i] * state.upper_bounds[i];
                    //double value = state.value[basic_i];
                    double mrt;
                    bool to_ub;
                    if (increase ^ (rate < 0.0))
                    {
                        mrt = value / rate;
                        to_ub = false;
                    }
                    else
                    {
                        double ub = state.upper_bounds[basic_i];
                        mrt = (ub - value) / rate;
                        to_ub = true;
                    }
                    mrt = Math.Abs(mrt);
                    if (mrt < min_mrt)
                    {
                        min_mrt = mrt;
                        leaving_j = j;
                        leaving_at_ub = to_ub;
                    }
                }
                if (min_mrt == double.PositiveInfinity) // unconstrained solution
                {
                    state.unconstrained = true;
                    break;
                }
                if (leaving_j < 0) //flip entering variable to its other bound, as it is also the leaving variable.
                {
                    state.at_ub[entering_i] = !state.at_ub[entering_i];
                }
                else
                {
                    // update tableau

                    int leaving_i = state.basic_vars[leaving_j];
                    state.at_ub[leaving_i] = leaving_at_ub;
                    state.at_ub[entering_i] = false;
                    state.basic_vars[leaving_j] = entering_i; // update basic variable list

                    //ensure pivot element is 1 by dividing its equation
                    double pivot_value = state.constraints[leaving_j, entering_i];
                    for (int i = 0; i < state.n; ++i)
                    {
                        state.constraints[leaving_j, i] /= pivot_value;
                    }
                    state.constraint_RHS[leaving_j] /= pivot_value;

                    // remove entering variable from all equations except its own using Gaussian elimination
                    // this puts the tableau back in proper form

                    for (int j = 0; j < state.m; ++j)
                    {
                        if (j != leaving_j)
                        {
                            double factor = state.constraints[j, entering_i];
                            for (int i = 0; i < state.n; ++i)
                                state.constraints[j, i] -= factor * state.constraints[leaving_j, i];
                            state.constraint_RHS[j] -= factor * state.constraint_RHS[leaving_j];
                        }
                    }
                    for(int k = 0; k < state.n_obj; ++ k)
                    {
                        double factor = state.objectives[k, entering_i];
                        for (int i = 0; i < state.n; ++i)
                            state.objectives[k, i] -= factor * state.constraints[leaving_j, i];
                        state.objective_RHS[k] -= factor * state.constraint_RHS[leaving_j];
                    }
                }
                ++state.iteration_count;
            }
        }
        public static bool canonicise(SimplexBoundedTwoPhase.State state, int ej, int basic_i)
        {
            double basic_value = state.constraints[ej, basic_i];
            if (Math.Abs(basic_value) <= double.Epsilon * 10)
                return false;
            //normalise
            for (int i = 0; i < state.n; ++i)
            {
                state.constraints[ej, i] /= basic_value;
            }
            state.constraint_RHS[ej] /= basic_value;

            // remove basic variable from all equations except its own using Gaussian elimination

            for (int j = 0; j < state.m; ++j)
            {
                if (j != ej)
                {
                    double factor = state.constraints[j, basic_i];
                    for (int i = 0; i < state.n; ++i)
                        state.constraints[j, i] -= factor * state.constraints[ej, i];
                    state.constraint_RHS[j] -= factor * state.constraint_RHS[ej];
                }
            }
            for (int k = 0; k < state.n_obj; ++k)
            {
                double factor = state.objectives[k, basic_i];
                for (int i = 0; i < state.n; ++i)
                    state.objectives[k, i] -= factor * state.constraints[ej, i];
                state.objective_RHS[k] -= factor * state.constraint_RHS[ej];
            }
            state.basic_vars[ej] = basic_i;
            return true;
        }

        public static double getValue(State state, int obj)
        {
            double value = state.objective_RHS[obj];
            for (int i = 0; i < state.n; ++i)
                if (state.at_ub[i])
                    value -= state.objectives[obj, i] * state.upper_bounds[i];
            return value;
        }
        public static void getVariableValues(State state, ref double[] valuesOut)
        {
            if (valuesOut == null || valuesOut.Length != state.n)
                valuesOut = new double[state.n];
            for (int i = 0; i < state.n; ++i)
            {
                valuesOut[i] = state.at_ub[i] ? state.upper_bounds[i] : 0;
            }
            for (int j = 0; j < state.m; ++j)
            {
                int basic_i = state.basic_vars[j];
                if (basic_i < state.n)
                {
                    double value = state.constraint_RHS[j];
                    for (int i = 0; i < state.n; ++i)
                        if (i != basic_i && state.at_ub[i])
                            value -= state.constraints[j, i] * state.upper_bounds[i];
                    valuesOut[basic_i] = value;
                }
            }
        }
    }
}
